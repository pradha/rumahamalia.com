<!-- ########## START: LEFT PANEL ########## -->
<div class="br-logo"><a href="">Dashboard</a></div>
<div class="br-sideleft overflow-y-auto">
    <label class="sidebar-label pd-x-15 mg-t-20">Navigation</label>
    <div class="br-sideleft-menu">
        <a href="<?= $this->url->get('/dashboard') ?>" class="call br-menu-link <?php if ($menu == 'dashboard') { ?>active<?php } ?>">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
                <span class="menu-item-label">Dashboard</span>
            </div>
        </a>

        <?php if (haveRole($this->session->get('group'), 17)) { ?>
            <a href="#" class="call br-menu-link <?php if ($menu == 'library' || $menu == 'book' || $menu == 'book-authors' || $menu == 'book-genres') { ?>active show-sub<?php } ?>">
                <div class="br-menu-item">
                    <i class="menu-item-icon icon ion-ios-book-outline tx-24"></i>
                    <span class="menu-item-label">Library</span>
                    <i class="menu-item-arrow fa fa-angle-down"></i>
                </div>
            </a>
            <ul class="br-menu-sub nav flex-column">
                <?php if (haveRole($this->session->get('group'), 18)) { ?><li class="nav-item"><a href="<?= $this->url->get('/library/book') ?>" class="call nav-link <?php if ($menu == 'book') { ?>active<?php } ?>">Books</a></li><?php } ?>
                <?php if (haveRole($this->session->get('group'), 18)) { ?><li class="nav-item"><a href="<?= $this->url->get('/library/author') ?>" class="call nav-link <?php if ($menu == 'book-authors') { ?>active<?php } ?>">Authors</a></li><?php } ?>
                <?php if (haveRole($this->session->get('group'), 18)) { ?><li class="nav-item"><a href="<?= $this->url->get('/library/genre') ?>" class="call nav-link <?php if ($menu == 'book-genres') { ?>active<?php } ?>">Genres</a></li><?php } ?>
            </ul>
        <?php } ?>
        <?php if (haveRole($this->session->get('group'), 17)) { ?>
            <a href="#" class="call br-menu-link <?php if ($menu == 'content' || $menu == 'page' || $menu == 'post' || $menu == 'category' || $menu == 'media' || $menu == 'slider' || $menu == 'comment') { ?>active show-sub<?php } ?>">
                <div class="br-menu-item">
                    <i class="menu-item-icon icon ion-ios-folder-outline tx-24"></i>
                    <span class="menu-item-label">Contents</span>
                    <i class="menu-item-arrow fa fa-angle-down"></i>
                </div>
            </a>
            <ul class="br-menu-sub nav flex-column">
                <?php if (haveRole($this->session->get('group'), 18)) { ?><li class="nav-item"><a href="<?= $this->url->get('/content/page') ?>" class="call nav-link <?php if ($menu == 'page') { ?>active<?php } ?>">Pages</a></li><?php } ?>
                <?php if (haveRole($this->session->get('group'), 22) || haveRole($this->session->get('group'), 23)) { ?><li class="nav-item"><a href="<?= $this->url->get('/content/post') ?>" class="call nav-link <?php if ($menu == 'post') { ?>active<?php } ?>">Posts</a></li><?php } ?>
                <?php if (haveRole($this->session->get('group'), 29)) { ?><li class="nav-item"><a href="<?= $this->url->get('/content/category') ?>" class="call nav-link <?php if ($menu == 'category') { ?>active<?php } ?>">Categories</a></li><?php } ?>
                <?php if (haveRole($this->session->get('group'), 32) || haveRole($this->session->get('group'), 33)) { ?><li class="nav-item"><a href="<?= $this->url->get('/content/media') ?>" class="call nav-link <?php if ($menu == 'media') { ?>active<?php } ?>">Media</a></li><?php } ?>
                <?php if (haveRole($this->session->get('group'), 37)) { ?><li class="nav-item"><a href="<?= $this->url->get('/content/slider') ?>" class="call nav-link <?php if ($menu == 'slider') { ?>active<?php } ?>">Slider</a></li><?php } ?>
                <?php if (haveRole($this->session->get('group'), 40) || haveRole($this->session->get('group'), 41)) { ?><li class="nav-item"><a href="<?= $this->url->get('/content/comment') ?>" class="call nav-link <?php if ($menu == 'comment') { ?>active<?php } ?>">Comments</a></li><?php } ?>
            </ul>
        <?php } ?>

        <?php if (haveRole($this->session->get('group'), 7)) { ?>
            <a href="#" class="call br-menu-link <?php if ($menu == 'system' || $menu == 'setting' || $menu == 'group' || $menu == 'user') { ?>active show-sub<?php } ?>">
                <div class="br-menu-item">
                    <i class="menu-item-icon icon ion-ios-gear-outline tx-24"></i>
                    <span class="menu-item-label">System</span>
                    <i class="menu-item-arrow fa fa-angle-down"></i>
                </div>
            </a>
            <ul class="br-menu-sub nav flex-column">
                <?php if (haveRole($this->session->get('group'), 16)) { ?><li class="nav-item"><a href="<?= $this->url->get('/system/setting') ?>" class="call nav-link <?php if ($menu == 'setting') { ?>active<?php } ?>">Settings</a></li><?php } ?>
                <?php if (haveRole($this->session->get('group'), 12)) { ?><li class="nav-item"><a href="<?= $this->url->get('/system/group') ?>" class="call nav-link <?php if ($menu == 'group') { ?>active<?php } ?>">Groups</a></li><?php } ?>
                <?php if (haveRole($this->session->get('group'), 8)) { ?><li class="nav-item"><a href="<?= $this->url->get('/system/user') ?>" class="call nav-link <?php if ($menu == 'user') { ?>active<?php } ?>">User</a></li><?php } ?>
            </ul>
        <?php } ?>
    </div>


    <br>
</div><!-- br-sideleft -->
<!-- ########## END: LEFT PANEL ########## -->

<!-- ########## START: HEAD PANEL ########## -->
<div class="br-header">
    <div class="br-header-left">
        <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="icon ion-navicon-round"></i></a></div>
        <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="icon ion-navicon-round"></i></a></div>
        <div class="input-group hidden-xs-down wd-170 transition">
            <input id="searchbox" type="text" class="form-control" placeholder="Search">
            <span class="input-group-btn">
            <button class="btn btn-secondary" type="button"><i class="fa fa-search"></i></button>
          </span>
        </div><!-- input-group -->
    </div><!-- br-header-left -->
    <div class="br-header-right">
        <nav class="nav">
            <div class="dropdown">
                <a href="" class="nav-link pd-x-7 pos-relative" data-toggle="dropdown">
                    <i class="icon ion-ios-email-outline tx-24"></i>
                    <!-- start: if statement -->
                    <span class="square-8 bg-danger pos-absolute t-15 r-0 rounded-circle"></span>
                    <!-- end: if statement -->
                </a>
                <div class="dropdown-menu dropdown-menu-header wd-300 pd-0-force">
                    <div class="d-flex align-items-center justify-content-between pd-y-10 pd-x-20 bd-b bd-gray-200">
                        <label class="tx-12 tx-info tx-uppercase tx-semibold tx-spacing-2 mg-b-0">Messages</label>
                        <a href="" class="tx-11">+ Add New Message</a>
                    </div><!-- d-flex -->

                    <div class="media-list">
                        <!-- loop starts here -->
                        <a href="" class="media-list-link">
                            <div class="media pd-x-20 pd-y-15">
                                <img src="http://via.placeholder.com/280x280" class="wd-40 rounded-circle" alt="">
                                <div class="media-body">
                                    <div class="d-flex align-items-center justify-content-between mg-b-5">
                                        <p class="mg-b-0 tx-medium tx-gray-800 tx-14">Donna Seay</p>
                                        <span class="tx-11 tx-gray-500">2 minutes ago</span>
                                    </div><!-- d-flex -->
                                    <p class="tx-12 mg-b-0">A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring.</p>
                                </div>
                            </div><!-- media -->
                        </a>
                        <!-- loop ends here -->
                        <a href="" class="media-list-link read">
                            <div class="media pd-x-20 pd-y-15">
                                <img src="http://via.placeholder.com/280x280" class="wd-40 rounded-circle" alt="">
                                <div class="media-body">
                                    <div class="d-flex align-items-center justify-content-between mg-b-5">
                                        <p class="mg-b-0 tx-medium tx-gray-800 tx-14">Samantha Francis</p>
                                        <span class="tx-11 tx-gray-500">3 hours ago</span>
                                    </div><!-- d-flex -->
                                    <p class="tx-12 mg-b-0">My entire soul, like these sweet mornings of spring.</p>
                                </div>
                            </div><!-- media -->
                        </a>
                        <a href="" class="media-list-link read">
                            <div class="media pd-x-20 pd-y-15">
                                <img src="http://via.placeholder.com/280x280" class="wd-40 rounded-circle" alt="">
                                <div class="media-body">
                                    <div class="d-flex align-items-center justify-content-between mg-b-5">
                                        <p class="mg-b-0 tx-medium tx-gray-800 tx-14">Robert Walker</p>
                                        <span class="tx-11 tx-gray-500">5 hours ago</span>
                                    </div><!-- d-flex -->
                                    <p class="tx-12 mg-b-0">I should be incapable of drawing a single stroke at the present moment...</p>
                                </div>
                            </div><!-- media -->
                        </a>
                        <a href="" class="media-list-link read">
                            <div class="media pd-x-20 pd-y-15">
                                <img src="http://via.placeholder.com/280x280" class="wd-40 rounded-circle" alt="">
                                <div class="media-body">
                                    <div class="d-flex align-items-center justify-content-between mg-b-5">
                                        <p class="mg-b-0 tx-medium tx-gray-800 tx-14">Larry Smith</p>
                                        <span class="tx-11 tx-gray-500">Yesterday</span>
                                    </div><!-- d-flex -->
                                    <p class="tx-12 mg-b-0">When, while the lovely valley teems with vapour around me, and the meridian sun strikes...</p>
                                </div>
                            </div><!-- media -->
                        </a>
                        <div class="pd-y-10 tx-center bd-t">
                            <a href="" class="tx-12"><i class="fa fa-angle-down mg-r-5"></i> Show All Messages</a>
                        </div>
                    </div><!-- media-list -->
                </div><!-- dropdown-menu -->
            </div><!-- dropdown -->
            <div class="dropdown">
                <a href="" class="nav-link pd-x-7 pos-relative" data-toggle="dropdown">
                    <i class="icon ion-ios-bell-outline tx-24"></i>
                    <!-- start: if statement -->
                    <span class="square-8 bg-danger pos-absolute t-15 r-5 rounded-circle"></span>
                    <!-- end: if statement -->
                </a>
                <div class="dropdown-menu dropdown-menu-header wd-300 pd-0-force">
                    <div class="d-flex align-items-center justify-content-between pd-y-10 pd-x-20 bd-b bd-gray-200">
                        <label class="tx-12 tx-info tx-uppercase tx-semibold tx-spacing-2 mg-b-0">Notifications</label>
                        <a href="" class="tx-11">Mark All as Read</a>
                    </div><!-- d-flex -->

                    <div class="media-list">
                        <!-- loop starts here -->
                        <a href="" class="media-list-link read">
                            <div class="media pd-x-20 pd-y-15">
                                <img src="http://via.placeholder.com/280x280" class="wd-40 rounded-circle" alt="">
                                <div class="media-body">
                                    <p class="tx-13 mg-b-0 tx-gray-700"><strong class="tx-medium tx-gray-800">Suzzeth Bungaos</strong> tagged you and 18 others in a post.</p>
                                    <span class="tx-12">October 03, 2017 8:45am</span>
                                </div>
                            </div><!-- media -->
                        </a>
                        <!-- loop ends here -->
                        <a href="" class="media-list-link read">
                            <div class="media pd-x-20 pd-y-15">
                                <img src="http://via.placeholder.com/280x280" class="wd-40 rounded-circle" alt="">
                                <div class="media-body">
                                    <p class="tx-13 mg-b-0 tx-gray-700"><strong class="tx-medium tx-gray-800">Julius Erving</strong> wants to connect with you on your conversation with <strong class="tx-medium tx-gray-800">Ronnie Mara</strong></p>
                                    <span class="tx-12">October 01, 2017 6:08pm</span>
                                </div>
                            </div><!-- media -->
                        </a>
                        <div class="pd-y-10 tx-center bd-t">
                            <a href="" class="tx-12"><i class="fa fa-angle-down mg-r-5"></i> Show All Notifications</a>
                        </div>
                    </div><!-- media-list -->
                </div><!-- dropdown-menu -->
            </div>

            <div class="dropdown">
                <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
                    <span class="logged-name hidden-md-down"><?= $this->session->get('name') ?></span>
                    <?php if ($this->session->get('photo') != '') { ?>
                        <img src="<?= $this->url->get('/files/upload/profile/') ?><?= $this->session->get('photo') ?>" class="wd-32 rounded-circle profile" alt="">
                    <?php } else { ?>
                        <img src="<?= $this->url->get('/img/user.png') ?>" class="wd-32 rounded-circle profile" alt="">
                    <?php } ?>
                    <span class="square-10 bg-success"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-header wd-200">
                    <ul class="list-unstyled user-profile-nav">
                        <?php if (haveRole($this->session->get('group'), 3)) { ?><li><a class="call" href="<?= $this->url->get('/profile') ?>"><i class="icon ion-ios-person"></i>Profile Saya</a></li><?php } ?>
                        <?php if (haveRole($this->session->get('group'), 6)) { ?><li><a class="call" href="<?= $this->url->get('/profile/setting') ?>"><i class="icon ion-ios-gear"></i> Pengaturan Akun</a></li><?php } ?>
                        <?php if (haveRole($this->session->get('group'), 5)) { ?><li><a class="call" href="<?= $this->url->get('/profile/password') ?>"><i class="icon ion-ios-download"></i> Ubah Password</a></li><?php } ?>
                        <li><a href="<?= $this->url->get('/out') ?>"><i class="icon ion-power"></i> Sign Out</a></li>
                    </ul>
                </div>
            </div>
        </nav>

    </div>
</div>
<!-- ########## END: HEAD PANEL ########## -->

<div class="br-mainpanel">
    <div id="overlay">
        <div class="d-flex  ht-100p pos-relative align-items-center">
            <div class="sk-three-bounce">
                <div class="sk-child sk-bounce1 bg-gray-800"></div>
                <div class="sk-child sk-bounce2 bg-gray-800"></div>
                <div class="sk-child sk-bounce3 bg-gray-800"></div>
                <div class="text">Please wait...</div>
            </div>
        </div>
    </div>
    <div id="main">
        <?= $this->getContent() ?>
    </div>

</div>
<script>
    $(function() {
        var load = function(url) {
            $('#overlay').toggle();
            $.get(url).done(function(data) {
                $("#main").html(data);
                $('#overlay').toggle();
                $.get("<?= $this->url->get('/dashboard/pageInfo/') ?>"+encodeURIComponent(url)).done(function(title) {
                    document.title = title;
                })

            })
        };

        $(document).on('click', 'a.call', function(e) {
            e.preventDefault();
            var $this = $(this),
                url = $this.attr("href"),
                title = $this.text();

            $('.nav-link').removeClass("active");
            $('.br-menu-link').removeClass("active");
            $('.br-menu-link').removeClass("show-sub");
            $('.br-menu-sub').hide();


            $(this).addClass("active");
            $(this).parent().parent().prev().addClass('active');
            $(this).parent().parent().prev().addClass('show-sub');
            $(this).parent().parent().show();

            history.pushState({
                url: url,
                title: title
            }, title, url);

            load(url);
            if (typeof summernote !== 'undefined' && $.isFunction(summernote)) {
                $('#summernote').summernote('destroy');
            }
            $('.note-popover').remove();
            $("html, body").animate({
                scrollTop: 0
            }, 600);
        });

        $(window).on('popstate', function(e) {
            var state = e.originalEvent.state;
            if (state !== null) {
                document.title = state.title;
                load(state.url);
            } else {
                document.title = 'World Regions';
                $("#content").empty();
            }
        });
    });
</script>
