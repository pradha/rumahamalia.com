<div class="mg-b-10">
    Ditemukan sebanyak : <strong><?= $this->length($media) ?></strong> data, Dari hasil pencarian : "<strong><?= $search ?></strong>"  <a title="hapus pencarian" class="btn btn-warning rounded-10 pd-x-10 pd-y-1" href="<?= $this->url->get('/content/media') ?>"><i class="icon fa fa-close"></i> </a>
</div>
<table class="table table-hover table-bordered table-responsive">
    <thead class="thead-colored thead-teal">
    <tr>
        <th class="text-light">#</th>
        <th class="text-light wd-5p-force">Media</th>
        <th class="text-light">Name</th>
        <th class="text-light">Description</th>
        <th class="text-light">Keywords</th>
        <th class="text-light">File Type</th>
        <th class="text-light">Size</th>
        <th class="text-light">Created</th>
        <th class="text-light">Updated</th>
    </tr>
    </thead>
    <tbody class="bg-lightsky">
    <?php foreach ($media as $data) { ?>
        <tr>
            <td>
                <a title="Edit" class="btn btn-teal pd-y-2 pg-x-3" href="<?= $this->url->get('/content/media/edit/') ?><?= $data->id ?>"><i class="icon ion-edit"></i> </a>
            </td>
            <td>
                <?php if ($data->content_type == 'image/png' || $data->content_type == 'image/jpg' || $data->content_type == 'image/jpeg' || $data->content_type == 'image/gif') { ?>
                    <img class="img img-fluid" src="<?= $this->url->get('/files/upload/') ?><?= date('Y', strtotime($data->created_at)) ?>/<?= date('m', strtotime($data->created_at)) ?>/<?= $data->original_file ?>">
                <?php } elseif ($data->content_type == 'application/pdf') { ?>
                    <img class="img img-fluid" src="<?= $this->url->get('/img/pdf.png') ?>">
                <?php } elseif ($data->content_type == 'text/plain') { ?>
                    <img class="img img-fluid" src="<?= $this->url->get('/img/text.png') ?>">
                <?php } else { ?>
                    <img class="img img-fluid" src="<?= $this->url->get('/img/file.png') ?>">
                <?php } ?>
            </td>
            <td><?= $data->name ?></td>
            <td><?= $data->description ?></td>
            <td><?= tags($data->keywords) ?></td>
            <td><?= $data->content_type ?></td>
            <td class="text-right"><?= fileSizeUnit($data->file_size) ?></td>
            <td><?= date('d M Y H:i:s', strtotime($data->created_at)) ?> / <?= $data->created_by ?></td>
            <td><?= date('d M Y H:i:s', strtotime($data->updated_at)) ?> / <?= $data->updated_by ?></td>
        </tr>
    <?php } ?>
    </tbody>