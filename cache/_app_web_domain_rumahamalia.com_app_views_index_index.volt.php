
<div class="ra-nav-index hidden-md-down ">
    <div class="container">
        <div class="ht-65 d-flex align-items-center justify-content-end justify-content-between">
            <h4 class="mg-b-0 tx-uppercase tx-bold tx-spacing--2 tx-inverse tx-poppins mg-r-auto">
                <a href="<?= $this->url->get() ?>">
                    <img src="<?= $this->url->get('/img/logo-bg.png') ?>" class="ht-50 mg-r-10">
                    Rumah Amalia
                </a>
            </h4>
            <ul class="nav active-info tx-uppercase tx-12 tx-medium tx-spacing-2 flex-column flex-sm-row" role="tablist">
                <li class="nav-item"><a class="nav-link"  href="<?= $this->url->get() ?>" role="tab">Beranda</a></li>
                <?php foreach ($pages as $page) { ?>
                    <li class="nav-item"><a class="nav-link"  href="<?= $this->url->get('/page/') ?><?= $page->slug ?>" role="tab"><?= $page->title ?></a></li>
                <?php } ?>
            </ul>
            <div class="social mg-l-20">
                <a href="https://www.facebook.com/Dahsyatnya.Doa" target="_blank" class="hover-dark tx-16 mg-l-10"><i class="fa fa-facebook"></i></a>
                <a href="http://instagram.com/dahsyatnya.doa" target="_blank" class="hover-dark tx-16 mg-l-10"><i class="fa fa-instagram"></i></a>
            </div>
        </div>
    </div>
</div>
<div class="ra-nav-index hidden-lg-up ">
    <div class="container">
        <div class="ht-65 d-flex align-items-center justify-content-between">
            <a href="#"  class="tx-gray-600 hover-dark openSidebar tx-18"><i class="fa fa-navicon"></i></a>
            <h4 class="mg-b-0 tx-uppercase tx-bold tx-spacing--2 tx-inverse tx-poppins mg-r-20">
                <a href="<?= $this->url->get() ?>">
                    Rumah Amalia
                </a>
            </h4>
            <div>

            </div>
        </div>
    </div>

</div>

<div class="slider">
    <?php foreach ($sliders as $slider) { ?>
        <div>
            <img src="<?= $slider->image ?>">

            <div class="content">
                <div class="container">
                    <div class="title"><?= $slider->title ?></div>
                    <div class="description"><?= $slider->description ?></div>
                </div>
            </div>

        </div>
    <?php } ?>
</div>

<script>
    $(document).ready(function(){
        $('.openSidebar').on('click',function(){
            $(".sidebar").toggle("slide", { direction: "left" }, 500);
        });
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();
            if (scroll >= 500) {
                $('.ra-nav-index a').css('color', '#363636');
                $('.ra-nav-index').css('background-color', 'rgba(250, 250, 250, 1)');

            } else {
                $('.ra-nav-index').css('background-color', 'rgba(0, 0, 0, 0.5)');
                $('.ra-nav-index a').css('color', '#ffffff');
            }
        });
        $('.course_list').on('afterChange', function(event, slick, currentSlide, nextSlide){
            var maxHeight = Math.max.apply(null, $(".course_item.slick-active").map(function ()
            {
                return $(this).height();
            }).get());
            $(this).animate({height: maxHeight + 30}, 500).css({ overflow:'visible'});

        });
        $('.slider').slick({
            autoplay:true,
            arrows:false,
            fade:true,
            mobileFirst:true,
            pauseOnFocus:false,
            pauseOnHover:false,
            speed: 1000,
            cssEase:'ease'
        });
    });
</script>

<div class="container mg-t-15">
    <div class="page-header">
        <h1 class="page-header">Dahsyatnya Doa Rumah Amalia</h1>
    </div>
    <hr>
    <p>
        Assalamu'alaikum, Selamat Datang! Bagi sahabat yang baru bergabung bersama kami, Keluarga Besar Rumah Amalia. Rumah Amalia adalah rumah belajar untuk anak yatim dan anak kaum dhuafa. ada 90 anak Rumah Amalia. Program kegiatan Studi Islam, akhlak, Leadership, kemandirian. (Bukan panti asuhan)
    </p>
    <p>
        Rumah Amalia beralamat di Jl. Subagyo IV Blok ii, No. 24 Komplek Peruri, Ciledug, Tangerang. Info selengkapnya silahkan SMS/WA ke 087 8777 12 431 (nomor hape Ust. M. Agus Syafii).
    </p>
    <p>
        4 Program Rumah Amalia untuk layanan masyarakat:
        <ol>
            <li>Pendidikan</li>
            <li>Konsultasi jodoh Dan keluarga</li>
            <li>"Fun Therapy"</li>
            <li> Kreativa (Kreativitas & Kerajinan )</li>
        </ol>
    </p>
    <p>
        KONSULTASI JODOH & KELUARGA: Bila anda ingin berkonsultasi jodoh dan keluarga pada Ustadz Muhammad Agus Syafii Silahkan SMS/WA ke 087877712431 Atau Silahkan datang langsung ke Rumah Amalia di Jl. Subagyo IV Blok ii, No. 24 Komplek Peruri, Ciledug, Tangerang. 15151 (untuk datang langsung mohon konfirmasi dulu sebelumnya via SMS/WA 087877712431)
    </p>
    <p>
        Terima kasih sahabat atas perhatiannya kepada kami. Teriring doa semoga Allah melimpahkan kesehatan, rizki, keberkahan dan kebahagiaan selalu untuk anda & keluarga, Amin ya robbal alamin
    </p>
    <hr>
    <h2>Pembaruan</h2>
    <div class="row no-gutters vdivide">
        <?php foreach ($posts as $post) { ?>
            <div class="col-sm-3 col-xs-12  d-flex align-items-stretch">
                <div class="card">
                    <img class="card-img-top img-fluid" src="<?= firstImage($post->content) ?>" alt="Image">
                    <div class="card-body">
                        <h5 class="card-title"><a href="<?= $this->url->get('/read/') ?><?= $post->slug ?>"><?= $post->title ?></a></h5>
                        <p class="card-text"><?= $post->description ?></p>
                    </div>
                </div>
            </div>

        <?php } ?>
    </div>
</div>

<footer class="footer-distributed">
    <div class="container">
        <div class="footer-left">

            <h3><img src="<?= $this->url->get('/img/logo-bg.png') ?>" class="ht-100 mg-r-20"> Rumah Amalia</h3>

            <p class="footer-links">
                <a href="">Beranda</a> .
                <?php $v153298751881276578131iterator = $pages; $v153298751881276578131incr = 0; $v153298751881276578131loop = new stdClass(); $v153298751881276578131loop->self = &$v153298751881276578131loop; $v153298751881276578131loop->length = count($v153298751881276578131iterator); $v153298751881276578131loop->index = 1; $v153298751881276578131loop->index0 = 1; $v153298751881276578131loop->revindex = $v153298751881276578131loop->length; $v153298751881276578131loop->revindex0 = $v153298751881276578131loop->length - 1; ?><?php foreach ($v153298751881276578131iterator as $page) { ?><?php $v153298751881276578131loop->first = ($v153298751881276578131incr == 0); $v153298751881276578131loop->index = $v153298751881276578131incr + 1; $v153298751881276578131loop->index0 = $v153298751881276578131incr; $v153298751881276578131loop->revindex = $v153298751881276578131loop->length - $v153298751881276578131incr; $v153298751881276578131loop->revindex0 = $v153298751881276578131loop->length - ($v153298751881276578131incr + 1); $v153298751881276578131loop->last = ($v153298751881276578131incr == ($v153298751881276578131loop->length - 1)); ?>
                    <a href="<?= $this->url->get('/page/') ?><?= $page->slug ?>"><?= $page->title ?></a>
                    <?php if ($v153298751881276578131loop->last == false) { ?>
                        .
                    <?php } ?>
                <?php $v153298751881276578131incr++; } ?>
            </p>

            <p class="footer-company-name">Rumah Amalia &copy; 2018. All rights reserved</p>
        </div>

        <div class="footer-center">

            <div>
                <i class="fa fa-map-marker"></i>
                <p><span>Jl. Subagyo IV Blok ii, No. 24 Komplek Peruri</span> Ciledug, Tangerang. 15151</p>
            </div>

            <div>
                <i class="fa fa-phone"></i>
                <p>+62 878 777 124 31</p>
            </div>

            <div>
                <i class="fa fa-envelope"></i>
                <p><a href="mailto:agussyafii@rumahamalia.com">info@rumahamalia.com</a></p>
            </div>

        </div>

        <div class="footer-right">

            <p class="footer-company-about">
                <span>Sekilas Rumah Amalia</span>
                Rumah Amalia adalah Komunitas rumah belajar untuk anak Yatim & anak Dhuafa, berjumlah 90 anak amalia.
            </p>

            <div class="footer-icons">
                <a target="_blank" href="https://facebook.com/Dahsyatnya.Doa"><i class="fa fa-facebook"></i></a>
                <a target="_blank" href="http://instagram.com/dahsyatnya.doa"><i class="fa fa-instagram"></i></a>
            </div>

        </div>
    </div>

</footer>
<div class="sidebar  bg-gray-900 ">
    <a href="#"  class="tx-gray-600 hover-dark openSidebar tx-18"><i class="fa fa-close"></i></a>
    <div class="pd-10">
        <ul class="nav nav-pills nav-pills-for-dark flex-column" role="tablist">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="<?= $this->url->get() ?>" role="tab">Home</a></li>
            <?php $v153298751881276578131iterator = $pages; $v153298751881276578131incr = 0; $v153298751881276578131loop = new stdClass(); $v153298751881276578131loop->self = &$v153298751881276578131loop; $v153298751881276578131loop->length = count($v153298751881276578131iterator); $v153298751881276578131loop->index = 1; $v153298751881276578131loop->index0 = 1; $v153298751881276578131loop->revindex = $v153298751881276578131loop->length; $v153298751881276578131loop->revindex0 = $v153298751881276578131loop->length - 1; ?><?php foreach ($v153298751881276578131iterator as $page) { ?><?php $v153298751881276578131loop->first = ($v153298751881276578131incr == 0); $v153298751881276578131loop->index = $v153298751881276578131incr + 1; $v153298751881276578131loop->index0 = $v153298751881276578131incr; $v153298751881276578131loop->revindex = $v153298751881276578131loop->length - $v153298751881276578131incr; $v153298751881276578131loop->revindex0 = $v153298751881276578131loop->length - ($v153298751881276578131incr + 1); $v153298751881276578131loop->last = ($v153298751881276578131incr == ($v153298751881276578131loop->length - 1)); ?>
                <li class="nav-item"><a class="nav-link" href="<?= $this->url->get('/page/') ?><?= $page->slug ?>"><?= $page->title ?></a></li>
            <?php $v153298751881276578131incr++; } ?>
        </ul>
    </div>
</div>