<div class="pd-y-50-force mg-x-20-force text-center">
    <h1 class="tx-80">ERROR 404</h1>
    <h2>NOT FOUND</h2>
    <img src="<?= $this->url->get('/img/monster.png') ?>" class="img mg-y-10-force ht-350">
    <h1>Oops...!Brain not found<br> <small>Let's check your<br> <a href="<?= $this->url->get() ?>">Home</a></small></h1>
</div>
