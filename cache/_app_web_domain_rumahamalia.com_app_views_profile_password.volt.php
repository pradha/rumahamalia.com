<script src="<?= $this->url->get('/lib/parsleyjs/parsley.js') ?>"></script>
<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="<?= $this->url->get('/dashboard') ?>">Main Dashboard</a>
        <span class="breadcrumb-item">Profile</span>
        <a class="breadcrumb-item active" href="<?= $this->url->get('/profile/password') ?>">Change Password</a>
    </nav>
</div><!-- br-pageheader -->
<div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
    <h4 class="tx-gray-800 mg-b-5">Ubah Password</h4>
    <p class="mg-b-0">Ubah Password Pengguna</p>
</div>

<div class="br-pagebody">
    <?php if (isset($status) && $status == 'success') { ?>
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong class="d-block d-sm-inline-block-force">Success!</strong> Password anda telah berhasil diubah.
        </div><!-- alert -->
    <?php } elseif (isset($error->password) || isset($error->length) || $error->repeat) { ?>
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong class="d-block d-sm-inline-block-force">Oops!</strong> Silahkan periksa kembali kesalahan berikut :
            <ul class="mg-b-0">
                <?php if (isset($error->password)) { ?><li>Password lama anda salah</li><?php } ?>
                <?php if (isset($error->length)) { ?><li>Panjang password harus lebih dari 6 karakter (kombinasikan huruf dengan angka demi keamanan) </li><?php } ?>
                <?php if (isset($error->repeat)) { ?><li>Ulangi password tidak sama dengan password</li><?php } ?>
            </ul>
        </div>
    <?php } ?>
    <form method="post" action="<?= $this->url->get('/profile/password') ?>" class="form-layout form-layout-1 bg-white" data-parsley-validate>
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-control-label">Password Lama: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="password" name="old" value="" placeholder="Masukan password lama" required="" autocomplete="off">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-control-label">Password Baru: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="password" name="new" value="" placeholder="Masukan password baru" required="" autocomplete="off">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-control-label">Ulangi Password Baru: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="password" name="renew" value="" placeholder="Ulangi password baru" required="" autocomplete="off">
                </div>
            </div>
        </div>

        <div class="form-layout-footer">
            <button type="submit" class="btn btn-teal">Create</button>
        </div>
    </form>

</div>

<script>
    $(document).ready(function(){
        $("form").submit(function(e) {
            e.preventDefault();
            $form = $(this);
            $('#overlay').toggle();
            $.post(
                $form.attr('action'),
                $(this).serialize(),
                function(result){
                    $("#main").html(result);
                    $('#overlay').toggle();
                })
                .fail(function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                });

        });
    });
</script>