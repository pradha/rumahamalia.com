<link href="<?= $this->url->get('/css/dropzone.css') ?>" rel="stylesheet">
<script src="<?= $this->url->get('/js/dropzone.js') ?>"></script>
<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="<?= $this->url->get('/dashboard') ?>">Main Dashboard</a>
        <span class="breadcrumb-item">Contents</span>
        <a class="breadcrumb-item" href="<?= $this->url->get('/content/media') ?>">Media</a>
        <a class="breadcrumb-item active" href="<?= $this->url->get('/content/media/add') ?>">Add</a>
    </nav>
</div><!-- br-pageheader -->
<div class="row no-gutters">
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <h4 class="tx-gray-800 mg-b-5">
            Tambah Media
            <a href="<?= $this->url->get('/content/media') ?>" class="btn btn-teal btn-with-icon">
                <div class="ht-25">
                    <span class="icon wd-25"><i class="fa fa-list-ul"></i></span>
                    <span class="pd-x-15">Daftar Media</span>
                </div>
            </a>
        </h4>
        <p class="mg-b-0">Tambah Media (file gambar, dokumen, dll)</p>
    </div>

</div>

<hr>
<div class="br-pagebody pd-sm-x-30">
    <form action="upload.php" class="dropzone" id="dropzonewidget">
    </form>

</div>

<script>
    Dropzone.autoDiscover = false;
    $("form.dropzone").dropzone({
        url: "<?= $this->url->get('/content/media/upload') ?>",
        maxFilesize: 2,
        maxThumbnailFilesize: 5,
        init: function() {
            this.on("success", function(file, response) {
                response=JSON.parse(response);
                console.log(response);
                if(response.status == 'success')
                {
                    this.defaultOptions.success(file,'success');
                    $('<a class="btn btn-teal pd-3 mg-t-5 tx-10" target="_blank" href="'+response.url+'"">Preview</a>').insertAfter($(file.previewTemplate).find('.dz-filename').off());

                }
                else
                {
                    this.defaultOptions.error(file, response.message);
                }
            })
        }
    });
</script>