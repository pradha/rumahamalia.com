<link href="<?= $this->url->get('/lib/bootstrap-tagsinput/bootstrap-tagsinput.css') ?>" rel="stylesheet">

<script src="<?= $this->url->get('/lib/parsleyjs/parsley.js') ?>"></script>
<script src="<?= $this->url->get('/lib/bootstrap-tagsinput/bootstrap-tagsinput.js') ?>"></script>


<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="<?= $this->url->get('/dashboard') ?>">Main Dashboard</a>
        <span class="breadcrumb-item">Contents</span>
        <a class="breadcrumb-item" href="<?= $this->url->get('/content/media') ?>">Media</a>
        <a class="breadcrumb-item active" href="<?= $this->url->get('/content/media/edit/') ?><?= $id ?>">Edit</a>
    </nav>
</div><!-- br-pageheader -->
<div class="row no-gutters">
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <h4 class="tx-gray-800 mg-b-5">
            Edit Media
            <a href="<?= $this->url->get('/content/media') ?>" class="btn btn-teal btn-with-icon">
                <div class="ht-25">
                    <span class="icon wd-25"><i class="fa fa-list-ul"></i></span>
                    <span class="pd-x-15">Daftar Media</span>
                </div>
            </a>
        </h4>
        <p class="mg-b-0">Edit Media (file gambar, dokumen, dll)</p>
    </div>

</div>

<hr>
<div class="br-pagebody pd-sm-x-30">
    <div class="row bg-white form-layout form-layout-1 vdivide">
        <div class="col-lg-4">
            <?php if ($media->content_type == 'image/png' || $media->content_type == 'image/jpg' || $media->content_type == 'image/jpeg' || $media->content_type == 'image/gif') { ?>
                <img class="img img-fluid" src="<?= $this->url->get('/files/upload/') ?><?= date('Y', strtotime($media->created_at)) ?>/<?= date('m', strtotime($media->created_at)) ?>/<?= $media->original_file ?>">
            <?php } elseif ($media->content_type == 'application/pdf') { ?>
                <img class="img img-fluid" src="<?= $this->url->get('/img/pdf.png') ?>">
            <?php } elseif ($data->content_type == 'text/plain') { ?>
                <img class="img img-fluid" src="<?= $this->url->get('/img/text.png') ?>">
            <?php } else { ?>
                <img class="img img-fluid" src="<?= $this->url->get('/img/file.png') ?>">
            <?php } ?>
        </div>
        <div class="col-lg-8">
            <?php if (isset($status) && $status == 'success') { ?>
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong class="d-block d-sm-inline-block-force">Success!</strong> Media telah berhasil diperbarui.
                </div><!-- alert -->
            <?php } elseif (isset($error->email) || isset($error->name) || isset($error->phone) || isset($error->gender) || isset($error->date_of_birth) || isset($error->biography)) { ?>
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong class="d-block d-sm-inline-block-force">Oops!</strong> Silahkan periksa kembali kesalahan berikut :
                    <ul class="mg-b-0">
                        <?php if (isset($error->email)) { ?><li>E-Mail yang digunakan telah terdaftar</li><?php } ?>
                        <?php if (isset($error->name)) { ?><li>Nama masih kosong</li><?php } ?>
                        <?php if (isset($error->phone)) { ?><li>No telepon masih kosong</li><?php } ?>
                        <?php if (isset($error->gender)) { ?><li>Jenis kelamin belum dipilih</li><?php } ?>
                        <?php if (isset($error->date_of_birth)) { ?><li>Tanggal lahir masih kosong</li><?php } ?>
                        <?php if (isset($error->biography)) { ?><li>Biography masih kosong</li><?php } ?>
                    </ul>
                </div>
            <?php } ?>
            <form method="post" action="<?= $this->url->get('/content/media/edit/') ?><?= $id ?>"  data-parsley-validate>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="form-control-label">Media Name/Title: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="name" value="<?php if ($this->request->isPost()) { ?><?= $this->request->getPost('name') ?><?php } else { ?><?= $media->name ?><?php } ?>" placeholder="Media name/title" required autocomplete="off">

                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group mg-b-10-force">
                            <label class="form-control-label">Description: <span class="tx-danger">*</span></label>
                            <textarea rows="3" class="form-control" name="description" placeholder="Description" required autocomplete="off"><?php if ($this->request->isPost()) { ?><?= $this->request->getPost('description') ?><?php } else { ?><?= $media->description ?><?php } ?></textarea>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group mg-b-10-force">
                            <label class="form-control-label">keywords: <span class="tx-danger">*</span></label>
                            <input class="form-control" type="text" name="keywords" value="<?php if ($this->request->isPost()) { ?><?= $this->request->getPost('keywords') ?><?php } else { ?><?= $media->keywords ?><?php } ?>" placeholder="Keywords or Tags" data-role="tagsinput" required>
                        </div>
                    </div>
                </div>

                <div class="form-layout-footer">
                    <button type="submit" class="btn btn-teal">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

        $("form").submit(function(e) {
            e.preventDefault();
            $form = $(this);
            $('#overlay').toggle();
            $.post(
                $form.attr('action'),
                $(this).serialize(),
                function(result){
                    $("#main").html(result);
                    $('#overlay').toggle();
                })
                .fail(function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                });

        });
    });
</script>