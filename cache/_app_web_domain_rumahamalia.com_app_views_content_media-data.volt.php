<div class="row">
    <?php foreach ($page->items as $data) { ?>
    <div class="col-lg-1 col-md-2 col-xs-12 col-sm-3 mg-b-5-force">
        <div class="crop" data-content="<?= $data->id ?>">
            <?php if ($data->content_type == 'image/png' || $data->content_type == 'image/jpg' || $data->content_type == 'image/jpeg' || $data->content_type == 'image/gif') { ?>
                <img class="img img-fluid" src="<?= $this->url->get('/files/upload/') ?><?= date('Y', strtotime($data->created_at)) ?>/<?= date('m', strtotime($data->created_at)) ?>/<?= $data->original_file ?>">
            <?php } elseif ($data->content_type == 'application/pdf') { ?>
                <img class="img img-fluid" src="<?= $this->url->get('/img/pdf.png') ?>">
            <?php } elseif ($data->content_type == 'text/plain') { ?>
                <img class="img img-fluid" src="<?= $this->url->get('/img/text.png') ?>">
            <?php } else { ?>
                <img class="img img-fluid" src="<?= $this->url->get('/img/file.png') ?>">
            <?php } ?>
        </div>
    </div>
    <?php } ?>
</div>

<script>
    $('.crop').on('click',function(){
        $.get("<?= $this->url->get('/content/media/insert/') ?>"+$(this).attr('data-content'), function(data, status){
            $('#data').html(data);
        });
    });
</script>