<div class="mg-b-10">
    Ditemukan sebanyak : <strong><?= $this->length($users) ?></strong> data, Dari hasil pencarian : "<strong><?= $search ?></strong>"  <a title="hapus pencarian" class="btn btn-warning rounded-10 pd-x-10 pd-y-1" href="<?= $this->url->get('/system/group') ?>"><i class="icon fa fa-close"></i> </a>
</div>
<table class="table table-hover table-bordered">
    <thead class="thead-colored thead-teal">
    <tr>
        <th class="text-light wd-10p-force">#</th>
        <th class="text-light">Name</th>
        <th class="text-light">Description</th>
        <th class="text-light">Created by</th>
        <th class="text-light">Created at</th>
    </tr>
    </thead>
    <tbody class="bg-lightsky">
    <?php foreach ($groups as $data) { ?>
        <tr>
            <td>
                <a title="edit" class="btn btn-teal pd-y-2 pg-x-3" href="<?= $this->url->get('/system/group/edit/') ?><?= encode($data->id) ?>"><i class="icon ion-edit"></i> </a>
                <a title="roles" class="btn btn-warning pd-y-2 pg-x-3" href="<?= $this->url->get('/system/role/') ?><?= $data->id ?>"><i class="icon ion-lock-combination"></i> </a>
            </td>
            <td><?= $data->name ?></td>
            <td><?= $data->description ?></td>
            <td><?= date('d M Y H:i:s', strtotime($data->created_at)) ?> / <?= $data->created_by ?></td>
            <td><?= date('d M Y H:i:s', strtotime($data->updated_at)) ?> / <?= $data->updated_by ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>