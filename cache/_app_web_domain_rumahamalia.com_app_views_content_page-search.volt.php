<div class="mg-b-10">
    Ditemukan sebanyak : <strong><?= $this->length($pages) ?></strong> data, Dari hasil pencarian : "<strong><?= $search ?></strong>"  <a title="hapus pencarian" class="btn btn-warning rounded-10 pd-x-10 pd-y-1" href="<?= $this->url->get('/content/page') ?>"><i class="icon fa fa-close"></i> </a>
</div>

<table class="table table-hover  table-bordered table-responsive">
    <thead class="thead-colored thead-teal">
    <tr>
        <th class="text-light">#</th>
        <th class="text-light">Title</th>
        <th class="text-light wd-30p-force">Description</th>
        <th class="text-light">Keywords</th>
        <th class="text-light">Slug</th>
        <th class="text-light">Status</th>
        <th class="text-light">Created</th>
        <th class="text-light">Updated</th>
    </tr>
    </thead>
    <tbody class="bg-lightsky">
    <?php foreach ($pages as $data) { ?>
        <tr>
            <td>
                <a title="edit" class="btn btn-teal pd-y-2 pg-x-3" href="<?= $this->url->get('/content/page/edit/') ?><?= $data->id ?>"><i class="icon ion-edit"></i> </a>
            </td>
            <td>
                <?php if ($data->parent != '') { ?>
                    <?= $data->parent_title ?> &gt;
                <?php } ?>
                <?= $data->title ?>
            </td>
            <td><?= $data->description ?></td>
            <td>
                <?= tags($data->keywords) ?>

            </td>
            <td><a target="_blank" href="<?= $this->url->get('/page/') ?><?= $data->slug ?>"><?= $data->slug ?></a> </td>
            <td>
                <?php if ($data->status == 3 || $data->status == 7) { ?>
                    <span class="badge badge-pill badge-danger tx-white mg-x-1 pd-5"><i class="icon fa fa-edit"></i> <?= $data->status_name ?></span>
                <?php } elseif ($data->status == 8) { ?>
                    <span class="badge badge-pill badge-success tx-white mg-x-1 pd-5"><i class="icon fa fa-check"></i> <?= $data->status_name ?></span>
                <?php } ?>
            </td>
            <td><?= date('d M Y H:i:s', strtotime($data->created_at)) ?> / <?= $data->created_by ?></td>
            <td><?= date('d M Y H:i:s', strtotime($data->updated_at)) ?> / <?= $data->updated_by ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>