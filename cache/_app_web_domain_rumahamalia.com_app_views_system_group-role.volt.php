<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="<?= $this->url->get('/dashboard') ?>">Main Dashboard</a>
        <span class="breadcrumb-item">System</span>
        <a class="breadcrumb-item" href="<?= $this->url->get('/system/group') ?>">Group</a>
        <a class="breadcrumb-item active" href="<?= $this->url->get('/system/group/role/') ?><?= $id ?>">Roles</a>
    </nav>
</div>
<div class="row no-gutters">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
            <h4 class="tx-gray-800 mg-b-5">
                Role Group Pengguna
                <a href="<?= $this->url->get('/system/group') ?>" class="btn btn-teal btn-with-icon">
                    <div class="ht-25">
                        <span class="icon wd-25"><i class="fa fa-list-alt"></i></span>
                        <span class="pd-x-15">Daftar Group</span>
                    </div>

                </a>
            </h4>
            <p class="mg-b-0">Atur Role Group Pengguna</p>
        </div>
</div>
<hr>
<div class="br-pagebody  pd-sm-x-30">
    <div class="ui form">
        <div class="row">
            <?php foreach ($roles as $role) { ?>
                <div class="col-lg-3 col-sm-4 col-xs-6">
                    <ul class="roles">
                        <li>
                            <label class="ckbox ckbox-info">
                                <input type="checkbox" name="role" value="<?= $role->id ?>" id="<?= $role->id ?>" class="role" <?php if (haveRole(decode($id), $role->id)) { ?>checked<?php } ?> >
                                <span title="<?= $role->description ?>"><?= $role->name ?></span>
                            </label>
                            <ul>
                                <?php
                                $subroles=Roles::find(array("conditions"=>"parent='".intval($role->id)."'", "order"=>"id"));
                                ?>
                                <?php foreach ($subroles as $sub) { ?>
                                    <li>
                                        <label class="ckbox ckbox-info">
                                            <input type="checkbox" name="example" value="<?= $sub->id ?>" id="<?= $sub->id ?>" class="role" <?php if (haveRole(decode($id), $sub->id)) { ?>checked<?php } ?> >
                                            <span title="<?= $sub->description ?>"><?= $sub->name ?></span>
                                        </label>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                    </ul>
                </div>
            <?php } ?>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('.role').on('click', function(){
                $.post( "<?= $this->url->get('/system/group/role/') ?><?= $id ?>", { group: '<?= $id ?>', role: $(this).attr('value'), status: this.checked })
                    .done(function( data ) {
                    });
            });
        });
    </script>
</div>