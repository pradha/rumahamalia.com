<link href="<?= $this->url->get('/lib/SpinKit/spinkit.css') ?>" rel="stylesheet">

<script src="<?= $this->url->get('/lib/parsleyjs/parsley.js') ?>"></script>

<div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v">
    <div class="row no-gutters">

        <div class="col-lg-12 bg-white">
            <div id="overlay">
                <div class="d-flex  ht-100p pos-relative align-items-center">
                    <div class="sk-three-bounce">
                        <div class="sk-child sk-bounce1 bg-gray-800"></div>
                        <div class="sk-child sk-bounce2 bg-gray-800"></div>
                        <div class="sk-child sk-bounce3 bg-gray-800"></div>
                        <div class="text">Setting password...</div>
                    </div>
                </div>
            </div>
            <div>
                <div class="pd-x-30 pd-y-20">
                    <h3 class="tx-inverse  mg-b-5">Set Password</h3>
                    <p>Silahkan masukan password baru anda</p>

                    <form id="recover" method="post" action="<?= $this->url->get('/recover/code/') ?><?= $hash ?>" data-parsley-validate>
                        <div class="form-group">
                            <input type="password" name="new" class="form-control" placeholder="Password Anda" autocomplete="off" required>
                        </div>
                        <div class="form-group">
                            <input type="password" name="renew" class="form-control" placeholder="Ulang Password Anda" autocomplete="off" required>
                        </div>
                        <button type="submit" class="btn btn-warning pd-y-12 btn-block">Set</button>
                        <a href="<?= $this->url->get('/index/') ?><?= $key ?>" class="tx-12 d-block mg-t-10">Login</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('[name="usermail"]').focus();
        $("form").submit(function(e){
            e.preventDefault();
            $form = $(this);
            $('#overlay').toggle();

            $.post(
                $form.attr('action'),
                $(this).serialize(),
                function(result){
                    var res=result.split(" ");
                    if (res[0]!="success"){
                        $('#overlay').toggle();
                        $(".alert").remove();
                        var warn='<div class="alert alert-warning" role="alert">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                            '<span aria-hidden="true">&times;</span>' +
                            '</button>' +
                            result+
                            '</div>';
                        $('#recover').prepend(warn);
                        $('[name="new"]').val('');
                        $('[name="renew"]').val('');
                        $('[name="usermail"]').focus();
                    } else {
                        var success='<div class="alert alert-success" role="alert">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                            '<span aria-hidden="true">&times;</span>' +
                            '</button>' +
                            'Password anda telah berhasil diubah, silahkan <a href="<?= $this->url->get('/index/') ?>'+res[1]+'">login</a> kembali untuk melanjutkan'+
                            '</div>';
                        $('#overlay').toggle();
                        $('#recover').html(success);
                    }
                })
                .fail(function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                });

        });
    });
</script>