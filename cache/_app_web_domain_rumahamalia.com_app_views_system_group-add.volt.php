<script src="<?= $this->url->get('/lib/parsleyjs/parsley.js') ?>"></script>
<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="<?= $this->url->get('/dashboard') ?>">Main Dashboard</a>
        <span class="breadcrumb-item">Contents</span>
        <a class="breadcrumb-item" href="<?= $this->url->get('/system/group') ?>">Group</a>
        <a class="breadcrumb-item active" href="<?= $this->url->get('/system/group/add') ?>">Add</a>
    </nav>
</div><!-- br-pageheader -->
<div class="row">
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
            <h4 class="tx-gray-800 mg-b-5">
                Tambah Group Pengguna
                <a href="<?= $this->url->get('/system/group/') ?>" class="btn btn-teal btn-with-icon">
                    <div class="ht-25">
                        <span class="icon wd-25"><i class="fa fa-list"></i></span>
                        <span class="pd-x-15">Daftar Group</span>
                    </div>
                </a>
            </h4>
            <p class="mg-b-0">Tambah Group Pengguna</p>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30 text-right">

        </div>
    </div>
</div>
<hr>
<div class="br-pagebody pd-sm-x-30">
    <form method="post" action="<?= $this->url->get('/system/group/add') ?>" class="form-layout form-layout-1 bg-white" data-parsley-validate>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-control-label">Group Name: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="name" value="<?php if ($this->request->isPost()) { ?><?= $this->request->getPost('name') ?><?php } ?>" placeholder="Group Name" required autocomplete="off">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group mg-b-10-force">
                    <label class="form-control-label">Description: <span class="tx-danger">*</span></label>
                    <textarea rows="3" class="form-control" name="description" placeholder="Category Description" required autocomplete="off"><?php if ($this->request->isPost()) { ?><?= $this->request->getPost('description') ?><?php } ?></textarea>
                </div>
            </div>
        </div><!-- row -->

        <div class="form-layout-footer">
            <button type="submit" class="btn btn-teal">Create</button>
        </div>
    </form>
</div>

<script>
    $(document).ready(function(){
        $("form").submit(function(e) {
            e.preventDefault();
            $form = $(this);
            $('#overlay').toggle();
            $.post(
                $form.attr('action'),
                $(this).serialize(),
                function(result){
                    $("#main").html(result);
                    $('#overlay').toggle();
                })
                .fail(function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                });

        });
    });
</script>