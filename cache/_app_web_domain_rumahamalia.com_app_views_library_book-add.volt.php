<script src="<?= $this->url->get('/lib/parsleyjs/parsley.js') ?>"></script>
<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="<?= $this->url->get('/dashboard') ?>">Main Dashboard</a>
        <span class="breadcrumb-item">Library</span>
        <a class="breadcrumb-item" href="<?= $this->url->get('/library/book/') ?>">Book</a>
        <a class="breadcrumb-item active" href="<?= $this->url->get('/library/book/add') ?>">Add Book</a>
    </nav>
</div><!-- br-pageheader -->
<div class="row no-gutters">
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
            <h4 class="tx-gray-800 mg-b-5">
                Tambah Buku
                <?php if (haveRole($this->session->get('group'), 30)) { ?>
                    <a href="<?= $this->url->get('/library/book') ?>" class="btn btn-teal btn-with-icon">
                        <div class="ht-25">
                            <span class="icon wd-25"><i class="fa fa-list-alt"></i></span>
                            <span class="pd-x-15">Daftar Buku</span>
                        </div>
                    </a>
                <?php } ?>
            </h4>
            <p class="mg-b-0">Daftar Buku</p>
        </div>
    </div>
</div>

<hr>
<div class="br-pagebody pd-sm-x-30">
    <?php if (isset($error->title) || isset($error->description) || isset($error->author) || isset($error->publication_date) || isset($error->quantity) || isset($error->genre) || isset($error->save)) { ?>
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong class="d-block d-sm-inline-block-force">Oops!</strong> Silahkan periksa kembali kesalahan berikut :
            <ul class="mg-b-0">
                <?php if (isset($error->title)) { ?><li>Nama masih kosong</li><?php } ?>
                <?php if (isset($error->description)) { ?><li>Deskriprsi masih kosong</li><?php } ?>
                <?php if (isset($error->author)) { ?><li>Author belum dipilih</li><?php } ?>
                <?php if (isset($error->publication_date)) { ?><li>Tanggal publikasi masih kosong</li><?php } ?>
                <?php if (isset($error->quantity)) { ?><li>Kuantitas/stok buku masih kosong</li><?php } ?>
                <?php if (isset($error->genre)) { ?><li>Genre buku belum dipiih</li><?php } ?>
                <?php if (isset($error->save)) { ?>
                    <?php foreach ($error->save as $err) { ?>
                        <li><?= $err ?></li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>
    <form method="post" action="<?= $this->url->get('/library/book/add') ?>" class="form-layout form-layout-1 bg-white" data-parsley-validate>
        <div class="row vdivide">
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="form-group">
                            <label class="form-control-label">Book Title: <span class="tx-danger">*</span></label>
                            <input class="form-control" type="text" name="title" value="<?php if ($this->request->isPost()) { ?><?= $this->request->getPost('title') ?><?php } ?>" placeholder="Book Title" required autocomplete="off" maxlength="100">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-control-label">ISBN:</label>
                            <input class="form-control" type="text" name="isbn" value="<?php if ($this->request->isPost()) { ?><?= $this->request->getPost('isbn') ?><?php } ?>" placeholder="ISBN" autocomplete="off" maxlength="13">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group mg-b-10-force">
                            <label class="form-control-label">Description: <span class="tx-danger">*</span></label>
                            <textarea rows="10" class="form-control" name="description" placeholder="Book Description" required autocomplete="off"><?php if ($this->request->isPost()) { ?><?= $this->request->getPost('description') ?><?php } ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-7">
                        <div class="form-group">
                            <label class="form-control-label">Author: <span class="tx-danger">*</span></label>
                            <select class="form-control select2 select2-hidden-accessible" required name="author" data-placeholder="Choose Author" tabindex="-1" aria-hidden="true">
                                <option label=".: no author :."></option>
                                <?php foreach ($authors as $author) { ?>
                                    <option value="<?= $author->id ?>" label="<?= $author->name ?>" <?php if ($this->request->isPost() && $this->request->getPost('author') == $author->id) { ?>selected<?php } ?>><?= $author->name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="form-control-label">Tanggal Publikasi:</label>
                            <input class="form-control" type="text" name="publication_date" value="<?php if ($this->request->isPost()) { ?><?= $this->request->getPost('publication_date') ?><?php } ?>" placeholder="Publication Date" required autocomplete="off">
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label class="form-control-label">Quantity:</label>
                            <input class="form-control" type="text" name="quantity" value="<?php if ($this->request->isPost()) { ?><?= $this->request->getPost('quantity') ?><?php } ?>" placeholder="Quantity" required autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="form-layout-footer">
                    <button type="submit" class="btn btn-teal">Create</button>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label class="form-control-label">Genre:</label>
                    <hr>
                    <div id="cbWrapper" class="row">
                        <?php foreach ($genres as $genre) { ?>
                            <div class="col-lg-6">
                                <ul class="catList">
                                    <li>
                                        <label class="ckbox ckbox-info">
                                            <input name="genre[]" type="checkbox" value="<?= $genre->id ?>" data-parsley-mincheck="1" data-parsley-class-handler="#cbWrapper" data-parsley-errors-container="#cbErrorContainer" required <?php if ($this->request->isPost() && is_array($this->request->getPost('genre')) && in_array($genre->id,$this->request->getPost('genre'))) echo 'checked'; ?>>
                                            <span><?= $genre->name ?></span>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                        <?php } ?>
                    </div>
                    <div id="cbErrorContainer"></div>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    $(document).ready(function(){
        $("form").submit(function(e) {
            e.preventDefault();
            $form = $(this);
            $('#overlay').toggle();
            $.post(
                $form.attr('action'),
                $(this).serialize(),
                function(result){
                    $("#main").html(result);
                    $('#overlay').toggle();
                })
                .fail(function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                });
        });
        $('[name="publication_date"]').datepicker({
            changeMonth: true,
            changeYear: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            maxDate:0,
            dateFormat: 'yy-mm-dd',
        });
    });
</script>