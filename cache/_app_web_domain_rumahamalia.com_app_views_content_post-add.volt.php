<link href="<?= $this->url->get('/lib/summernote/summernote-bs4.css') ?>" rel="stylesheet">
<link href="<?= $this->url->get('/lib/bootstrap-tagsinput/bootstrap-tagsinput.css') ?>" rel="stylesheet">
<link href="<?= $this->url->get('/lib/select2/css/select2.min.css') ?>" rel="stylesheet">

<script src="<?= $this->url->get('/lib/parsleyjs/parsley.js') ?>"></script>
<script src="<?= $this->url->get('/lib/bootstrap-tagsinput/bootstrap-tagsinput.js') ?>"></script>


<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="<?= $this->url->get('/dashboard') ?>">Main Dashboard</a>
        <span class="breadcrumb-item">Contents</span>
        <a class="breadcrumb-item" href="<?= $this->url->get('/content/post') ?>">Post</a>
        <a class="breadcrumb-item active" href="<?= $this->url->get('/content/post/add') ?>">Add</a>
    </nav>
</div><!-- br-pageheader -->
<div class="row no-gutters">
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
            <h4 class="tx-gray-800 mg-b-5">
                Tambah Postingan
                <a href="<?= $this->url->get('/content/post/') ?>" class="btn btn-teal btn-with-icon">
                    <div class="ht-25">
                        <span class="icon wd-25"><i class="fa fa-list"></i></span>
                        <span class="pd-x-15">Daftar Postingan</span>
                    </div>
                </a>
            </h4>
            <p class="mg-b-0">Tambah Postingan</p>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30 text-right">

        </div>
    </div>
</div>

<hr>
<div class="br-pagebody pd-sm-x-30">
    <form method="post" action="<?= $this->url->get('/content/post/add') ?>" class="form-layout form-layout-1 bg-white" data-parsley-validate>
        <div class="row">
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="form-control-label">Post Title: <span class="tx-danger">*</span></label>
                            <input class="form-control" maxlength="150" type="text" name="title" value="<?php if ($this->request->isPost()) { ?><?= $this->request->getPost('title') ?><?php } ?>" placeholder="Enter page title..." required autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group mg-b-10-force">
                            <label class="form-control-label">Post Content: <span class="tx-danger">*</span></label>
                            <textarea id="summernote" class="form-control" name="content" placeholder="Post Content" required autocomplete="off"><?php if ($this->request->isPost()) { ?><?= $this->request->getPost('content') ?><?php } ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label class="form-control-label bd-b">Post Category: <span class="tx-danger">*</span></label>
                    <div id="cbWrapper" class="row">
                        <?php foreach ($categories as $category) { ?>
                            <div class="col-lg-6">
                                <ul class="catList">
                                    <li>
                                        <label class="ckbox">
                                            <input name="category[]" type="checkbox" value="<?= $category->id ?>" data-parsley-mincheck="1" data-parsley-class-handler="#cbWrapper" data-parsley-errors-container="#cbErrorContainer" required <?php if ($this->request->isPost() && is_array($this->request->getPost('category')) && in_array($category->id,$this->request->getPost('category'))) echo 'checked'; ?>>
                                            <span><?= $category->name ?></span>
                                        </label>
                                        <?= subCat($category->id) ?>
                                    </li>
                                </ul>

                            </div>

                        <?php } ?>
                    </div>
                    <div id="cbErrorContainer"></div>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Post Description: <span class="tx-danger">*</span></label>
                    <textarea rows="5" class="form-control" name="description" placeholder="Post Description" required autocomplete="off" maxlength="250"><?php if ($this->request->isPost()) { ?><?= $this->request->getPost('description') ?><?php } ?></textarea>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Post Slug: <span class="tx-danger">*</span></label>
                    <div class="input-group">
                        <span class="input-group-addon tx-size-sm lh-2"><?= $this->url->get('/read/') ?></span>
                        <input type="text" class="form-control" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Keywords: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="keywords" value="<?php if ($this->request->isPost()) { ?><?= $this->request->getPost('keywords') ?><?php } ?>" placeholder="Keywords or Tags" data-role="tagsinput" required>
                </div>
            </div>
        </div>


        <div class="form-layout-footer">
            <button type="submit" class="btn btn-teal">Tambah</button>
        </div>
    </form>
</div>

<div id="gallery" class="modal fade">
    <div class="modal-dialog modal-v-lg" role="document">
        <div class="modal-content tx-size-sm">
            <div class="modal-header pd-x-20">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Media Gallery | <button id="upload" class="btn btn-teal pd-x-8 pd-y-3" href="#upload"><small><i class="icon fa fa-upload"></i> Upload</small></button> </h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div id="data" class="modal-body pd-20 text-center">

            </div>
        </div>
    </div>
</div>

<script src="<?= $this->url->get('/lib/summernote/summernote-bs4.min.js') ?>"></script>
<script>
    function loadMedia(){
        $('#data').html("Silahkan tunggu...");
        $.get("<?= $this->url->get('/content/media/data') ?>", function(data, status){
            $('#data').html(data);
        });
    }

    $(document).ready(function(){
        $('#upload').on('click',function(){
            $.get("<?= $this->url->get('/content/media/insertUpload') ?>", function(data, status){
                $('#data').html(data);
            });
        });
        var galleryButton = function (context) {
            var ui = $.summernote.ui;

            var button = ui.button({
                contents: '<i class="fa fa-image"/>',
                tooltip: 'Gallery',
                click: function () {
                    $('#gallery').modal('show');
                    loadMedia();
                }
            });

            return button.render();
        };
        $('#summernote').summernote({
            minHeight:($(window).height() - 550),
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline']],
                ['font', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link','gallery','video']],
                ['height', ['height']],
                ['misc', ['fullscreen','codeview']]
            ],

            buttons: {
                gallery: galleryButton
            }
        });

        $("form").submit(function(e) {
            e.preventDefault();
            $form = $(this);
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $('#overlay').toggle();
            $.post(
                $form.attr('action'),
                $(this).serialize(),
                function(result){
                    $("#main").html(result);
                    $('#overlay').toggle();
                })
                .fail(function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                });

        });
    });
</script>
