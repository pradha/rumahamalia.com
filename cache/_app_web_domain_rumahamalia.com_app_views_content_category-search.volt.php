<div class="mg-b-10">
    Ditemukan sebanyak : <strong><?= $this->length($categories) ?></strong> data, Dari hasil pencarian : "<strong><?= $search ?></strong>"  <a title="hapus pencarian" class="btn btn-warning rounded-10 pd-x-10 pd-y-1" href="<?= $this->url->get('/content/category') ?>"><i class="icon fa fa-close"></i> </a>
</div>
<table class="table table-hover table-bordered table-responsive">
    <thead class="thead-colored thead-teal">
    <tr>
        <th class="text-light">#</th>
        <th class="text-light">Name</th>
        <th class="text-light">Description</th>
        <th class="text-light">Keywords</th>
        <th class="text-light">Slug</th>
        <th class="text-light">Created</th>
        <th class="text-light">Updated</th>
    </tr>
    </thead>
    <tbody class="bg-lightsky">
    <?php foreach ($categories as $data) { ?>
        <tr>
            <td>
                <a title="edit" class="btn btn-teal pd-y-2 pg-x-3" href="<?= $this->url->get('/content/category/edit/') ?><?= $data->id ?>"><i class="icon ion-edit"></i> </a>
            </td>
            <td>
                <?php if ($data->parent != '') { ?>
                    <?= $data->parent_name ?> &gt;
                <?php } ?>
                <?= $data->name ?>
            </td>
            <td><?= $data->description ?></td>
            <td><?= tags($data->keywords) ?></td>
            <td><a href="<?= $this->url->get('/category/') ?><?= $data->slug ?>" target="_blank"><?= $data->slug ?></a> </td>
            <td><?= date('d M Y H:i:s', strtotime($data->created_at)) ?> / <?= $data->created_by ?></td>
            <td><?= date('d M Y H:i:s', strtotime($data->updated_at)) ?> / <?= $data->updated_by ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>