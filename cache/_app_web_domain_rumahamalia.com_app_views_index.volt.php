<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title><?php if (isset($title)) { ?><?= $title ?><?php } else { ?>Rumah Amalia | Organisasi Nonprofit<?php } ?></title>
    <meta name="description" content="<?php if (isset($description)) { ?><?= $description ?><?php } else { ?>Rumah Amalia adalah Komunitas rumah belajar untuk anak Yatim & anak Dhuafa<?php } ?>">
    <meta name="keywords" content="<?php if (isset($keywords)) { ?><?= $keywords ?><?php } else { ?>Ruah Amalia, Komunitas rumah belajar, anak yatim, sosial, dhuafa<?php } ?>">
    <meta name="author" content="<?php if (isset($author) && isset($author_email)) { ?><?= $author ?>, <?= $author_email ?><?php } else { ?>Rumah Amalia, adit@pradha.id<?php } ?>">
    <meta name='designer' content='<?php if (isset($designer)) { ?><?= $designer ?><?php } else { ?>Aditya Y Pradhana<?php } ?>'>
    <meta name='copyright' content='<?php if (isset($copyright)) { ?><?= $copyright ?><?php } else { ?>Pradha Corp<?php } ?>'>
    <meta name='language' content='<?php if (isset($language)) { ?><?= $language ?><?php } else { ?>ID<?php } ?>'>
    <meta name='robots' content='<?php if (isset($robots)) { ?><?= $robots ?><?php } else { ?>index,follow<?php } ?>'>
    <meta name='coverage' content='<?php if (isset($coverage)) { ?><?= $coverage ?><?php } else { ?>Worldwide<?php } ?>'>
    <meta name='distribution' content='<?php if (isset($distribution)) { ?><?= $distribution ?><?php } else { ?>Global<?php } ?>'>
    <meta name='target' content='<?php if (isset($target)) { ?><?= $target ?><?php } else { ?>all<?php } ?>'>
    <meta name='owner' content='<?php if (isset($owner)) { ?><?= $owner ?><?php } else { ?>Aditya Y Pradhana<?php } ?>'>
    <meta name='url' content='<?= $this->url->get() ?><?= $this->router->getRewriteUri() ?>'>
    <meta name='identifier-URL' content='<?= $this->url->get() ?><?= $this->router->getRewriteUri() ?>'>
    <link rel='canonical' href='<?= $this->url->get() ?><?= $this->router->getRewriteUri() ?>'>
    <link rel='publisher' href='<?php if (isset($publisher)) { ?><?= $publisher ?><?php } else { ?>Aditya Y Pradhana<?php } ?>'>

    <!-- Facebook Open Graph -->
    <meta property="og:url" content="<?= $this->url->get() ?><?= $this->router->getRewriteUri() ?>" />
    <meta property="og:type" content="<?php if (isset($og_type)) { ?><?= $og_type ?><?php } else { ?>summary<?php } ?>" />
    <meta property="og:title" content="<?php if (isset($title)) { ?><?= $title ?><?php } else { ?>Rumah Amalia | Organisasi Nonprofit<?php } ?>" />
    <meta property="og:description" content="<?php if (isset($description)) { ?><?= $description ?><?php } else { ?>Rumah Amalia adalah Komunitas rumah belajar untuk anak Yatim & anak Dhuafa<?php } ?>" />

    <?php if (isset($images)) { ?>
        <?php $v153298751881276578131iterator = $images; $v153298751881276578131incr = 0; $v153298751881276578131loop = new stdClass(); $v153298751881276578131loop->self = &$v153298751881276578131loop; $v153298751881276578131loop->length = count($v153298751881276578131iterator); $v153298751881276578131loop->index = 1; $v153298751881276578131loop->index0 = 1; $v153298751881276578131loop->revindex = $v153298751881276578131loop->length; $v153298751881276578131loop->revindex0 = $v153298751881276578131loop->length - 1; ?><?php foreach ($v153298751881276578131iterator as $image) { ?><?php $v153298751881276578131loop->first = ($v153298751881276578131incr == 0); $v153298751881276578131loop->index = $v153298751881276578131incr + 1; $v153298751881276578131loop->index0 = $v153298751881276578131incr; $v153298751881276578131loop->revindex = $v153298751881276578131loop->length - $v153298751881276578131incr; $v153298751881276578131loop->revindex0 = $v153298751881276578131loop->length - ($v153298751881276578131incr + 1); $v153298751881276578131loop->last = ($v153298751881276578131incr == ($v153298751881276578131loop->length - 1)); ?>
        <meta property="og:image" content="<?= $image ?>" />
        <?php $v153298751881276578131incr++; } ?>
    <?php } elseif (isset($image)) { ?>
        <meta property="og:image" content="<?= $image ?>" />
    <?php } ?>

    <meta property="og:locale" content="<?php if (isset($locale)) { ?><?= $locale ?><?php } else { ?>id_ID<?php } ?>" />
    <!-- End Facebook Open Graph -->

    <!-- Twitter Card -->
    <meta name="twitter:card" content="<?php if (isset($twitter_card)) { ?><?= $twitter_card ?><?php } else { ?>summary<?php } ?>">
    <meta name="twitter:url" content="<?= $this->url->get() ?><?= $this->router->getRewriteUri() ?>">
    <meta name="twitter:title" content="<?php if (isset($title)) { ?><?= $title ?><?php } else { ?>Rumah Amalia | Organisasi Nonprofit<?php } ?>">
    <meta name="twitter:description" content="<?php if (isset($description)) { ?><?= $description ?><?php } else { ?>Rumah Amalia adalah Komunitas rumah belajar untuk anak Yatim & anak Dhuafa<?php } ?>">

    <?php if (isset($images)) { ?>
        <?php $v153298751881276578131iterator = $images; $v153298751881276578131incr = 0; $v153298751881276578131loop = new stdClass(); $v153298751881276578131loop->self = &$v153298751881276578131loop; $v153298751881276578131loop->length = count($v153298751881276578131iterator); $v153298751881276578131loop->index = 1; $v153298751881276578131loop->index0 = 1; $v153298751881276578131loop->revindex = $v153298751881276578131loop->length; $v153298751881276578131loop->revindex0 = $v153298751881276578131loop->length - 1; ?><?php foreach ($v153298751881276578131iterator as $image) { ?><?php $v153298751881276578131loop->first = ($v153298751881276578131incr == 0); $v153298751881276578131loop->index = $v153298751881276578131incr + 1; $v153298751881276578131loop->index0 = $v153298751881276578131incr; $v153298751881276578131loop->revindex = $v153298751881276578131loop->length - $v153298751881276578131incr; $v153298751881276578131loop->revindex0 = $v153298751881276578131loop->length - ($v153298751881276578131incr + 1); $v153298751881276578131loop->last = ($v153298751881276578131incr == ($v153298751881276578131loop->length - 1)); ?>
            <meta name="twitter:image" content="<?= $image ?>">
        <?php $v153298751881276578131incr++; } ?>
    <?php } elseif (isset($image)) { ?>
        <meta name="twitter:image" content="<?= $image ?>">
    <?php } ?>

    <meta name="twitter:site" content="<?php if (isset($twitter_site)) { ?><?= $twitter_site ?><?php } else { ?>@adityudhna<?php } ?>">
    <meta name="twitter:creator" content="<?php if (isset($twitter_creator)) { ?><?= $twitter_creator ?><?php } else { ?>@adityudhna<?php } ?>">
    <!-- End Twitter Card -->

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?= $this->url->get('/img/favicons/apple-icon-57x57.png') ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= $this->url->get('/img/favicons/apple-icon-60x60.png') ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= $this->url->get('/img/favicons/apple-icon-72x72.png') ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= $this->url->get('/img/favicons/apple-icon-76x76.png') ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= $this->url->get('/img/favicons/apple-icon-114x114.png') ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= $this->url->get('/img/favicons/apple-icon-120x120.png') ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= $this->url->get('/img/favicons/apple-icon-144x144.png') ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= $this->url->get('/img/favicons/apple-icon-152x152.png') ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= $this->url->get('/img/favicons/apple-icon-180x180.png') ?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= $this->url->get('/img/favicons/android-icon-192x192.png') ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= $this->url->get('/img/favicons/favicon-32x32.png') ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= $this->url->get('/img/favicons/favicon-96x96.png') ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= $this->url->get('/img/favicons/favicon-16x16.png') ?>">
    <link rel="manifest" href="<?= $this->url->get('/img/favicons/manifest.json') ?>">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= $this->url->get('/img/favicons/ms-icon-144x144.png') ?>">
    <meta name="theme-color" content="#ffffff">
    <!-- End Favicon -->

    <!-- Stylesheet -->
    <link href="<?= $this->url->get('/lib/font-awesome/css/font-awesome.css') ?>" rel="stylesheet">
    <link href="<?= $this->url->get('/lib/Ionicons/css/ionicons.css') ?>" rel="stylesheet">
    <link href="<?= $this->url->get('/lib/perfect-scrollbar/css/perfect-scrollbar.css') ?>" rel="stylesheet">
    <link href="<?= $this->url->get('/lib/jquery-switchbutton/jquery.switchButton.css') ?>" rel="stylesheet">
    <link href="<?= $this->url->get('/lib/SpinKit/spinkit.css') ?>" rel="stylesheet">
    <link href="<?= $this->url->get('/css/bracket.css') ?>" rel="stylesheet">
    <link href="<?= $this->url->get('/css/slick.css') ?>" rel="stylesheet">
    <link href="<?= $this->url->get('/css/slick-theme.css') ?>" rel="stylesheet">
    <link href="<?= $this->url->get('/css/style.css') ?>" rel="stylesheet">

    <!-- End Stylesheet -->


    <!-- Scripts -->
    <script src="<?= $this->url->get('/lib/jquery/jquery.js') ?>"></script>
    <script src="<?= $this->url->get('/js/slick.min.js') ?>"></script>
    <script src="<?= $this->url->get('/js/jquery-sidebar.min.js') ?>"></script>
    <!-- End Scripts -->

</head>
<body>
    <?= $this->getContent() ?>
    <script src="<?= $this->url->get('/lib/popper.js/popper.js') ?>"></script>
    <script src="<?= $this->url->get('/lib/bootstrap/bootstrap.js') ?>"></script>
    <script src="<?= $this->url->get('/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js') ?>"></script>
    <script src="<?= $this->url->get('/lib/moment/moment.js') ?>"></script>
    <script src="<?= $this->url->get('/lib/jquery-ui/jquery-ui.js') ?>"></script>
    <script src="<?= $this->url->get('/lib/jquery-switchbutton/jquery.switchButton.js') ?>"></script>
    <script src="<?= $this->url->get('/lib/peity/jquery.peity.js') ?>"></script>
    <script src="<?= $this->url->get('/js/bracket.js') ?>"></script>

</body>
</html>
