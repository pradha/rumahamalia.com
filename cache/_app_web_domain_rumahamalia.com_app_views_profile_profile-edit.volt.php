<script src="<?= $this->url->get('/lib/parsleyjs/parsley.js') ?>"></script>
<script src="<?= $this->url->get('/js/simpleUpload.min.js') ?>"></script>

<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="<?= $this->url->get('/dashboard') ?>">Main Dashboard</a>
        <a class="breadcrumb-item" href="<?= $this->url->get('/profile') ?>">Profile</a>
        <a class="breadcrumb-item active" href="<?= $this->url->get('/profile/edit') ?>">Edit</a>
    </nav>
</div><!-- br-pageheader -->
<div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">

    <h4 class="tx-gray-800 mg-b-5">
        Edit Profile
        <a href="<?= $this->url->get('/profile/') ?>" class="btn btn-teal btn-with-icon">
            <div class="ht-25">
                <span class="icon wd-25"><i class="fa fa-user"></i></span>
                <span class="pd-x-15">Lihat Profile</span>
            </div>
        </a>
    </h4>
    <p class="mg-b-0">Edit Profile Pengguna</p>
</div>

<div class="br-pagebody pd-sm-x-30">
    <div class="row bg-white form-layout form-layout-1 vdivide">
        <div class="col-lg-3 profilePict text-center">
            <?php if ($user->photo != '') { ?>
                <img src="<?= $this->url->get('/files/upload/profile/') ?><?= $user->photo ?>" class="img profile img-fluid">
            <?php } else { ?>
                <img src="<?= $this->url->get('/img/user.png') ?>" class="img profile img-fluid">
            <?php } ?>
            <div class="control">
                <span class="text" data-toggle="modal" data-target="#profilePict"> <i class="icon fa fa-image"></i> Ganti Foto</span>
            </div>
        </div>
        <div class="col-lg-9">
            <?php if (isset($status) && $status == 'success') { ?>
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong class="d-block d-sm-inline-block-force">Success!</strong> Profile anda telah berhasil disimpan.
                </div><!-- alert -->
            <?php } elseif (isset($error->email) || isset($error->name) || isset($error->phone) || isset($error->gender) || isset($error->date_of_birth) || isset($error->biography)) { ?>
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong class="d-block d-sm-inline-block-force">Oops!</strong> Silahkan periksa kembali kesalahan berikut :
                    <ul class="mg-b-0">
                        <?php if (isset($error->email)) { ?><li>E-Mail yang digunakan telah terdaftar</li><?php } ?>
                        <?php if (isset($error->name)) { ?><li>Nama masih kosong</li><?php } ?>
                        <?php if (isset($error->phone)) { ?><li>No telepon masih kosong</li><?php } ?>
                        <?php if (isset($error->gender)) { ?><li>Jenis kelamin belum dipilih</li><?php } ?>
                        <?php if (isset($error->date_of_birth)) { ?><li>Tanggal lahir masih kosong</li><?php } ?>
                        <?php if (isset($error->biography)) { ?><li>Biography masih kosong</li><?php } ?>
                    </ul>
                </div>
            <?php } ?>
            <form method="post" action="<?= $this->url->get('/profile/edit') ?>"  data-parsley-validate>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label">Username: <span class="tx-danger">*</span></label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon ion-person tx-16 lh-0 op-6"></i></span>
                                <input class="form-control" type="text" name="username" value="<?= $user->username ?>" placeholder="Username" required autocomplete="off" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label">E-Mail: <span class="tx-danger">*</span></label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon ion-android-mail tx-16 lh-0 op-6"></i></span>
                                <input class="form-control" type="email" name="email" value="<?php if ($this->request->isPost()) { ?><?= $this->request->getPost('email') ?><?php } else { ?><?= $user->email ?><?php } ?>" placeholder="E-Mail" required autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="form-control-label">Full Name: <span class="tx-danger">*</span></label>
                            <input class="form-control" type="text" name="name" value="<?php if ($this->request->isPost()) { ?><?= $this->request->getPost('name') ?><?php } else { ?><?= $user->name ?><?php } ?>" placeholder="Full Name" required autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-control-label">Phone: <span class="tx-danger">*</span></label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon ion-android-phone-portrait tx-16 lh-0 op-6"></i></span>
                                <input data-parsley-errors-container="#errorPhone" class="form-control" type="text" name="phone" value="<?php if ($this->request->isPost()) { ?><?= $this->request->getPost('phone') ?><?php } else { ?><?= $user->phone ?><?php } ?>" placeholder="Phone" required autocomplete="off">
                            </div>
                            <span id="errorPhone"></span>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-control-label">Gender: <span class="tx-danger">*</span></label>
                            <select class="form-control select2 select2-hidden-accessible" name="gender" data-placeholder="Choose gender" tabindex="-1" aria-hidden="true">
                                <?php foreach ($genders as $gender) { ?>
                                <option value="<?= $gender->id ?>" label="<?= $gender->name ?>" <?php if ($this->request->isPost() && $this->request->getPost('gender') == $gender->id) { ?>selected<?php } elseif ($user->gender == $gender->id) { ?>selected<?php } ?>><?= $gender->name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-control-label">Date of Birth: <span class="tx-danger">*</span></label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon ion-calendar tx-16 lh-0 op-6"></i></span>
                                <input data-parsley-errors-container="#errorDate" class="form-control fc-datepicker"  type="text" name="date_of_birth" value="<?php if ($this->request->isPost()) { ?><?= $this->request->getPost('date_of_birth') ?><?php } else { ?><?= $user->date_of_birth ?><?php } ?>" placeholder="Date of Birth" required autocomplete="off">
                            </div>
                            <span id="errorDate"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group mg-b-10-force">
                            <label class="form-control-label">Biography: <span class="tx-danger">*</span></label>
                            <textarea rows="3" class="form-control" name="biography" placeholder="Biography" required autocomplete="off"><?php if ($this->request->isPost()) { ?><?= $this->request->getPost('biography') ?><?php } else { ?><?= $user->biography ?><?php } ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-layout-footer">
                    <button type="submit" class="btn btn-teal">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="profilePict" class="modal fade">
    <div class="modal-dialog modal-dialog-vertical-center" role="document">
        <div class="modal-content bd-0 tx-14">
            <div class="modal-header pd-y-20 pd-x-25">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Upload Image</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body pd-25">
                <form id="upload" method="post" action="<?= $this->url->get('profile/uploadPict') ?>" enctype="multipart/form-data">
                    <input type="file" name="file">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div><!-- modal-dialog -->
</div>
<script>
    $(document).ready(function(){

        $('input[type=file]').change(function(){

            $(this).simpleUpload("<?= $this->url->get('/profile/uploadPict') ?>", {

                start: function(file){
                    console.log("upload started");
                },

                progress: function(progress){
                    console.log("upload progress: " + Math.round(progress) + "%");
                },

                success: function(data){
                    if (data!="failed"){
                        $("#upload").html('<img src="'+data+'" class="img ht-200">');
                        $("img.profile").attr('src',data);
                    }
                    console.log("upload successful!");
                    console.log(data);
                },

                error: function(error){
                    console.log("upload error: " + error.name + ": " + error.message);
                }

            });

        });

        $('[name="date_of_birth"]').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd'
        });

        $("form").submit(function(e) {
            e.preventDefault();
            $form = $(this);
            $('#overlay').toggle();
            $.post(
                $form.attr('action'),
                $(this).serialize(),
                function(result){
                    $("#main").html(result);
                    $('#overlay').toggle();
                })
                .fail(function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                });

        });
    });
</script>