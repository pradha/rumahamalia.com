<div class="mg-b-10">
    Ditemukan sebanyak : <strong><?= $this->length($sliders) ?></strong> data, Dari hasil pencarian : "<strong><?= $search ?></strong>"  <a title="hapus pencarian" class="btn btn-warning rounded-10 pd-x-10 pd-y-1" href="<?= $this->url->get('/content/slider') ?>"><i class="icon fa fa-close"></i> </a>
</div>
<table class="table table-hover table-bordered">
    <thead class="thead-colored thead-teal">
    <tr>
        <th class="text-light">#</th>
        <th class="text-light wd-20p-force">Slider</th>
        <th class="text-light">Title</th>
        <th class="text-light">Description</th>
        <th class="text-light">Target</th>
        <th class="text-light">Placement</th>
        <th class="text-light">Status</th>
        <th class="text-light">Created by</th>
        <th class="text-light">Created at</th>
    </tr>
    </thead>
    <tbody class="bg-lightsky">
    <?php foreach ($sliders as $data) { ?>
        <tr>
            <td>
                <a title="edit" class="btn btn-teal pd-y-2 pg-x-3" href="<?= $this->url->get('/content/slider/edit/') ?><?= $data->id ?>"><i class="icon ion-edit"></i> </a>
                <a title="delete" class="btn btn-teal pd-y-2 pg-x-3" href="<?= $this->url->get('/content/slider/delete/') ?><?= $data->id ?>"><i class="icon ion-trash-a"></i> </a>
            </td>
            <td><img src="<?= $data->image ?>" class="img img-fluid"></td>
            <td><?= $data->title ?></td>
            <td><?= $data->description ?></td>
            <td><a target="_blank" href="<?= $data->url ?>"><i class="icon fa fa-external-link"></i> </a> </td>
            <td><?= $data->placement ?> </td>
            <td><?= $data->status ?> </td>
            <td><?= date('d M Y H:i:s', strtotime($data->created_at)) ?> / <?= $data->created_by ?></td>
            <td><?= date('d M Y H:i:s', strtotime($data->updated_at)) ?> / <?= $data->updated_by ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>