<?php

class PageController extends \Phalcon\Mvc\Controller
{
    public $options;
    public function initialize()
    {
        $this->view->setLayout('layout');
        $this->view->pages=Pages::find(["parent is null AND status=8"]);
    }

    public function indexAction($slug)
    {
        $page=Pages::findFirst(array("conditions"=>"slug='".$slug."' AND status=8"));
        if (isset($page->id)){
            $this->view->title = $page->title;
            $this->view->description = $page->description;
            $this->view->keywords = $page->keywords;
            $this->view->data = $page;
        } else {
            $this->view->title = "Not found";
            $this->view->description = "Not found";
            $this->view->keywords = "Not Found";
            $this->view->pick(['error/404']);
        }
    }


}

