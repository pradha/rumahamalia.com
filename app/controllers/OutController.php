<?php

class OutController extends \Phalcon\Mvc\Controller
{
    var $access_key;
    public function initialize(){
        $this->access_key=Options::findFirst(["name='access_key'"]);
        if (!$this->session->has('username')){
            $this->response->redirect($this->url->get('index/'.$this->access_key->value.'/no-access'));
        }
    }
    public function indexAction($param=null)
    {
        $access_key=Options::findFirst(["name='access_key'"]);
        $this->view->title="Sign Out";
        $this->view->disable();
        if ($this->session->has('username')){
            $user=Users::findFirst(["username='".$this->session->get('username')."'"]);
            if (isset($user->username)){
                $user->is_logged_in=false;
                $user->save();
            }

        }
        $this->session->destroy();
        if ($param=="kicked"){
            echo "You have been kicked";
        } else if ($param=="timeout"){
            echo 'Your session has been timeout. please re-<a href="'.$this->url->get('/index/').$this->access_key->value.'">Login</a>';
        } else {
            $this->response->redirect('/index/'.$this->access_key->value);
        }

    }
}

