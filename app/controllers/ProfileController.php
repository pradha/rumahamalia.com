<?php

class ProfileController extends \Phalcon\Mvc\Controller
{
    var $access_key;
    var $function;
    public function initialize()
    {
        $this->view->setLayout('dashboard');
        $this->access_key=Options::findFirst(["name='access_key'"]);
        if (!$this->session->has('username')){
            $this->response->redirect($this->url->get('/index/'.$this->access_key->value.'/no-access'));
        }
        $this->view->options = Options::find(["order" => "created_at asc"]);
        $this->view->title="My Profile";
        $this->view->menu = "profile";
        $this->function=new Functions();
    }
    public function indexAction()
    {
        if ($this->request->isAjax())
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);

        $this->view->menu="profile";
        $this->view->title="My Profile";
        $this->view->user=VUsers::findFirst(["username='".$this->session->get('username')."'"]);
    }

    public function editAction($status=null){
        if ($this->request->isAjax())
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $this->view->status=$status;
        $this->view->menu="profile";
        $this->view->title="Edit Profile";
        $error=new ArrayObject();
        $this->view->user=VUsers::findFirst(["username='".$this->session->get('username')."'"]);
        $this->view->genders=Gender::find();

        if ($this->request->isPost()){
            $user=Users::findFirst(["username='".$this->session->get('username')."'"]);
            $checkMail=Users::findFirst(["username!='".$this->session->get('username')."' AND email='".$this->request->getPost('email')."'"]);
            if (isset($checkMail->email) || $this->request->getPost('name')=="" || $this->request->getPost('phone')=="" || $this->request->getPost('gender')=="" || $this->request->getPost('date_of_birth')=="" || $this->request->getPost('biography')==""){
                if (isset($checkMail->email)) $error->email=true;
                if ($this->request->getPost('name')=="") $error->name=true;
                if ($this->request->getPost('phone')=="") $error->phone=true;
                if ($this->request->getPost('date_of_birth')=="") $error->date_of_birth=true;
                if ($this->request->getPost('biography')=="") $error->biography=true;
            } else {
                $user->email=$this->request->getPost('email');
                $user->name=$this->request->getPost('name');
                $user->phone=$this->request->getPost('phone');
                $user->gender=$this->request->getPost('gender');
                $user->date_of_birth=$this->request->getPost('date_of_birth');
                $user->biography=$this->request->getPost('biography');
                if ($user->save()){
                    $this->response->redirect('/profile/edit/success');
                } else {
                    $error->save=$user->getMessages();
                }
            }
        }
        $this->view->error=$error;
        $this->view->pick(['profile/profile-edit']);
    }

    public function passwordAction($status=null){
        if ($this->request->isAjax())
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $this->view->status=$status;
        $this->view->menu="password";
        $this->view->title="Change Password";
        $this->view->user=$vuser=VUsers::findFirst(["username='".$this->session->get('username')."'"]);
        $error=new ArrayObject();
        $function=new Functions();
        if ($this->request->isPost()){
            if (!password_verify($this->request->getPost('old'),$vuser->password) || strlen($this->request->getPost('new'))<=6 || $this->request->getPost('new')!=$this->request->getPost('renew')){
                if (!password_verify($this->request->getPost('old'),$vuser->password)) $error->password=true;
                if (strlen($this->request->getPost('new'))<=6) $error->length=true;
                if ($this->request->getPost('new')!=$this->request->getPost('renew')) $error->repeat=true;
                $this->view->error=$error;
            } else {
                $user=Users::findFirst(["username='".$this->session->get('username')."'"]);
                $user->password=$function->cryptPass($this->request->getPost('new'));
                $user->updated_by=$this->session->get('username');
                $user->updated_at=date('Y-m-d');
                if ($user->save()){
                    $this->response->redirect('/profile/password/success');
                } else {
                    $error->save=$user->getMessages();
                    $this->view->error=$error;
                }
            }
            $this->view->error=$error;
        }
    }
    public function uploadPictAction(){
        $this->view->disable();
        $user=Users::findFirst(["username='".$this->session->get('username')."'"]);

        $dir='./files/upload/profile/';

        unlink($dir.$user->photo);
        $extension = pathinfo(basename($_FILES["file"]["name"]), PATHINFO_EXTENSION);
        $filename = $this->function->slugify(null,pathinfo(basename($_FILES["file"]["name"]), PATHINFO_FILENAME));

        $newName=$filename.".".$extension;
        $counter=1;
        while (file_exists( $dir.$newName ))
            $newName = $filename . '-' . $counter++ . '.' . $extension;

        $new_path=$dir.$newName;
        $content_type = mime_content_type($_FILES["file"]["tmp_name"]);
        $file_size = filesize($_FILES["file"]["tmp_name"]);
        if (move_uploaded_file($_FILES["file"]["tmp_name"], $new_path)) {
            $user->photo=basename($new_path);
            $user->updated_by=$this->session->get('username');
            $user->updated_at=date('Y-m-d H:i:s');
            if ($user->save()){
                echo $this->url->get('/files/upload/profile/'.$user->photo.'?rand='.uniqid());
                $this->session->set('photo', $user->photo);
            } else {
                echo 'failed';
            }
        }
    }
    public function settingAction($status=null){
        if ($this->request->isAjax())
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
    }

}

