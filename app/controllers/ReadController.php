<?php

class ReadController extends \Phalcon\Mvc\Controller
{
    public $options;
    public function initialize()
    {
        $this->view->setLayout('layout');
        $this->view->pages=Pages::find(["parent is null AND status=8"]);
    }
    public function indexAction($slug)
    {
        $post=Posts::findFirst(array("conditions"=>"slug='".$slug."' AND status=8"));
        if (isset($post->id)){
            $this->view->title = $post->title;
            $this->view->description = $post->description;
            $this->view->keywords = $post->keywords;
            $this->view->image = firstImage($post->content);
            $this->view->data = $post;

            $title = array_filter(explode(" ", str_replace([':', '\\', '/', '*','!',',','.','"','“','”','?','\''], '',$post->title)));
            $tags = array_filter(array_map('trim', explode(',', $post->keywords)));

            $keywords = array_diff(array_map('strtolower',array_merge($title, $tags)), array('dan', 'di', 'yuk','kita','hal','itu', 'untuk'));

            $this->view->keywords=$keywords;
            $this->view->tags=$tags;
            $query="";
            $i=1;
            foreach($keywords as $keyword){
                $query .= "LOWER(title) LIKE LOWER('%".trim($keyword)."%')";
                if($i < count($keywords)) $query .= " OR ";
                $i++;
            }
            $this->view->query=$query;

            $this->view->postBefore=Posts::findFirst(array("conditions"=>"published_at<'".$post->published_at."' AND status=1","order"=>"published_at DESC"));
            $this->view->postAfter=Posts::findFirst(array("conditions"=>"published_at>'".$post->published_at."' AND status=1","order"=>"published_at ASC"));

            $this->view->latests = Posts::find(array("conditions"=>"status=1","order"=>"published_at desc","limit"=>"5"));
            $this->view->relates = Posts::find(array("conditions"=>"status=1 AND id!='".$post->id."' AND (".$query.")" ,"order"=>"published_at desc","limit"=>"8"));
            $this->view->categories=VPostInCategory::find(array("conditions"=>"post='".$post->id."'"));
            if ($this->session->get('username')==""){
                $post->count=$post->count+1;
                $post->save();
            }
        } else {
            $this->view->title = "Not found";
            $this->view->description = "Not found";
            $this->view->keywords = "Not Found";

            $this->view->latests = Posts::find(array("conditions"=>"status=1","order"=>"published_at desc","limit"=>"12"));

            $this->view->pick(['error/404']);
        }
    }


}

