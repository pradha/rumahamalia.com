<?php

class LibraryController extends \Phalcon\Mvc\Controller
{
    var $access_key;
    var $function;
    var $options;
    public function initialize()
    {
        $this->view->setLayout('dashboard');
        $this->access_key=Options::findFirst(["name='access_key'"]);
        if (!$this->session->has('username')){
            $this->response->redirect($this->url->get('/index/'.$this->access_key->value.'/no-access'));
        }
        $this->view->options = $this->options = Options::find(["order" => "created_at asc"]);
        $this->view->title="Libraries";
        $this->view->menu = "library";
        $this->function = new Functions();
    }

    public function indexAction()
    {

    }

    public function bookAction($target=null, $id=null, $status=null){
        if ($this->request->isAjax())
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $this->view->menu="book";
        $this->view->title="Books";
        $this->view->target=$target;
        $this->view->id=$id;
        $this->view->status=$status;
        $error=new ArrayObject();
        $this->view->authors=BookAuthors::find();
        $this->view->genres=BookGenres::find();
        if ($target=="add"){
            $this->view->title="Add Book";
            if ($this->request->isPost()){
                $genres = $this->request->getPost('genre');
                if (count($genres) <= 0 || trim($this->request->getPost('quantity'))=="" || trim($this->request->getPost('title'))=="" || trim($this->request->getPost('description'))=="" || trim($this->request->getPost('author'))==""  || trim($this->request->getPost('publication_date'))==""){
                    if (count($genres) <= 0) $error->genre=true;
                    if (trim($this->request->getPost('title'))=="") $error->title=true;
                    if (trim($this->request->getPost('description'))=="") $error->description=true;
                    if (trim($this->request->getPost('author'))=="") $error->author=true;
                    if (trim($this->request->getPost('publication_date'))=="") $error->publication_date=true;
                    if (trim($this->request->getPost('quantity'))=="") $error->quantity=true;
                } else {
                    $book=new Books();
                    $book->id=$this->function->createUUID();
                    $book->number = hexdec(uniqid());
                    $book->isbn = trim($this->request->getPost('isbn'))=="" ? null : trim($this->request->getPost('isbn'));
                    $book->title = trim($this->request->getPost('title'));
                    $book->description = trim($this->request->getPost('description'));
                    $book->author = trim($this->request->getPost('author'));
                    $book->quantity = trim($this->request->getPost('quantity'));
                    $book->publication_date = trim($this->request->getPost('publication_date'));
                    $book->created_by = $book->updated_by = $this->request->get('username');
                    if ($book->create()){
                        foreach ($genres as $genre) {
                            $genreBook = new GenreOfBook();
                            $genreBook->book = $book->id;
                            $genreBook->genre = $genre;
                            $genreBook->create();
                        }
                        $this->response->redirect('/library/book/edit/'.$book->id.'/success');
                    } else {
                        $error->save=$book->getMessages();
                    }
                }
                $this->view->error=$error;
            }
            $this->view->pick(['library/book-add']);
        } else if ($target=="edit"){
            $this->view->title="Edit Book";
            if ($this->function->isUUID($id)){
                $book=Books::findFirst(["id='".$id."'"]);
                if (isset($book->id)){
                    $this->view->book=$book;
                    $this->view->genreBook = GenreOfBook::find(array("columns" => "genre", "conditions" => "book='" . $book->id . "'"))->toArray(true);
                    $this->view->pick(['library/book-edit']);
                } else
                    $this->view->pick(['error/404']);
            } else  $this->view->pick(['error/404']);
        } else if ($target=="upload"){
            $this->view->disable();
            if ($this->function->isUUID($id)){
                $book=Books::findFirst(["id='".$id."'"]);
                if (isset($book->id)){
                    $dir='./files/upload/book/cover/';

                    if ($book->cover!="")
                        unlink($dir.$book->cover);
                    $extension = pathinfo(basename($_FILES["file"]["name"]), PATHINFO_EXTENSION);
                    $filename = $this->function->slugify(null,pathinfo(basename($_FILES["file"]["name"]), PATHINFO_FILENAME));

                    $newName=$filename.".".$extension;
                    $counter=1;
                    while (file_exists( $dir.$newName ))
                        $newName = $filename . '-' . $counter++ . '.' . $extension;

                    $new_path=$dir.$newName;
                    $content_type = mime_content_type($_FILES["file"]["tmp_name"]);
                    $file_size = filesize($_FILES["file"]["tmp_name"]);
                    if (move_uploaded_file($_FILES["file"]["tmp_name"], $new_path)) {
                        $book->cover=basename($new_path);
                        $book->updated_by=$this->session->get('username');
                        $book->updated_at=date('Y-m-d H:i:s');
                        if ($book->save()){
                            echo $this->url->get('/files/upload/book/cover/'.$book->cover.'?rand='.uniqid());
                        } else {
                            echo 'failed';
                        }
                    } else echo 'failed';
                } else echo 'failed';
            } else  echo 'failed';
        } else if ($target=="detail"){
            $this->view->title="Detail Book";
        } else {
            if (haveRole($this->session->get('group'),18)) {
                $data=VBooks::find(["order"=>"created_at"]);
                if (is_numeric($target))
                    $currentpage = $target;
                else
                    $currentpage = 1;
                $paginator = new Phalcon\Paginator\Adapter\Model(
                    array(
                        "data" => $data,
                        "limit" => 20,
                        "page" => $currentpage
                    )
                );
                $this->view->page = $page = $paginator->getPaginate();
            } else {
                $this->view->pick(['error/403']);
            }
        }
    }

    public function authorAction($target=null, $id=null, $status=null){
        if ($this->request->isAjax())
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $this->view->menu="book-authors";
        $this->view->title="Book Authors";
        $this->view->target=$target;
        $this->view->id=$id;
        $this->view->status=$status;
        $error=new ArrayObject();
        if ($target=="add"){
            $this->view->title="Add Author";
            if ($this->request->isPost()){
                if (trim($this->request->getPost('name'))=="" || trim($this->request->getPost('information'))==""){
                    if (trim($this->request->getPost('name'))=="") $error->name=true;
                    if (trim($this->request->getPost('information'))=="") $error->information=true;
                } else {
                    $author=new BookAuthors();
                    $author->id=$this->function->createUUID();
                    $author->name=trim($this->request->getPost('name'));
                    $author->information=trim($this->request->getPost('information'));
                    $author->created_by=$author->updated_by=$this->session->get('username');
                    if ($author->create()){
                        $this->response->redirect('/library/author/edit/'.$author->id.'/success');
                    } else {
                        $error->save=$author->getMessages();
                    }
                    $this->view->error=$error;
                }
            }
            $this->view->pick(['library/author-add']);
        } else if ($target=="edit"){
            $this->view->title="Edit Author";
            if ($this->function->isUUID($id)){
                $author=BookAuthors::findFirst(["id='".$id."'"]);
                if (isset($author->id)){
                    if ($this->request->isPost()){
                        if (trim($this->request->getPost('name'))=="" || trim($this->request->getPost('information'))==""){
                            if (trim($this->request->getPost('name'))=="") $error->name=true;
                            if (trim($this->request->getPost('information'))=="") $error->information=true;
                        } else {
                            $author->name=trim($this->request->getPost('name'));
                            $author->information=trim($this->request->getPost('information'));
                            $author->created_by=$author->updated_by=$this->session->get('username');
                            if ($author->save()){
                                $this->response->redirect('/library/author/edit/'.$author->id.'/success');
                            } else {
                                $error->save=$author->getMessages();
                            }
                            $this->view->error=$error;
                        }
                    }
                    $this->view->author=$author;
                    $this->view->pick(['library/author-edit']);
                } else {
                    $this->view->pick(['error/404']);
                }
            } else {
                $this->view->pick(['error/404']);
            }
        } else {
            if (haveRole($this->session->get('group'),18)) {
                $data=BookAuthors::find(["order"=>"created_at"]);
                if (is_numeric($target))
                    $currentpage = $target;
                else
                    $currentpage = 1;
                $paginator = new Phalcon\Paginator\Adapter\Model(
                    array(
                        "data" => $data,
                        "limit" => 20,
                        "page" => $currentpage
                    )
                );
                $this->view->page = $page = $paginator->getPaginate();
            } else {
                $this->view->pick(['error/403']);
            }
        }
    }

    public function genreAction($target=null, $id=null, $status=null){
        if ($this->request->isAjax())
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $this->view->menu="book-genres";
        $this->view->title="Book Genres";
        $this->view->target=$target;
        $this->view->id=$id;
        $this->view->status=$status;
        $error=new ArrayObject();
        if ($target=="add"){
            $this->view->title="Add Genre";
            if ($this->request->isPost()){
                if (trim($this->request->getPost('name'))=="" || trim($this->request->getPost('description'))==""){
                    if (trim($this->request->getPost('name'))=="") $error->name=true;
                    if (trim($this->request->getPost('description'))=="") $error->description=true;
                } else {
                    $genre=new BookGenres();
                    $genre->id=$this->function->createUUID();
                    $genre->name=trim($this->request->getPost('name'));
                    $genre->description=trim($this->request->getPost('description'));
                    $genre->created_by=$genre->updated_by=$this->session->get('username');
                    if ($genre->create()){
                        $this->response->redirect('/library/genre/edit/'.$genre->id.'/success');
                    } else {
                        $error->save=$genre->getMessages();
                    }
                    $this->view->error=$error;
                }
            }
            $this->view->pick(['library/genre-add']);
        } else if ($target=="edit"){
            $this->view->title="Edit Genre";
            if ($this->function->isUUID($id)){
                $genre=BookGenres::findFirst(["id='".$id."'"]);
                if (isset($genre->id)){
                    if ($this->request->isPost()){
                        if (trim($this->request->getPost('name'))=="" || trim($this->request->getPost('description'))==""){
                            if (trim($this->request->getPost('name'))=="") $error->name=true;
                            if (trim($this->request->getPost('description'))=="") $error->description=true;
                        } else {
                            $genre->name=trim($this->request->getPost('name'));
                            $genre->description=trim($this->request->getPost('description'));
                            $genre->created_by=$genre->updated_by=$this->session->get('username');
                            if ($genre->save()){
                                $this->response->redirect('/library/genre/edit/'.$genre->id.'/success');
                            } else {
                                $error->save=$genre->getMessages();
                            }
                        }
                    }
                    $this->view->genre=$genre;
                    $this->view->error=$error;
                    $this->view->pick(['library/genre-edit']);
                } else {
                    $this->view->pick(['error/404']);
                }
            } else {
                $this->view->pick(['error/404']);
            }
        } else {
            if (haveRole($this->session->get('group'),18)) {
                $data=BookGenres::find(["order"=>"created_at"]);
                if (is_numeric($target))
                    $currentpage = $target;
                else
                    $currentpage = 1;
                $paginator = new Phalcon\Paginator\Adapter\Model(
                    array(
                        "data" => $data,
                        "limit" => 20,
                        "page" => $currentpage
                    )
                );
                $this->view->page = $page = $paginator->getPaginate();
            } else {
                $this->view->pick(['error/403']);
            }
        }
    }
}

