<?php

class ContentController extends \Phalcon\Mvc\Controller
{
    var $access_key;
    var $function;
    var $options;
    public function initialize()
    {
        $this->view->setLayout('dashboard');
        $this->access_key=Options::findFirst(["name='access_key'"]);
        if (!$this->session->has('username')){
            $this->response->redirect($this->url->get('/index/'.$this->access_key->value.'/no-access'));
        }
        $this->view->options = $this->options = Options::find(["order" => "created_at asc"]);
        $this->view->title="System Contents";
        $this->view->menu = "content";
        $this->function = new Functions();
    }
    public function indexAction()
    {

    }

    public function pageAction($target=null, $id=null, $status=null){
        if ($this->request->isAjax())
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $this->view->menu="page";
        $this->view->title="Pages";
        $this->view->target=$target;
        $this->view->id=$id;
        $this->view->status=$status;
        $error=new ArrayObject();
        $this->view->parents=Pages::find(["parent is null"]);
        if ($target=="add"){
            if (haveRole($this->session->get('group'),19)) {
                $this->view->title = "Add Page";
                if ($this->request->isPost()) {
                    if (trim($this->request->getPost('title') == "") || trim($this->request->getPost('content') == "") || trim($this->request->getPost('description') == "") || trim($this->request->getPost('keywords') == "")) {
                        if (trim($this->request->getPost('title') == "")) $error->title = true;
                        if (trim($this->request->getPost('content') == "")) $error->content = true;
                        if (trim($this->request->getPost('description') == "")) $error->description = true;
                        if (trim($this->request->getPost('keywords') == "")) $error->keywords = true;
                    } else {
                        $page = new Pages();
                        $page->id = $this->function->createUUID();
                        $page->parent = trim($this->request->getPost('parent')) == "" ? NULL : trim($this->request->getPost('parent'));
                        $page->title = trim($this->request->getPost('title'));
                        $page->content = trim($this->request->getPost('content'));
                        $page->description = trim($this->request->getPost('description'));
                        $page->slug = $this->function->slugify("page", trim($this->request->getPost('title')));
                        $page->keywords = trim($this->request->getPost('keywords'));
                        $page->created_by = $page->updated_by = $this->session->get('username');
                        if ($page->create()) {
                            $this->response->redirect('/content/page/edit/' . $page->id . '/success');
                        } else {
                            $error->save = $page->getMessages();
                        }
                    }
                    $this->view->error = $error;
                }
                $this->view->pick(['content/page-add']);
            } else {
                $this->view->pick(['error/403']);
            }
        } else if ($target=="edit"){
            if (haveRole($this->session->get('group'),20)) {
                $this->view->title = "Edit Page";
                if ($this->function->isUUID($id)) {
                    $page = Pages::findFirst(["id='" . $id . "'"]);
                    if (isset($page->id)) {
                        $this->view->page = $page;
                        if ($this->request->isPost()) {
                            if (trim($this->request->getPost('title') == "") || trim($this->request->getPost('content') == "") || trim($this->request->getPost('description') == "") || trim($this->request->getPost('keywords') == "")) {
                                if (trim($this->request->getPost('title') == "")) $error->title = true;
                                if (trim($this->request->getPost('content') == "")) $error->content = true;
                                if (trim($this->request->getPost('description') == "")) $error->description = true;
                                if (trim($this->request->getPost('keywords') == "")) $error->keywords = true;
                            } else {
                                $page->parent = trim($this->request->getPost('parent')) == "" ? NULL : trim($this->request->getPost('parent'));
                                $page->title = trim($this->request->getPost('title'));
                                $page->content = trim($this->request->getPost('content'));
                                $page->description = trim($this->request->getPost('description'));
                                $page->slug = $this->function->slugify("page", trim($this->request->getPost('title')), $page->id);
                                $page->keywords = trim($this->request->getPost('keywords'));
                                $page->updated_by = $this->session->get('username');
                                $page->updated_at = date('Y-m-d H:i:s');
                                if ($page->save()) {
                                    $this->response->redirect('/content/page/edit/' . $page->id . '/success');
                                } else {
                                    $error->save = $page->getMessages();
                                }
                            }
                            $this->view->error = $error;
                        }
                        $this->view->pick(['content/page-edit']);
                    } else {
                        $this->view->pick(['error/404']);
                    }
                } else {
                    $this->view->pick(['error/404']);
                }

            } else {
                $this->view->pick(['error/403']);
            }
        } else if ($target=="preview"){
            $this->view->title="Preview Page";
            $this->view->pick(['content/page-preview']);
        } else if ($target=="search"){
            if ($this->request->isPost() && trim($this->request->getPost('search'))!=""){
                $txt=$this->request->getPost('search');
                $this->view->search=$txt;
                $this->view->pages=Pages::find(["title like '%".$txt."%' OR content like '%".$txt."%' OR keywords LIKE '%".$txt."%'"]);
                $this->view->pick(['content/page-search']);
            } else {
                $this->view->pick(['error/404']);
            }
        } else if ($target=="publish"){
            if (haveRole($this->session->get('group'),21)) {
                $this->view->disable();
                if ($this->request->isPost()) {
                    $page = Pages::findFirst(["id='" . $this->request->getPost('id') . "'"]);
                    $page->status = 8;
                    $page->updated_by = $this->session->get('username');
                    $page->updated_at = date('Y-m-d H:i:s');
                    if ($page->save()) {
                        $this->response->redirect('/content/page/edit/' . $page->id . '/published');
                    }
                }
            } else {
                $this->view->pick(['error/403']);
            }
        } else {
            if (haveRole($this->session->get('group'),18)) {
                $data=VPages::find(["order"=>"created_at"]);
                if (is_numeric($target))
                    $currentpage = $target;
                else
                    $currentpage = 1;
                $paginator = new Phalcon\Paginator\Adapter\Model(
                    array(
                        "data" => $data,
                        "limit" => 20,
                        "page" => $currentpage
                    )
                );
                $this->view->page = $page = $paginator->getPaginate();
            } else {
                $this->view->pick(['error/403']);
            }
        }
    }
    public function categoryAction($target=null, $id=null, $status=null){
        if ($this->request->isAjax())
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $this->view->target=$target;
        $this->view->id=$id;
        $this->view->status=$status;
        $this->view->menu="category";
        $this->view->title="Categories";
        $this->view->target=$target;
        $this->view->id=$id;
        $this->view->status=$status;
        $error=new ArrayObject();
        $this->view->parents=Categories::find(["parent is null"]);
        $function=new Functions();
        if ($target=="add"){
            if (haveRole($this->session->get('group'),30)) {
                $this->view->title = "Add Category";
                if ($this->request->isPost()) {
                    if (trim($this->request->getPost('name') == "") || trim($this->request->getPost('description') == "")) {
                        if (trim($this->request->getPost('name')) == "") $error->name = true;
                        if (trim($this->request->getPost('description')) == "") $error->description = true;
                    } else {
                        $category = new Categories();
                        $category->id = $function->createUUID();
                        $category->parent = trim($this->request->getPost('parent')) == "" ? NULL : trim($this->request->getPost('parent'));
                        $category->name = trim($this->request->getPost('name'));
                        $category->description = trim($this->request->getPost('description'));
                        $category->keywords = trim($this->request->getPost('keywords'));
                        $category->options = trim($this->request->getPost('option')) == "" ? null : trim($this->request->getPost('option'));
                        $category->slug = $function->slugify("category", trim($this->request->getPost('name')));
                        $category->created_by = $category->updated_by = $this->session->get('username');
                        if ($category->create()) {
                            $this->response->redirect('/content/category/edit/' . $category->id . '/success');
                        } else {
                            $error->save = $category->getMessages();
                            $this->view->error = $error;
                        }
                    }
                    $this->view->error = $error;
                }
                $this->view->pick(['content/category-add']);
            } else {
                $this->view->pick(['error/403']);
            }
        }  else if ($target=="edit"){
            if (haveRole($this->session->get('group'),31)) {
                $this->view->title = "Edit Category";
                if ($function->isUUID($id)) {
                    $category = Categories::findFirst(["id='" . $id . "'"]);
                    if (isset($category->id)) {
                        if ($this->request->isPost()) {
                            if (trim($this->request->getPost('name') == "") || trim($this->request->getPost('keywords') == "") || trim($this->request->getPost('description') == "")) {
                                if (trim($this->request->getPost('name')) == "") $error->name = true;
                                if (trim($this->request->getPost('description')) == "") $error->description = true;
                                if (trim($this->request->getPost('keywords')) == "") $error->keywords = true;
                            } else {
                                $category->parent = trim($this->request->getPost('parent')) == "" ? NULL : trim($this->request->getPost('parent'));
                                $category->name = trim($this->request->getPost('name'));
                                $category->description = trim($this->request->getPost('description'));
                                $category->keywords = trim($this->request->getPost('keywords'));
                                $category->options = trim($this->request->getPost('option')) == "" ? null : trim($this->request->getPost('option'));
                                $category->slug = $function->slugify("category", trim($this->request->getPost('name')), $category->id);
                                $category->updated_by = $this->session->get('username');
                                $category->updated_at = date('Y-m-d H:i:s');
                                if ($category->save()) {
                                    $this->response->redirect('/content/category/edit/' . $category->id . '/success');
                                } else {
                                    $error->save = $category->getMessages();
                                    $this->view->error = $error;
                                }
                            }
                        }
                        $this->view->category = $category;
                        $this->view->pick(['content/category-edit']);
                    } else {
                        $this->view->pick(['error/404']);
                    }
                } else {
                    $this->view->pick(['error/404']);
                }
            } else {
                $this->view->pick(['error/403']);
            }
        } else if ($target=="search"){
            if ($this->request->isPost() && trim($this->request->getPost('search'))!=""){
                $txt=$this->request->getPost('search');
                $this->view->search=strtolower($txt);
                $this->view->categories=Categories::find(["lower(name) like '%".$txt."%' OR lower(description) like '%".$txt."%' OR lower(keywords) LIKE '%".$txt."%'"]);
                $this->view->pick(['content/category-search']);
            } else {
                $this->view->pick(['error/404']);
            }
        } else {
            if (haveRole($this->session->get('group'),29)) {
                $data = VCategories::find(["order" => "created_at"]);
                if (is_numeric($target))
                    $currentpage = $target;
                else
                    $currentpage = 1;
                $paginator = new Phalcon\Paginator\Adapter\Model(
                    array(
                        "data" => $data,
                        "limit" => 20,
                        "page" => $currentpage
                    )
                );
                $this->view->page = $page = $paginator->getPaginate();
            }  else {
                $this->view->pick(['error/403']);
            }
        }
    }

    public function postAction($target=null, $id=null, $status=null){
        if ($this->request->isAjax())
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $this->view->menu="post";
        $this->view->title="Posts";
        $this->view->target=$target;
        $this->view->id=$id;
        $this->view->status=$status;
        $error=new ArrayObject();
        $this->view->categories=Categories::find(["parent is null"]);
        if ($target=="add"){
            if (haveRole($this->session->get('group'),24)) {
                $this->view->title = "Add Post";
                if ($this->request->isPost()) {
                    $cats = $this->request->getPost('category');
                    if (count($cats) <= 0 || trim($this->request->getPost('title') == "") || trim($this->request->getPost('content') == "") || trim($this->request->getPost('description') == "") || trim($this->request->getPost('keywords') == "")) {
                        if (count($cats) <= 0) $error->category = true;
                        if (trim($this->request->getPost('title') == "")) $error->title = true;
                        if (trim($this->request->getPost('content') == "")) $error->content = true;
                        if (trim($this->request->getPost('description') == "")) $error->description = true;
                        if (trim($this->request->getPost('keywords') == "")) $error->keywords = true;
                    } else {
                        $post = new Posts();
                        $post->id = $this->function->createUUID();
                        $post->title = trim($this->request->getPost('title'));
                        $post->content = trim($this->request->getPost('content'));
                        $post->description = trim($this->request->getPost('description'));
                        $post->slug = $this->function->slugify("page", trim($this->request->getPost('title')));
                        $post->keywords = trim($this->request->getPost('keywords'));
                        $post->created_by = $post->updated_by = $this->session->get('username');
                        if ($post->create()) {
                            foreach ($cats as $cat) {
                                $postCat = new PostInCategory();
                                $postCat->post = $post->id;
                                $postCat->category = $cat;
                                $postCat->create();
                            }
                            $this->response->redirect('/content/post/edit/' . $post->id . '/success');
                        } else {
                            $error->save = $post->getMessages();
                        }
                    }
                    $this->view->error = $error;
                }
                $this->view->pick(['content/post-add']);
            } else {
                $this->view->pick(['error/403']);
            }
        } else if ($target=="edit"){
            if (haveRole($this->session->get('group'),25) || haveRole($this->session->get('group'),26)) {
                $this->view->title = "Edit Post";
                if ($this->function->isUUID($id)) {
                    if (haveRole($this->session->get('group'),25))
                        $post = Posts::findFirst(["id='" . $id . "'"]);
                    else if  (haveRole($this->session->get('group'),26))
                        $post = Posts::findFirst(["id='" . $id . "' AND created_by='".$this->session->get('username')."'"]);
                    if (isset($post->id)) {
                        $this->view->postCat = PostInCategory::find(array("columns" => "category", "conditions" => "post='" . $post->id . "'"))->toArray(true);
                        $this->view->post = $post;
                        if ($this->request->isPost()) {
                            $cats = $this->request->getPost('category');
                            if (count($cats) <= 0 || trim($this->request->getPost('title') == "") || trim($this->request->getPost('content') == "") || trim($this->request->getPost('description') == "") || trim($this->request->getPost('keywords') == "")) {
                                if (count($cats) <= 0) $error->category = true;
                                if (trim($this->request->getPost('title') == "")) $error->title = true;
                                if (trim($this->request->getPost('content') == "")) $error->content = true;
                                if (trim($this->request->getPost('description') == "")) $error->description = true;
                                if (trim($this->request->getPost('keywords') == "")) $error->keywords = true;
                            } else {
                                $post->title = trim($this->request->getPost('title'));
                                $post->content = trim($this->request->getPost('content'));
                                $post->description = trim($this->request->getPost('description'));
                                $post->slug = $this->function->slugify("page", trim($this->request->getPost('title')));
                                $post->keywords = trim($this->request->getPost('keywords'));
                                $post->created_by = $post->updated_by = $this->session->get('username');
                                if ($post->save()) {
                                    foreach (PostInCategory::find(array("conditions" => "post='" . $post->id . "'")) as $remove) {
                                        $remove->delete();
                                    }
                                    foreach ($cats as $cat) {
                                        $postCat = new PostInCategory();
                                        $postCat->post = $post->id;
                                        $postCat->category = $cat;
                                        $postCat->create();
                                    }
                                    $this->response->redirect('/content/post/edit/' . $post->id . '/success');
                                } else {
                                    $error->save = $post->getMessages();
                                }
                            }
                            $this->view->error = $error;
                        }
                        $this->view->pick(['content/post-edit']);
                    } else {
                        $this->view->pick(['error/404']);
                    }
                } else {
                    $this->view->pick(['error/404']);
                }
            } else {
                $this->view->pick(['error/403']);
            }

        } else if ($target=="preview"){
            $this->view->title="Preview Post";
            $this->view->pick(['content/post-preview']);
        } else if ($target=="search"){
            if ($this->request->isPost() && trim($this->request->getPost('search'))!=""){
                $txt=$this->request->getPost('search');
                $this->view->search=strtolower($txt);
                $this->view->posts=Posts::find(["lower(title) like '%".$txt."%' OR lower(content) like '%".$txt."%' OR lower(keywords) LIKE '%".$txt."%'"]);
                $this->view->pick(['content/post-search']);
            } else {
                $this->view->pick(['error/404']);
            }
        } else if ($target=="publish"){
            if (haveRole($this->session->get('group'),27) || haveRole($this->session->get('group'),28)) {
                $this->view->disable();
                if ($this->request->isPost()) {
                    if (haveRole($this->session->get('group'),27))
                        $post = Posts::findFirst(["id='" . $this->request->getPost('id') . "'"]);
                    else if (haveRole($this->session->get('group'),28))
                        $post = Posts::findFirst(["id='" . $this->request->getPost('id') . "' AND created_by='".$this->session->get('username')."'"]);
                    if (isset($post->is)) {
                        $post->status = 8;
                        $post->updated_by = $post->published_by = $this->session->get('username');
                        $post->updated_at = $post->published_at = date('Y-m-d H:i:s');
                        if ($post->save()) {
                            $this->response->redirect('/content/post/edit/' . $post->id . '/published');
                        }
                    } else {
                        $this->view->pick(['error/404']);
                    }
                }
            } else {
                $this->view->pick(['error/403']);
            }
        } else {
            if (haveRole($this->session->get('group'),22) || haveRole($this->session->get('group'),23)) {
                if (haveRole($this->session->get('group'),22))
                    $data = VPosts::find(["order" => "created_at desc,status asc"]);
                else if (haveRole($this->session->get('group'),23))
                    $data = VPosts::find(["conditions"=>"created_by='".$this->session->get('username')."'","order" => "created_at desc,status asc"]);
                if (is_numeric($target))
                    $currentpage = $target;
                else
                    $currentpage = 1;
                $paginator = new Phalcon\Paginator\Adapter\Model(
                    array(
                        "data" => $data,
                        "limit" => 20,
                        "page" => $currentpage
                    )
                );
                $this->view->page = $page = $paginator->getPaginate();
            } else {
                $this->view->pick(['error/403']);
            }
        }
    }

    public function mediaAction($target=null, $id=null, $status=null){
        if ($this->request->isAjax())
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $this->view->menu="media";
        $this->view->title="Media";
        $this->view->target=$target;
        $this->view->id=$id;
        $this->view->status=$status;
        $error=new ArrayObject();
        if ($target=="add"){
            if (haveRole($this->session->get('group'),34)) {
                $this->view->title = "Add Media";
                $this->view->pick(['content/media-add']);
            } else {
                $this->view->pick(['error/403']);
            }
        } else if ($target=="edit"){
            if (haveRole($this->session->get('group'),35) || haveRole($this->session->get('group'),36)) {
                $this->view->title = "Edit Media";
                if ($this->function->isUUID($id)) {
                    if (haveRole($this->session->get('group'),35))
                        $media = Media::findFirst(["id='" . $id . "'"]);
                    else if (haveRole($this->session->get('group'),36))
                        $media = Media::findFirst(["id='" . $id . "' AND created_by='".$this->session->get('username')."'"]);
                    if (isset($media->id)) {
                        if ($this->request->isPost()) {
                            if (trim($this->request->getPost('keywords')) == "" || trim($this->request->getPost('name') == "") || trim($this->request->getPost('description') == "")) {
                                if (trim($this->request->getPost('name')) == "") $error->name = true;
                                if (trim($this->request->getPost('description')) == "") $error->description = true;
                                if (trim($this->request->getPost('keywords')) == "") $error->keywords = true;
                            } else {
                                $media->name = trim($this->request->getPost('name'));
                                $media->description = trim($this->request->getPost('description'));
                                $media->keywords = trim($this->request->getPost('keywords'));
                                $media->updated_by = $this->session->get('username');
                                $media->updated_at = date('Y-m-d H:i:s');
                                if ($media->save()) {
                                    $this->response->redirect('/content/media/edit/' . $media->id . '/success');
                                } else {
                                    $error->save = $media->getMessages();
                                    $this->view->error = $error;
                                }
                            }
                        }
                        $this->view->media = $media;
                        $this->view->pick(['content/media-edit']);
                    } else {
                        $this->view->pick(['error/404']);
                    }
                } else {
                    $this->view->pick(['error/404']);
                }
            } else {
                $this->view->pick(['error/403']);
            }

        } else if ($target=="upload"){
            if (haveRole($this->session->get('group'),34)) {
                $this->view->disable();
                $dir = $this->function->createFolder('./files/upload/');
                $extension = pathinfo(basename($_FILES["file"]["name"]), PATHINFO_EXTENSION);
                $filename = $this->function->slugify(null, pathinfo(basename($_FILES["file"]["name"]), PATHINFO_FILENAME));

                $newName = $filename . "." . $extension;
                $counter = 1;
                while (file_exists($dir . $newName))
                    $newName = $filename . '-' . $counter++ . '.' . $extension;

                $new_path = $dir . $newName;
                $content_type = mime_content_type($_FILES["file"]["tmp_name"]);
                $file_size = filesize($_FILES["file"]["tmp_name"]);
                if (move_uploaded_file($_FILES["file"]["tmp_name"], $new_path)) {
                    $media = new Media();
                    $media->id = $this->function->createUUID();
                    $media->name = pathinfo($_FILES["file"]["name"], PATHINFO_FILENAME);
                    $media->description = pathinfo($_FILES["file"]["name"], PATHINFO_FILENAME);
                    $media->content_type = $content_type;
                    $media->file_size = $file_size;
                    $media->extension = $extension;
                    $media->original_file = basename($new_path);
                    $media->keywords = str_replace("-", ",", $this->function->slugify(null, pathinfo($_FILES["file"]["name"], PATHINFO_FILENAME)));
                    $media->slug = $this->function->slugify(null, basename(preg_replace('/\\.[^.\\s]{3,4}$/', '', $new_path)));
                    $media->create();
                    $response = array(
                        'status' => 'success',
                        'message' => 'url',
                        'content_type' => $content_type,
                        'extension' => $extension,
                        'size' => $file_size,
                        'file' => $file_size,
                        'url' => $new_path
                    );
                } else {
                    $response = array(
                        'status' => 'failed',
                        'message' => 'url'
                    );
                }
            } else {
                $response = array(
                    'status' => 'failed',
                    'message' => 'No privilege'
                );
            }
            return json_encode($response);
        } else if ($target=="data"){
            $data=Media::find(["order"=>"created_at desc"]);
            if (is_numeric($target))
                $currentpage = $target;
            else
                $currentpage = 1;
            $paginator = new Phalcon\Paginator\Adapter\Model(
                array(
                    "data" => $data,
                    "limit" => 20,
                    "page" => $currentpage
                )
            );
            $this->view->page = $page = $paginator->getPaginate();
            $this->view->pick(['content/media-data']);
        } else if ($target=="insert"){
            if ($this->function->isUUID($id)) {
                $media = Media::findFirst(["id='" . $id . "'"]);
                if (isset($media->id)) {
                    $this->view->media=$media;
                    $this->view->pick(['content/media-insert']);
                } else {
                    $this->view->pick(['error/404']);
                }
            } else {
                $this->view->pick(['error/404']);
            }

        } else if ($target=="insertUpload"){
            if ($this->request->isPost()){
                $dir=$this->function->createFolder('./files/upload/');
                $extension = pathinfo(basename($_FILES["file"]["name"]), PATHINFO_EXTENSION);
                $filename = $this->function->slugify(null,pathinfo(basename($_FILES["file"]["name"]), PATHINFO_FILENAME));

                $newName=$filename.".".$extension;
                $counter=1;
                while (file_exists( $dir.$newName ))
                    $newName = $filename . '-' . $counter++ . '.' . $extension;

                $new_path=$dir.$newName;
                $content_type = mime_content_type($_FILES["file"]["tmp_name"]);
                $file_size = filesize($_FILES["file"]["tmp_name"]);
                if (move_uploaded_file($_FILES["file"]["tmp_name"], $new_path)) {
                    $media=new Media();
                    $media->id=$this->function->createUUID();
                    $media->name=pathinfo($_FILES["file"]["name"], PATHINFO_FILENAME);
                    $media->description=pathinfo($_FILES["file"]["name"], PATHINFO_FILENAME);
                    $media->content_type=$content_type;
                    $media->file_size=$file_size;
                    $media->extension=$extension;
                    $media->original_file=basename($new_path);
                    $media->keywords=str_replace("-",",",$this->function->slugify(null,pathinfo($_FILES["file"]["name"],PATHINFO_FILENAME)));
                    $media->slug=$this->function->slugify(null,basename(preg_replace('/\\.[^.\\s]{3,4}$/', '', $new_path)));
                    $media->create();
                    $response = array(
                        'status' => 'success',
                        'message'    => 'url',
                        'content_type'    => $content_type,
                        'extension'    => $extension,
                        'size'    => $file_size,
                        'file'    => $file_size,
                        'url'    => $new_path,
                        'id'    =>  $media->id
                    );
                } else {
                    $response = array(
                        'status' => 'failed',
                        'message'    => 'url'
                    );
                }
                return json_encode($response);
            } else {
                $this->view->pick(['content/media-upload']);
            }

        } else if ($target=="search"){
            if ($this->request->isPost() && trim($this->request->getPost('search'))!=""){
                $txt=$this->request->getPost('search');
                $this->view->search=strtolower($txt);
                $this->view->media=Media::find(["lower(name) like '%".$txt."%' OR lower(description) like '%".$txt."%' OR lower(keywords) LIKE '%".$txt."%'"]);
                $this->view->pick(['content/media-search']);
            } else {
                $this->view->pick(['error/404']);
            }
        } else {
            if (haveRole($this->session->get('group'),32) || haveRole($this->session->get('group'),33)) {
                if (haveRole($this->session->get('group'),32))
                    $data = Media::find(["order" => "created_at desc"]);
                else if (haveRole($this->session->get('group'),33))
                    $data = Media::find(["conditions"=>"created_by='".$this->session->get('username')."'","order" => "created_at desc"]);
                if (is_numeric($target))
                    $currentpage = $target;
                else
                    $currentpage = 1;
                $paginator = new Phalcon\Paginator\Adapter\Model(
                    array(
                        "data" => $data,
                        "limit" => 15,
                        "page" => $currentpage
                    )
                );
                $this->view->page = $page = $paginator->getPaginate();
            } else {
                $this->view->pick(['error/403']);
            }
        }
    }
    public function sliderAction($target=null, $id=null, $status=null){
        if ($this->request->isAjax())
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $this->view->menu="slider";
        $this->view->title="Slider";
        $this->view->target=$target;
        $this->view->id=$id;
        $this->view->status=$status;
        $error=new ArrayObject();
        $this->view->categories=Categories::find(["parent is null"]);
        if ($target=="add"){
            if (haveRole($this->session->get('group'),38)) {
                $this->view->title = "Add Slider";
                if ($this->request->isPost()) {
                    if (trim($this->request->getPost('title')) == "" || trim($this->request->getPost('description')) == "" || trim($this->request->getPost('url')) == "" || trim($this->request->getPost('image')) == "") {
                        if (trim($this->request->getPost('title')) == "") $error->title = true;
                        if (trim($this->request->getPost('description')) == "") $error->description = true;
                        if (trim($this->request->getPost('url')) == "") $error->url = true;
                        if (trim($this->request->getPost('image')) == "") $error->image = true;
                    } else {
                        $slider = new Sliders();
                        $slider->id = $this->function->createUUID();
                        $slider->title = trim($this->request->getPost('title'));
                        $slider->description = trim($this->request->getPost('description'));
                        $slider->url = trim($this->request->getPost('url'));
                        $slider->image = trim($this->request->getPost('image'));
                        $slider->placement = trim($this->request->getPost('placement')) == "" ? null : trim($this->request->getPost('placement'));
                        $slider->created_by = $slider->updated_by = $this->session->get('username');
                        if ($slider->create()) {
                            $this->response->redirect('/content/slider/edit/' . $slider->id . '/success');
                        } else $error->save = $slider->getMessages();
                    }
                }
                $this->view->error = $error;
                $this->view->pick(['content/slider-add']);
            } else {
                $this->view->pick(['error/403']);
            }
        } else if ($target=="edit"){
            if (haveRole($this->session->get('group'),39)) {
                $this->view->title = "Edit Media";
                if ($this->function->isUUID($id)) {
                    $slider = Sliders::findFirst(["id='" . $id . "'"]);
                    if (isset($slider->id)) {
                        $this->view->statuses = Status::find(["order" => "id"]);
                        $this->view->slider = $slider;
                        if ($this->request->isPost()) {
                            if (trim($this->request->getPost('title')) == "" || trim($this->request->getPost('description')) == "" || trim($this->request->getPost('url')) == "" || trim($this->request->getPost('image')) == "") {
                                if (trim($this->request->getPost('title')) == "") $error->title = true;
                                if (trim($this->request->getPost('description')) == "") $error->description = true;
                                if (trim($this->request->getPost('url')) == "") $error->url = true;
                                if (trim($this->request->getPost('image')) == "") $error->image = true;
                            } else {
                                $slider->title = trim($this->request->getPost('title'));
                                $slider->description = trim($this->request->getPost('description'));
                                $slider->url = trim($this->request->getPost('url'));
                                $slider->image = trim($this->request->getPost('image'));
                                $slider->placement = trim($this->request->getPost('placement')) == "" ? null : trim($this->request->getPost('placement'));
                                $slider->status = trim($this->request->getPost('status'));
                                $slider->updated_by = $this->session->get('username');
                                $slider->updated_at = date('Y-m-d H:i:s');
                                if ($slider->save()) {
                                    $this->response->redirect('/content/slider/edit/' . $slider->id . '/success');
                                } else $error->save = $slider->getMessages();
                            }
                        }
                        $this->view->pick(['content/slider-edit']);
                    } else {
                        $this->view->pick(['error/404']);
                    }
                } else {
                    $this->view->pick(['error/404']);
                }
            } else {
                $this->view->pick(['error/403']);
            }
        } else if ($target=="search"){
            if ($this->request->isPost() && trim($this->request->getPost('search'))!=""){
                $txt=$this->request->getPost('search');
                $this->view->search=strtolower($txt);
                $this->view->sliders=Sliders::find(["lower(title) like '%".$txt."%' OR lower(description) like '%".$txt."%'"]);
                $this->view->pick(['content/slider-search']);
            } else {
                $this->view->pick(['error/404']);
            }
        } else {
            if (haveRole($this->session->get('group'),37)) {
                $data = VSliders::find(["order" => "created_at"]);
                if (is_numeric($target))
                    $currentpage = $target;
                else
                    $currentpage = 1;
                $paginator = new Phalcon\Paginator\Adapter\Model(
                    array(
                        "data" => $data,
                        "limit" => 20,
                        "page" => $currentpage
                    )
                );
                $this->view->page = $page = $paginator->getPaginate();
            } else {
                $this->view->pick(['error/403']);
            }
        }
    }

    public function commentAction($target=null, $id=null, $status=null){
        if ($this->request->isAjax())
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $this->view->menu="comment";
        $this->view->title="Comments";
        $this->view->target=$target;
        $this->view->id=$id;
        $this->view->status=$status;
        $error=new ArrayObject();
        if ($target=="detail"){
            $this->view->pick(['content/comment-detail']);
        } else {
            if (haveRole($this->session->get('group'),41) || haveRole($this->session->get('group'),42)) {

            } else {
                $this->view->pick(['error/403']);
            }
        }

    }
}

