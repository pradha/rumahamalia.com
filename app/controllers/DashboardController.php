<?php

class DashboardController extends \Phalcon\Mvc\Controller
{
    var $access_key;
    public function initialize()
    {
        $this->access_key=Options::findFirst(["name='access_key'"]);
        if (!$this->session->has('username')){
            $this->response->redirect($this->url->get('/index/'.$this->access_key->value.'/no-access'));
        }
        $this->view->options = Options::find(["order" => "created_at asc"]);
        $this->view->title="Dashboard Panel";
        $this->view->menu = "dashboard";
    }

    public function indexAction()
    {
        if ($this->request->isAjax())
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
    }

    public function pageInfoAction($page=null,$all){
        $this->view->disable();
        if ($page!=null){
            $data = file_get_contents(urldecode($page));
            $title = preg_match('/<title[^>]*>(.*?)<\/title>/ims', $data, $matches) ? $matches[1] : null;
            echo $title;
            echo $page.$all;
            echo "asas";
        } else {
            echo "unable to locate page, please check your target page";
        }
    }
}

