<?php

class SystemController extends \Phalcon\Mvc\Controller
{
    var $access_key;
    var $function;
    var $options;
    public function initialize()
    {
        $this->view->setLayout('dashboard');
        $this->access_key=Options::findFirst(["name='access_key'"]);
        if (!$this->session->has('username')){
            $this->response->redirect($this->url->get('/index/'.$this->access_key->value.'/no-access'));
        }
        $this->view->options = $this->options = Options::find(["order" => "created_at asc"]);
        $this->view->title="System Settings";
        $this->view->menu = "system";
        $this->function = new Functions();
    }
    public function indexAction()
    {

    }

    public function settingAction($target=null, $id=null, $status=null){
        if ($this->request->isAjax())
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $this->view->menu="setting";
        $this->view->title="System Settings";
        $this->view->target=$target;
        $this->view->id=$id;
        $this->view->status=$status;
        $error=new ArrayObject();
        if (haveRole($this->session->get('group'),16)) {

        } else {
            $this->view->pick(['error/403']);
        }

    }
    public function groupAction($target=null, $id=null, $status=null){
        if ($this->request->isAjax())
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $this->view->menu="group";
        $this->view->title="System Groups";
        $this->view->target=$target;
        $this->view->id=$id;
        $this->view->status=$status;
        $error=new ArrayObject();
        if ($target=="add"){
            if (haveRole($this->session->get('group'),13)) {
                $this->view->title = "Add Group";
                if ($this->request->isPost()) {
                    if (trim($this->request->getPost('name')) == "" || trim($this->request->getPost('description')) == "") {
                        if (trim($this->request->getPost('name')) == "") $error->name = true;
                        if (trim($this->request->getPost('description')) == "") $error->description = true;
                    } else {
                        $group = new Groups();
                        $group->name = trim($this->request->getPost('name'));
                        $group->description = trim($this->request->getPost('description'));
                        $group->created_by = $group->updated_by = $this->session->get('username');
                        if ($group->create()) {
                            $this->response->redirect('/system/group/edit/' . encode($group->id) . '/success');
                        } else $error->save = $group->getMessages();
                    }
                }
                $this->view->error = $error;
                $this->view->pick(['system/group-add']);
            } else {
                $this->view->pick(['error/403']);
            }
        } else if ($target=="edit"){
            if (haveRole($this->session->get('group'),14)) {
                $this->view->title = "Edit Group";
                if (is_numeric(decode($id))) {
                    $group = Groups::findFirst(["id=" . decode($id)]);
                    if (isset($group->id)) {
                        $this->view->group = $group;
                        if ($this->request->isPost()) {
                            if (trim($this->request->getPost('name')) == "" || trim($this->request->getPost('description')) == "") {
                                if (trim($this->request->getPost('name')) == "") $error->name = true;
                                if (trim($this->request->getPost('description')) == "") $error->description = true;
                            } else {
                                $group->name = trim($this->request->getPost('name'));
                                $group->description = trim($this->request->getPost('description'));
                                $group->updated_by = $this->session->get('username');
                                $group->updated_at = date('Y-m-d H:i:s');
                                if ($group->save()) {
                                    $this->response->redirect('/system/group/edit/' . encode($group->id) . '/success');
                                } else $error->save = $group->getMessages();
                            }
                        }
                        $this->view->pick(['system/group-edit']);
                    } else {
                        $this->view->pick(['error/404']);
                    }
                } else {
                    $this->view->pick(['error/404']);
                }
            } else {
                $this->view->pick(['error/403']);
            }
        } else if ($target=="role"){
            if (haveRole($this->session->get('group'),15)) {
                $this->view->title = "Group have Roles";
                $this->view->roles = Roles::find(["parent is null"]);
                $group=Groups::findFirst(["id='".decode($id)."'"]);
                if (isset($group->id)){
                    $this->view->group=$group;
                    if ($this->request->isPost()) {
                        $this->view->disable();
                        if ($this->request->getPost('group') == "" || $this->request->getPost('role') == "" || $this->request->getPost('status') == "") {
                            if ($this->request->getPost('group') == "") echo "Group ID required\n";
                            if ($this->request->getPost('role') == "") echo "role ID required\n";
                            if ($this->request->getPost('status') == "") echo "status ID required\n";
                        } else {
                            if ($this->request->getPost('status') == "true") {
                                if (!haveRole(decode($this->request->getPost('group')), $this->request->getPost('role'))) {
                                    $checkRole = new GroupHaveRole();
                                    $checkRole->group_id = decode($this->request->getPost('group'));
                                    $checkRole->role_id = $this->request->getPost('role');
                                    $checkRole->save();
                                }
                            } else {
                                $checkRole = GroupHaveRole::findFirst("group_id='" . decode($this->request->getPost('group')) . "' AND role_id='" . $this->request->getPost('role') . "'");
                                if ($checkRole) {
                                    if ($checkRole->delete() == false) {
                                        echo "cant delete";
                                    } else {
                                        echo 'deleted';
                                    }
                                }
                            }
                            echo "Group " . $this->request->getPost('group') . "\n";
                            echo "Role " . $this->request->getPost('role') . "\n";
                            echo "Status " . $this->request->getPost('status') . "\n";
                            echo "success";
                        }

                    }
                    $this->view->pick(['system/group-role']);
                }
            } else {
                $this->view->pick(['error/403']);
            }
        } else if ($target=="search"){
            if ($this->request->isPost() && trim($this->request->getPost('search'))!=""){
                $txt=$this->request->getPost('search');
                $this->view->search=strtolower($txt);
                $this->view->groups=Groups::find(["lower(name) like '%".$txt."%' OR lower(description) like '%".$txt."%'"]);
                $this->view->pick(['system/group-search']);
            } else {
                $this->view->pick(['error/404']);
            }
        } else {
            if (haveRole($this->session->get('group'),12)) {
                $data = Groups::find(["order" => "created_at"]);
                if (is_numeric($target))
                    $currentpage = $target;
                else
                    $currentpage = 1;
                $paginator = new Phalcon\Paginator\Adapter\Model(
                    array(
                        "data" => $data,
                        "limit" => 20,
                        "page" => $currentpage
                    )
                );
                $this->view->page = $page = $paginator->getPaginate();
            } else {
                $this->view->pick(['error/403']);
            }
        }
    }
    public function userAction($target=null, $id=null, $status=null){
        if ($this->request->isAjax())
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $this->view->menu="user";
        $this->view->title="System Users";
        $this->view->target=$target;
        $this->view->id=$id;
        $this->view->status=$status;
        $error=new ArrayObject();
        $this->view->genders=Gender::find();
        if ($target=="add"){
            if (haveRole($this->session->get('group'),9)) {
                $this->view->title = "Add User";
                if ($this->request->isPost()) {
                    $checkUser = Users::findFirst(["username='" . $this->request->getPost('username') . "'"]);
                    $checkMail = Users::findFirst(["email='" . $this->request->getPost('email') . "'"]);
                    if (isset($checkUser->username) || isset($checkMail->email) || $this->request->getPost('name') == "" || $this->request->getPost('phone') == "" || $this->request->getPost('gender') == "" || $this->request->getPost('date_of_birth') == "" || $this->request->getPost('biography') == "") {
                        if (isset($checkUser->username)) $error->username = true;
                        if (isset($checkMail->email)) $error->email = true;
                        if ($this->request->getPost('name') == "") $error->name = true;
                        if ($this->request->getPost('phone') == "") $error->phone = true;
                        if ($this->request->getPost('date_of_birth') == "") $error->date_of_birth = true;
                        if ($this->request->getPost('biography') == "") $error->biography = true;
                    } else {
                        $password = $this->function->genPass(8);
                        $user = new Users();
                        $user->group_id = 3;
                        $user->username = $this->request->getPost('username');
                        $user->password = $this->function->cryptPass($password);
                        $user->email = $this->request->getPost('email');
                        $user->name = $this->request->getPost('name');
                        $user->phone = $this->request->getPost('phone');
                        $user->gender = $this->request->getPost('gender');
                        $user->date_of_birth = $this->request->getPost('date_of_birth');
                        $user->biography = $this->request->getPost('biography');
                        $user->created_by = $user->updated_by = $this->session->get('username');
                        if ($user->create()) {
                            //send Mail
                            $access_key = Options::findFirst(["name='access_key'"]);
                            $message = "<div>";
                            $message .= '<img style="float: left;margin:20px 20px 20px 0;width:20%" src="' . $this->options[7]->value . '">';
                            $message .= "<p>Hi...<strong>" . $user->name . "</strong>, Selamat datang dan selamat bergabung di " . $this->url->get() . "</p>";
                            $message .= "<p>Akun anda telah ditambahkan kedalam sistem " . $this->url->get() . "</p>";
                            $message .= "<p>Berikut adalah informasi akun anda :</p>";
                            $message .= "<blockquote>- Username : <strong>" . $user->username . "</strong><br>";
                            $message .= "- Password : <strong>" . $password . "</strong></blockquote>";
                            $message .= "<p>Akun anda dapat diakses dari <a href='" . $this->url->get('/index/') . $access_key->value . "'>sini</a>, atau <a href='" . $this->url->get('/index/') . $access_key->value . "'>" . $this->url->get('/index/') . $access_key->value . "</a> </p>";
                            $message .= "<p>Email ini adalah informasi akun anda dengan password yang di generate secara acak oleh sistem (tidak ada seorang pun yang tahu password anda), silahkan ubah password untuk memudahkan mengingat password akun anda secepatnya</p>";
                            $message .= "</div>";
                            $this->function->sendMail("adit@pradha.id", $this->options[0]->value, $user->email, $user->name, "Pendaftaran Akun", $message);
                            $this->response->redirect('/system/user/edit/' . encode($user->username) . '/success');
                        } else {
                            $error->save = $user->getMessages();
                        }
                    }
                }
                $this->view->error = $error;
                $this->view->pick(['system/user-add']);
            } else {
                $this->view->pick(['error/403']);
            }
        } else if ($target=="edit"){
            if (haveRole($this->session->get('group'),10)) {
                $this->view->title = "Edit User";
                if ($id !== null) {
                    $user = Users::findFirst(["username='" . decode($id) . "'"]);
                    if (isset($user->username)) {
                        $this->view->user = $user;
                        if ($this->request->isPost()) {
                            $checkMail = Users::findFirst(["username!='" . $user->username . "' AND email='" . $this->request->getPost('email') . "'"]);
                            if (isset($checkMail->email) || $this->request->getPost('name') == "" || $this->request->getPost('phone') == "" || $this->request->getPost('gender') == "" || $this->request->getPost('date_of_birth') == "" || $this->request->getPost('biography') == "") {
                                if (isset($checkMail->email)) $error->email = true;
                                if ($this->request->getPost('name') == "") $error->name = true;
                                if ($this->request->getPost('phone') == "") $error->phone = true;
                                if ($this->request->getPost('date_of_birth') == "") $error->date_of_birth = true;
                                if ($this->request->getPost('biography') == "") $error->biography = true;
                            } else {
                                $user->email = $this->request->getPost('email');
                                $user->name = $this->request->getPost('name');
                                $user->phone = $this->request->getPost('phone');
                                $user->gender = $this->request->getPost('gender');
                                $user->date_of_birth = $this->request->getPost('date_of_birth');
                                $user->biography = $this->request->getPost('biography');
                                $user->updated_by = $this->session->get('username');
                                $user->updated_at = date('Y-m-d H:i:s');
                                if ($user->save()) {
                                    $this->response->redirect('/system/user/edit/' . encode($user->username) . '/success');
                                } else {
                                    $error->save = $user->getMessages();
                                }
                            }
                        }
                        $this->view->error = $error;
                        $this->view->pick(['system/user-edit']);
                    } else {
                        $this->view->pick(['error/404']);
                    }
                } else {
                    $this->view->pick(['error/404']);
                }
            } else {
                $this->view->pick(['error/403']);
            }
        } else if ($target=="search"){
            if ($this->request->isPost() && trim($this->request->getPost('search'))!=""){
                $txt=$this->request->getPost('search');
                $this->view->search=strtolower($txt);
                $this->view->users=Users::find(["lower(username) like '%".$txt."%' OR lower(name) like '%".$txt."%' OR lower(email) like '%".$txt."%' OR lower(biography) like '%".$txt."%'"]);
                $this->view->pick(['system/user-search']);
            } else {
                $this->view->pick(['error/404']);
            }
        } else {
            if (haveRole($this->session->get('group'),8)) {
                $data = VUsers::find(["order" => "created_at"]);
                if (is_numeric($target))
                    $currentpage = $target;
                else
                    $currentpage = 1;
                $paginator = new Phalcon\Paginator\Adapter\Model(
                    array(
                        "data" => $data,
                        "limit" => 15,
                        "page" => $currentpage
                    )
                );
                $this->view->page = $page = $paginator->getPaginate();
            } else {
                $this->view->pick(['error/403']);
            }
        }
    }


}

