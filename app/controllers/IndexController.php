<?php

class IndexController extends ControllerBase
{
    public function initialize(){
    }
    public function indexAction($param=null, $target=null)
    {
        $this->view->sliders=Sliders::find(["status=1"]);
        $this->view->pages=Pages::find(["parent is null AND status=8"]);
        $this->view->posts=Posts::find(["conditions"=>"status=8","order"=>"published_at desc","limit"=>4]);
        $access_key=Options::findFirst(["name='access_key'"]);
        if ($param==$access_key->value){
            $token=md5(uniqid().date('Ymdhis').$this->request->getClientAddress());
            $this->session->set("token",$token);
            $this->response->redirect($this->url->get('/login/'.$token.'/'.$target));
        }

    }

}

