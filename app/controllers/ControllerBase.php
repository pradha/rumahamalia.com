<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    var $options=null;
    public function initialize(){
        $this->options=Options::find(["order"=>"created_at asc"]);
        $this->view->option=$this->options;
        $this->view->title=$this->options[0]->value;
        $this->view->description=$this->options[2]->value;
        $this->view->keywords=$this->options[3]->value;
        $this->view->author=$this->options[4]->value;
        $this->view->designer=$this->options[12]->value;
        $this->view->copyright=$this->options[9]->value;
        $this->view->language=$this->options[10]->value;
        $this->view->robot=$this->options[6]->value;
        $this->view->coverage=$this->options[13]->value;
        $this->view->distribution=$this->options[14]->value;
        $this->view->target=$this->options[15]->value;
        $this->view->owner=$this->options[16]->value;
        $this->view->publisher=$this->options[17]->value;
        $this->view->image=$this->options[7]->value;
        $this->view->locale=$this->options[8]->value;
        $this->view->og_type=$this->options[18]->value;
        $this->view->twitter_card=$this->options[19]->value;
        $this->view->twitter_site=$this->options[20]->value;
        $this->view->twitter_creator=$this->options[21]->value;
    }
}
