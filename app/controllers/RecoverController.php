<?php

class RecoverController extends \Phalcon\Mvc\Controller
{
    var $access_key;
    var $function;
    var $options;
    public function initialize()
    {
        $this->access_key=Options::findFirst(["name='access_key'"]);
        if ($this->session->has('username')){
            $this->response->redirect($this->url->get('/dashboard'));
        }
        $this->view->options = $this->options = Options::find(["order" => "created_at asc"]);
        $this->view->title="Recover";
        $this->view->menu = "recover";
        $this->function = new Functions();
    }
    public function indexAction()
    {
        if ($this->request->isAjax())
            $this->view->disable();
        $this->access_key=Options::findFirst(["name='access_key'"]);
        $this->view->key=$this->access_key->value;
        if ($this->request->isPost()){
            $user=Users::findFirst("username='".$this->request->getPost('usermail')."' OR email='".$this->request->getPost('usermail')."'");
            if (isset($user->username)){
                $user->password_verify=md5(uniqid("recover_").date('YmdHis'));
                if ($user->save()){
                    $message = "<div>";
                    $message .= '<img style="float: left;margin:20px 20px 20px 0;width:20%" src="' . $this->options[7]->value . '">';
                    $message .= "<p>Hi...<strong>" . $user->name . "</strong>, Anda telah melakukan request untuk me-<em>reset</em> password anda di " . $this->url->get() . "</p>";
                    $message .= "<p>Klik tautan dibawah ini, anda akan diarahkan menuju password anda</p>";
                    $message .= '<blockquote><a href="'.$this->url->get('/recover/code/'.$user->password_verify).'">'.$this->url->get('/recover/code/'.$user->password_verify).'</a></blockquote><br>';
                    $message .= "<p>Jika email ini di anggap kesalahan, silahkan laporkan melalui akun anda</p>";
                    $message .= "</div>";
                    $this->function->sendMail("adit@pradha.id", $this->options[0]->value, $user->email, $user->name, "Reset Password", $message);
                    echo 'success';
                } else {
                    echo 'Oops...!Something error';
                }
            } else {
                echo "Username atau E-Mail tidak terdaftar";
            }
        }
    }
    public function codeAction($hash=null){
        if ($this->request->isAjax())
            $this->view->disable();
        $error=new ArrayObject();
        $function=new Functions();
        $this->view->hash=$hash;
        if ($hash!=null){
            $user=Users::findFirst(["password_verify='".$hash."'"]);
            if (isset($user->username)){
                if ($this->request->isPost()){
                    if (strlen($this->request->getPost('new'))<=6 || $this->request->getPost('new')!=$this->request->getPost('renew')){
                        echo 'Please check the following :';
                        echo '<ul>';
                        if (strlen($this->request->getPost('new'))<=6) echo '<li>Password harus lebih dari 6 karakter</li>';
                        if ($this->request->getPost('new')!=$this->request->getPost('renew')) echo '<li>Ulangi password salah</li>';
                        echo '</ul>';
                        $this->view->error=$error;
                    } else {
                        $user->password=$function->cryptPass($this->request->getPost('new'));
                        $user->password_verify=null;
                        $user->updated_by=$user->username;
                        $user->updated_at=date('Y-m-d');
                        if ($user->save()){
                            echo 'success '.Options::findFirst(["name='access_key'"])->value;
                        }
                    }
                    $this->view->error=$error;
                }
            } else {
                $this->view->pick(['error/404']);
            }
        } else {
            $this->view->pick(['error/404']);
        }
    }

}

