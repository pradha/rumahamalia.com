<?php

class LoginController extends \Phalcon\Mvc\Controller
{

    public function initialize()
    {
        if ($this->session->has('username')){
            $this->response->redirect($this->url->get('/dashboard'));
        }
        $this->view->options = Options::find(["order" => "created_at asc"]);
        $this->view->title="Login Area";
    }
    public function indexAction($param=null, $target=null)
    {
        if ($this->request->isAjax()) {
            $this->view->disable();
        }
        if ($param!=null && $param==$this->session->get('token')){
            $this->view->param=$param;
            $this->view->status=0;
            if ($this->request->isPost()){
                $user=Users::findFirst("username='".$this->request->getPost('usermail')."' OR email='".$this->request->getPost('usermail')."'");
                if (isset($user->username) && $user->status && haveRole($user->group_id,2)){
                    if ($user->is_logged_in && round(abs(time()-strtotime($user->last_logged_in))/60,2)<=10){
                        if ($this->request->isAjax())
                            echo 'user is still logged in';
                        else
                            $this->view->status=3;
                    } else if (password_verify($this->request->getPost('password'),$user->password)){
                        $user->is_logged_in=true;
                        $user->last_logged_in=date('Y-m-d H:i:s');
                        $user->last_logged_in_from=$this->request->getClientAddress();
                        $user->kicked_out=false;
                        $user->save();
                        $this->session->set('username', $user->username);
                        $this->session->set('name', $user->name);
                        $this->session->set('email', $user->email);
                        $this->session->set('photo', $user->photo);
                        $this->session->set('group', $user->group_id);
                        $this->session->set('lang', $user->language);
                        $this->session->set('theme', $user->theme);
                        $this->session->set('last_activity', time());
                        if ($this->request->isAjax())
                            echo 'success';
                        else
                            $this->response->redirect('dashboard');
                    } else {
                        if ($this->request->isAjax())
                            echo 'invalid username or password';
                        else
                            $this->view->status=4;
                    }
                } else {
                    if ($this->request->isAjax())
                        echo 'invalid account or no permission';
                    else
                        $this->view->status=2;
                }
            }
        } else {
            $this->view->disable();
            $this->response->setStatusCode(403, "Ilegal access");
        }

    }

}

