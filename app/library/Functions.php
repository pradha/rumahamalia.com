<?php
/**
 * Created by PhpStorm.
 * User: adit
 * Date: 2/14/2018
 * Time: 8:24 AM
 */
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Functions
{
    var $access_key;
    var $function;
    var $options;

    function cryptPass($password){
        $password_hash=password_hash($password, PASSWORD_BCRYPT, array('cost' => 10));
        return $password_hash;
    }
    function createUUID(){
        $random = new \Phalcon\Security\Random();
        return $random->uuid();
    }
    function isUUID($uuid){
        if (!is_string($uuid) || (preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', $uuid) !== 1)) {
            return false;
        } else return true;
    }
    function createFolder($path="./files/"){
        $year = $path . date('Y');
        $month = $path . date('Y') . '/' . date('m');
        if (file_exists($year)) {
            if (!file_exists($month)) {
                mkdir($month, 0777);
            }
        } else {
            mkdir($year, 0777);
            mkdir($month, 0777);
        }
        return $month."/";
    }
    function genPass($length = 20){
        $chars =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.
            '0123456789`-=~!@#$%^&*()_+,./<>?;:[]{}\|';

        $str = '';
        $max = strlen($chars) - 1;

        for ($i=0; $i < $length; $i++)
            $str .= $chars[random_int(0, $max)];

        return $str;
    }
    function slugify($type,$text,$ignore=null)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        if ($type=="category"){
            if ($ignore!=null)
                $exist=Categories::findFirst(["conditions"=>"slug like'".$text."%' AND id!='".$ignore."'","order"=>"slug desc"]);
            else
                $exist=Categories::findFirst(["conditions"=>"slug like '".$text."%'","order"=>"slug desc"]);
        } else if ($type=="page"){
            if ($ignore!=null)
                $exist=Pages::findFirst(["conditions"=>"slug like'".$text."%' AND id!='".$ignore."'","order"=>"slug desc"]);
            else
                $exist=Pages::findFirst(["conditions"=>"slug like '".$text."%'","order"=>"slug desc"]);
        }
        if (isset($exist->id)){
            if(strpos($exist->slug, '-') !== false) {
                $data = explode("-",$exist->slug);
                $lastNum=end($data);
                if (is_numeric(end($data))){
                    $num=end($data)+1;
                } else {
                    $num=2;
                }
            } else $num=2;
            $text.="-".$num;
        }
        return trim($text,"-");
    }
    function sendMail($from, $fromName, $to, $toName, $subject, $messages){
        $this->options=Options::find(["order"=>"created_at asc"]);
        require_once 'PHPMailer/PHPMailer.php';
        require_once 'PHPMailer/SMTP.php';
        require_once 'PHPMailer/Exception.php';
        $mail = new PHPMailer(true);
        try {
            //Server settings
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = $this->options[23]->value;            //'pradha.id';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = $this->options[24]->value;            //'adit@pradha.id';                 // SMTP username
            $mail->Password = $this->options[25]->value;        //'bismillah';                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom($from, $fromName);
            $mail->addAddress($to, $toName);

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body    = $messages;
            $mail->AltBody = strip_tags($messages);

            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            $mail->send();
        } catch (Exception $e) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }
    }
}