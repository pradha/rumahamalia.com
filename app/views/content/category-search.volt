<div class="mg-b-10">
    Ditemukan sebanyak : <strong>{{ categories|length }}</strong> data, Dari hasil pencarian : "<strong>{{ search }}</strong>"  <a title="hapus pencarian" class="btn btn-warning rounded-10 pd-x-10 pd-y-1" href="{{ url('/content/category') }}"><i class="icon fa fa-close"></i> </a>
</div>
<table class="table table-hover table-bordered table-responsive">
    <thead class="thead-colored thead-teal">
    <tr>
        <th class="text-light">#</th>
        <th class="text-light">Name</th>
        <th class="text-light">Description</th>
        <th class="text-light">Keywords</th>
        <th class="text-light">Slug</th>
        <th class="text-light">Created</th>
        <th class="text-light">Updated</th>
    </tr>
    </thead>
    <tbody class="bg-lightsky">
    {% for data in categories %}
        <tr>
            <td>
                <a title="edit" class="btn btn-teal pd-y-2 pg-x-3" href="{{ url('/content/category/edit/') }}{{ data.id }}"><i class="icon ion-edit"></i> </a>
            </td>
            <td>
                {% if data.parent!="" %}
                    {{ data.parent_name }} &gt;
                {% endif %}
                {{ data.name }}
            </td>
            <td>{{ data.description }}</td>
            <td>{{ data.keywords|tags }}</td>
            <td><a href="{{ url('/category/') }}{{ data.slug }}" target="_blank">{{ data.slug }}</a> </td>
            <td>{{ data.created_at|dateTimeFormat }} / {{ data.created_by }}</td>
            <td>{{ data.updated_at|dateTimeFormat }} / {{ data.updated_by }}</td>
        </tr>
    {% endfor %}
    </tbody>
</table>