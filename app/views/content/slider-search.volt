<div class="mg-b-10">
    Ditemukan sebanyak : <strong>{{ sliders|length }}</strong> data, Dari hasil pencarian : "<strong>{{ search }}</strong>"  <a title="hapus pencarian" class="btn btn-warning rounded-10 pd-x-10 pd-y-1" href="{{ url('/content/slider') }}"><i class="icon fa fa-close"></i> </a>
</div>
<table class="table table-hover table-bordered">
    <thead class="thead-colored thead-teal">
    <tr>
        <th class="text-light">#</th>
        <th class="text-light wd-20p-force">Slider</th>
        <th class="text-light">Title</th>
        <th class="text-light">Description</th>
        <th class="text-light">Target</th>
        <th class="text-light">Placement</th>
        <th class="text-light">Status</th>
        <th class="text-light">Created by</th>
        <th class="text-light">Created at</th>
    </tr>
    </thead>
    <tbody class="bg-lightsky">
    {% for data in sliders %}
        <tr>
            <td>
                <a title="edit" class="btn btn-teal pd-y-2 pg-x-3" href="{{ url('/content/slider/edit/') }}{{ data.id }}"><i class="icon ion-edit"></i> </a>
                <a title="delete" class="btn btn-teal pd-y-2 pg-x-3" href="{{ url('/content/slider/delete/') }}{{ data.id }}"><i class="icon ion-trash-a"></i> </a>
            </td>
            <td><img src="{{ data.image }}" class="img img-fluid"></td>
            <td>{{ data.title }}</td>
            <td>{{ data.description }}</td>
            <td><a target="_blank" href="{{ data.url }}"><i class="icon fa fa-external-link"></i> </a> </td>
            <td>{{ data.placement }} </td>
            <td>{{ data.status }} </td>
            <td>{{ data.created_at|dateTimeFormat }} / {{ data.created_by }}</td>
            <td>{{ data.updated_at|dateTimeFormat }} / {{ data.updated_by }}</td>
        </tr>
    {% endfor %}
    </tbody>
</table>