<div class="mg-b-10">
    Ditemukan sebanyak : <strong>{{ pages|length }}</strong> data, Dari hasil pencarian : "<strong>{{ search }}</strong>"  <a title="hapus pencarian" class="btn btn-warning rounded-10 pd-x-10 pd-y-1" href="{{ url('/content/page') }}"><i class="icon fa fa-close"></i> </a>
</div>

<table class="table table-hover  table-bordered table-responsive">
    <thead class="thead-colored thead-teal">
    <tr>
        <th class="text-light">#</th>
        <th class="text-light">Title</th>
        <th class="text-light wd-30p-force">Description</th>
        <th class="text-light">Keywords</th>
        <th class="text-light">Slug</th>
        <th class="text-light">Status</th>
        <th class="text-light">Created</th>
        <th class="text-light">Updated</th>
    </tr>
    </thead>
    <tbody class="bg-lightsky">
    {% for data in pages %}
        <tr>
            <td>
                <a title="edit" class="btn btn-teal pd-y-2 pg-x-3" href="{{ url('/content/page/edit/') }}{{ data.id }}"><i class="icon ion-edit"></i> </a>
            </td>
            <td>
                {% if data.parent!="" %}
                    {{ data.parent_title }} &gt;
                {% endif %}
                {{ data.title }}
            </td>
            <td>{{ data.description }}</td>
            <td>
                {{ data.keywords|tags }}

            </td>
            <td><a target="_blank" href="{{ url('/page/') }}{{ data.slug }}">{{ data.slug }}</a> </td>
            <td>
                {% if data.status==3 OR data.status==7 %}
                    <span class="badge badge-pill badge-danger tx-white mg-x-1 pd-5"><i class="icon fa fa-edit"></i> {{ data.status_name }}</span>
                {% elseif data.status==8 %}
                    <span class="badge badge-pill badge-success tx-white mg-x-1 pd-5"><i class="icon fa fa-check"></i> {{ data.status_name }}</span>
                {% endif %}
            </td>
            <td>{{ data.created_at|dateTimeFormat }} / {{ data.created_by }}</td>
            <td>{{ data.updated_at|dateTimeFormat }} / {{ data.updated_by }}</td>
        </tr>
    {% endfor %}
    </tbody>
</table>