<link href="{{ url('/lib/summernote/summernote-bs4.css') }}" rel="stylesheet">
<link href="{{ url('/lib/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">

<script src="{{ url('/lib/parsleyjs/parsley.js') }}"></script>
<script src="{{ url('/lib/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>


<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{ url('/dashboard') }}">Main Dashboard</a>
        <span class="breadcrumb-item">Contents</span>
        <a class="breadcrumb-item" href="{{ url('/content/page') }}">Page</a>
        <a class="breadcrumb-item active" href="{{ url('/content/page/add') }}">Add</a>
    </nav>
</div><!-- br-pageheader -->
<div class="row no-gutters">
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
            <h4 class="tx-gray-800 mg-b-5">
                Tambah Halaman
                <a href="{{ url('/content/page/') }}" class="btn btn-teal btn-with-icon">
                    <div class="ht-25">
                        <span class="icon wd-25"><i class="fa fa-list"></i></span>
                        <span class="pd-x-15">Daftar Halaman</span>
                    </div>
                </a>
            </h4>
            <p class="mg-b-0">Tambah Halaman</p>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30 text-right">

        </div>
    </div>
</div>

<hr>
<div class="br-pagebody pd-sm-x-30">
    {% if error.title is defined OR error.content is defined  OR error.description is defined  OR error.keywords is defined %}
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong class="d-block d-sm-inline-block-force">Oops!</strong> Silahkan periksa kembali kesalahan berikut :
            <ul class="mg-b-0">
                {% if error.title is defined %}<li>Judul halaman masih kosong</li>{% endif %}
                {% if error.content is defined %}<li>Isi halaman masih kosong</li>{% endif %}
                {% if error.description is defined %}<li>Deskripsi halaman masih kosong</li>{% endif %}
                {% if error.keywords is defined %}<li>Tambahkan beberapa tags/keywords untuk memudahkan pencarian</li>{% endif %}
                {% if error.save is defined %}
                    {% for err in error.save %}
                        <li>Something error in database</li>
                    {% endfor %}
                {% endif %}
            </ul>
        </div>
    {% endif %}
    <form method="post" action="{{ url('/content/page/add') }}" class="form-layout form-layout-1 bg-white" data-parsley-validate>
        <div class="row">
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="form-control-label">Page Title: <span class="tx-danger">*</span></label>
                            <input class="form-control" type="text" name="title" value="{% if request.isPost() %}{{ request.getPost('title') }}{% else %}{% endif %}" placeholder="Enter page title..." required autocomplete="off" maxlength="150">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group mg-b-10-force">
                            <label class="form-control-label">Page Content: <span class="tx-danger">*</span></label>
                            <textarea id="summernote" class="form-control" name="content" placeholder="Page Content" required autocomplete="off">{% if request.isPost() %}{{ request.getPost('content') }}{% else %}{% endif %}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label class="form-control-label">Page Parent:</label>
                    <select class="form-control" name="parent" data-placeholder="Choose parent" tabindex="-1" aria-hidden="true">
                        <option label=".: no parent :."></option>
                        {% for parent in parents %}
                            <option value="{{ parent.id }}" label="{{ parent.title }}" {% if request.isPost() AND request.getPost('parent')==parent.id %}selected{% endif %}>{{ parent.title }}</option>
                        {% endfor %}
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Page Description: <span class="tx-danger">*</span></label>
                    <textarea rows="5" maxlength="250" class="form-control" name="description" placeholder="Page Description" required autocomplete="off">{% if request.isPost() %}{{ request.getPost('description') }}{% else %}{% endif %}</textarea>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Page Slug:</label>
                    <div class="input-group">
                        <span class="input-group-addon tx-size-sm lh-2">{{ url('/page/') }}</span>
                        <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Keywords: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="keywords" value="{% if request.isPost() %}{{ request.getPost('keywords') }}{% else %}{% endif %}" placeholder="Keywords or Tags" data-role="tagsinput" required>
                </div>
            </div>
        </div>


        <div class="form-layout-footer">
            <button type="submit" class="btn btn-teal">Create</button>
        </div>
    </form>
</div>

<div id="gallery" class="modal fade">
    <div class="modal-dialog modal-v-lg" role="document">
        <div class="modal-content tx-size-sm">
            <div class="modal-header pd-x-20">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Media Gallery | <button id="upload" class="btn btn-teal pd-x-8 pd-y-3" href="#upload"><small><i class="icon fa fa-upload"></i> Upload</small></button> </h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div id="data" class="modal-body pd-20 text-center">

            </div>
        </div>
    </div>
</div>

<script src="{{ url('/lib/summernote/summernote-bs4.min.js') }}"></script>
<script>
    function loadMedia(){
        $('#data').html("Silahkan tunggu...");
        $.get("{{ url('/content/media/data') }}", function(data, status){
            $('#data').html(data);
        });
    }

    $(document).ready(function(){
        $('#upload').on('click',function(){
            $.get("{{ url('/content/media/insertUpload') }}", function(data, status){
                $('#data').html(data);
            });
        });
        var galleryButton = function (context) {
            var ui = $.summernote.ui;

            var button = ui.button({
                contents: '<i class="fa fa-image"/>',
                tooltip: 'Gallery',
                click: function () {
                    $('#gallery').modal('show');
                    loadMedia();
                    /*context.invoke('editor.insertText', 'hello');*/
                }
            });

            return button.render();
        };
        $('#summernote').summernote({
            minHeight:($(window).height() - 550),
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline']],
                ['font', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link','gallery','video']],
                ['height', ['height']],
                ['misc', ['fullscreen','codeview']]
            ],

            buttons: {
                gallery: galleryButton
            }
        });

        $("form").submit(function(e) {
            e.preventDefault();
            $form = $(this);
            $('#overlay').toggle();
            $.post(
                $form.attr('action'),
                $(this).serialize(),
                function(result){
                    $("#main").html(result);
                    $('#overlay').toggle();
                })
                .fail(function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                });

        });
    });
</script>
