<div class="row vdivide">
    <div class="col-lg-9 text-center media-insert">
        {% if media.content_type=="image/png" OR media.content_type=="image/jpg" OR media.content_type=="image/jpeg" OR media.content_type=="image/gif" %}
            <img class="img img-fluid" src="{{ url('/files/upload/') }}{{ media.created_at|year }}/{{ media.created_at|month }}/{{ media.original_file }}">
        {% elseif media.content_type=="application/pdf"  %}
            <img class="img img-fluid" src="{{ url('/img/pdf.png') }}">
        {% elseif media.content_type=="text/plain"  %}
            <img class="img img-fluid" src="{{ url('/img/text.png') }}">
        {% else  %}
            <img class="img img-fluid" src="{{ url('/img/file.png') }}">
        {% endif %}
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <label class="form-control-label">Media Name: <span class="tx-danger">*</span></label>
            <input type="text" name="media-name" class="form-control pd-y-12" placeholder="File Name" value="{{ media.name }}" autocomplete="off" required="">
        </div>
        <div class="form-group">
            <label class="form-control-label">Description: <span class="tx-danger">*</span></label>
            <textarea class="form-control" name="media-description">{{ media.description }}</textarea>
            <input type="hidden" value=''>
        </div>
        <div class="form-layout-footer">
            <button id="insertMedia" class="btn btn-teal">Insert</button>
            <button id="backToGallery" class="btn btn-teal">Back to gallery</button>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#insertMedia').on('click',function(){
            var element='<a href="{{ url('/media/preview/') }}{{ media.id }}/{{ media.slug }}">'+
                $('.media-insert').html()
                +'</a>';
            $('#summernote').summernote('editor.saveRange');
            $('#summernote').summernote('editor.restoreRange');
            $('#summernote').summernote('editor.focus');
            $('#summernote').summernote('pasteHTML',element );
            $('#gallery').modal('hide');
        });
        $('#backToGallery').on('click',function(){
            loadMedia();
        });
    });
</script>