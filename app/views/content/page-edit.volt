<link href="{{ url('/lib/summernote/summernote-bs4.css') }}" rel="stylesheet">
<link href="{{ url('/lib/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">

<script src="{{ url('/lib/parsleyjs/parsley.js') }}"></script>
<script src="{{ url('/lib/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>


<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{ url('/dashboard') }}">Main Dashboard</a>
        <span class="breadcrumb-item">Contents</span>
        <a class="breadcrumb-item" href="{{ url('/content/page') }}">Page</a>
        <a class="breadcrumb-item active" href="{{ url('/content/page/edit/') }}{{ id }}">Edit</a>
    </nav>
</div><!-- br-pageheader -->
{% if (page.status==3 OR page.status==7) AND haveRole(session.get('group'),21) %}
    <div id="publish" class="modal fade">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content tx-size-sm">
                <div class="modal-header pd-x-20">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Publish Halaman</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <p class="mg-b-5">Pastikan halaman telah disimpan sebelum anda publish. <br>Apakah anda yakin akan mem-publikasikan halaman ini?</p>
                    <form id="publishForm" method="post" action="{{ url('/content/page/publish') }}">
                        <input type="hidden" name="id" value="{{ page.id }}">
                    </form>
                </div><!-- modal-body -->
                <div class="modal-footer">
                    <button type="submit" form="publishForm" class="btn btn-success submit tx-size-xs">Ya</button>
                    <button type="button" class="btn btn-secondary tx-size-xs" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
{% endif %}

<div class="row no-gutters">
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
            <h4 class="tx-gray-800 mg-b-5">
                Edit Halaman
                <a href="{{ url('/content/page/') }}" class="btn btn-teal btn-with-icon">
                    <div class="ht-25">
                        <span class="icon wd-25"><i class="fa fa-list"></i></span>
                        <span class="pd-x-15">Daftar Halaman</span>
                    </div>
                </a>
                {% if (page.status==3 OR page.status==7) AND haveRole(session.get('group'),21) %}
                    <button  class="btn btn-success btn-with-icon" data-toggle="modal" data-target="#publish">
                        <div class="ht-25">
                            <span class="icon wd-25"><i class="fa fa-send"></i></span>
                            <span class="pd-x-15">Publish</span>
                        </div>
                    </button>
                {% endif %}
            </h4>
            <p class="mg-b-0">Edit Halaman</p>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30 text-right">

        </div>
    </div>
</div>

<hr>
<div class="br-pagebody pd-sm-x-30">
    {% if status is defined %}
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {% if status=="success" %}
                <strong class="d-block d-sm-inline-block-force">Success!</strong> Halaman telah berhasil disimpan.
            {% elseif status=="published" %}
                <strong class="d-block d-sm-inline-block-force">Success!</strong> Halaman telah berhasil di publikasikan.
            {% endif %}
        </div><!-- alert -->
    {% elseif error.title is defined OR error.content is defined  OR error.description is defined  OR error.keywords is defined %}
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong class="d-block d-sm-inline-block-force">Oops!</strong> Silahkan periksa kembali kesalahan berikut :
            <ul class="mg-b-0">
                {% if error.title is defined %}<li>Judul halaman masih kosong</li>{% endif %}
                {% if error.content is defined %}<li>Isi halaman masih kosong</li>{% endif %}
                {% if error.description is defined %}<li>Deskripsi halaman masih kosong</li>{% endif %}
                {% if error.keywords is defined %}<li>Tambahkan beberapa tags/keywords untuk memudahkan pencarian</li>{% endif %}
                {% if error.save is defined %}
                    {% for err in error.save %}
                        <li>Something error in database</li>
                    {% endfor %}
                {% endif %}
            </ul>
        </div>
    {% endif %}
    <form method="post" action="{{ url('/content/page/edit/') }}{{ id }}" class="form-layout form-layout-1 bg-white" data-parsley-validate>
        <div class="row">
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="form-control-label">Page Title: <span class="tx-danger">*</span></label>
                            <input class="form-control" type="text" name="title" value="{% if request.isPost() %}{{ request.getPost('title') }}{% else %}{{ page.title }}{% endif %}" placeholder="Enter page title..." required autocomplete="off" maxlength="150">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group mg-b-10-force">
                            <label class="form-control-label">Page Content: <span class="tx-danger">*</span></label>
                            <textarea id="summernote" class="form-control" name="content" placeholder="Page Content" required autocomplete="off">{% if request.isPost() %}{{ request.getPost('content') }}{% else %}{{ page.content }}{% endif %}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label class="form-control-label">Page Parent:</label>
                    <select class="form-control" name="parent" data-placeholder="Choose parent" tabindex="-1" aria-hidden="true">
                        <option label=".: no parent :."></option>
                        {% for parent in parents %}
                            {% if page.id!=parent.id %}
                                <option value="{{ parent.id }}" label="{{ parent.title }}" {% if request.isPost() AND request.getPost('parent')==parent.id %}selected{% elseif parent.id==page.parent %}selected{% endif %}>{{ parent.name }}</option>
                            {% endif %}
                        {% endfor %}
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Page Description: <span class="tx-danger">*</span></label>
                    <textarea rows="5" maxlength="250" class="form-control" name="description" placeholder="Page Description" required autocomplete="off">{% if request.isPost() %}{{ request.getPost('description') }}{% else %}{{ page.description }}{% endif %}</textarea>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Page Slug:</label>
                    <div class="input-group">
                        <span class="input-group-addon tx-size-sm lh-2">{{ url('/page/') }}</span>
                        <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" readonly value="{{ page.slug }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Keywords: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="keywords" value="{% if request.isPost() %}{{ request.getPost('keywords') }}{% else %}{{ page.keywords }}{% endif %}" placeholder="Keywords or Tags" data-role="tagsinput" required>
                </div>
            </div>
        </div>


        <div class="form-layout-footer">
            <button type="submit" class="btn btn-teal">Simpan</button>
        </div>
    </form>
</div>

<div id="gallery" class="modal fade">
    <div class="modal-dialog modal-v-lg" role="document">
        <div class="modal-content tx-size-sm">
            <div class="modal-header pd-x-20">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Media Gallery | <button id="upload" class="btn btn-teal pd-x-8 pd-y-3" href="#upload"><small><i class="icon fa fa-upload"></i> Upload</small></button> </h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div id="data" class="modal-body pd-20 text-center">

            </div>
        </div>
    </div>
</div>

<script src="{{ url('/lib/summernote/summernote-bs4.min.js') }}"></script>
<script>
    function loadMedia(){
        $('#data').html("Silahkan tunggu...");
        $.get("{{ url('/content/media/data') }}", function(data, status){
            $('#data').html(data);
        });
    }

    $(document).ready(function(){
        $('#upload').on('click',function(){
            $.get("{{ url('/content/media/insertUpload') }}", function(data, status){
                $('#data').html(data);
            });
        });
        var galleryButton = function (context) {
            var ui = $.summernote.ui;

            var button = ui.button({
                contents: '<i class="fa fa-image"/>',
                tooltip: 'Gallery',
                click: function () {
                    $('#gallery').modal('show');
                    loadMedia();
                    /*context.invoke('editor.insertText', 'hello');*/
                }
            });

            return button.render();
        };
        $('#summernote').summernote({
            dialogsInBody: true,
            minHeight:($(window).height() - 550),
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline']],
                ['font', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link','gallery','video']],
                ['height', ['height']],
                ['misc', ['fullscreen','codeview']]
            ],

            buttons: {
                gallery: galleryButton
            }
        });

        $("form").submit(function(e) {
            e.preventDefault();
            $('.modal').modal('hide');
            $form = $(this);
            $('#overlay').toggle();
            $.post(
                $form.attr('action'),
                $(this).serialize(),
                function(result){
                    $("#main").html(result);
                    $('#overlay').toggle();
                })
                .fail(function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                });

        });
    });
</script>
