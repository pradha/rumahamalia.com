<div class="row">
    {% for data in page.items %}
    <div class="col-lg-1 col-md-2 col-xs-12 col-sm-3 mg-b-5-force">
        <div class="crop" data-content="{{ data.id }}">
            {% if data.content_type=="image/png" OR data.content_type=="image/jpg" OR data.content_type=="image/jpeg" OR data.content_type=="image/gif" %}
                <img class="img img-fluid" src="{{ url('/files/upload/') }}{{ data.created_at|year }}/{{ data.created_at|month }}/{{ data.original_file }}">
            {% elseif data.content_type=="application/pdf"  %}
                <img class="img img-fluid" src="{{ url('/img/pdf.png') }}">
            {% elseif data.content_type=="text/plain"  %}
                <img class="img img-fluid" src="{{ url('/img/text.png') }}">
            {% else  %}
                <img class="img img-fluid" src="{{ url('/img/file.png') }}">
            {% endif %}
        </div>
    </div>
    {% endfor %}
</div>

<script>
    $('.crop').on('click',function(){
        $.get("{{ url('/content/media/insert/') }}"+$(this).attr('data-content'), function(data, status){
            $('#data').html(data);
        });
    });
</script>