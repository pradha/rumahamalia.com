<script src="{{ url('/lib/parsleyjs/parsley.js') }}"></script>
<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{ url('/dashboard') }}">Main Dashboard</a>
        <span class="breadcrumb-item">Contents</span>
        <a class="breadcrumb-item" href="{{ url('/content/slider') }}">Slider</a>
        <a class="breadcrumb-item active" href="{{ url('/content/slider/edit/') }}{{ id }}">Edit</a>
    </nav>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
            <h4 class="tx-gray-800 mg-b-5">
                Edit Gambar Slider
                <a href="{{ url('/content/slider/') }}" class="btn btn-teal btn-with-icon">
                    <div class="ht-25">
                        <span class="icon wd-25"><i class="fa fa-list"></i></span>
                        <span class="pd-x-15">Daftar Slider</span>
                    </div>
                </a>
            </h4>
            <p class="mg-b-0">Edit Gambar Slider</p>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30 text-right">

        </div>
    </div>
</div>

<hr>
<div class="br-pagebody pd-sm-x-30">
    {% if status is defined AND status=="success" %}
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong class="d-block d-sm-inline-block-force">Success!</strong> Slider telah berhasil disimpan.
        </div><!-- alert -->
    {% elseif error.title is defined OR error.description is defined OR error.image is defined OR error.url is defined %}
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong class="d-block d-sm-inline-block-force">Oops!</strong> Silahkan periksa kembali kesalahan berikut :
            <ul class="mg-b-0">
                {% if error.title is defined %}<li>Judul slider belum diisi</li>{% endif %}
                {% if error.description is defined %}<li>Deskripsi slider belum diisi</li>{% endif %}
                {% if error.image is defined %}<li>Url gambar slider belum diisi</li>{% endif %}
                {% if error.url is defined %}<li>Target url slider belum diisi</li>{% endif %}
            </ul>
        </div>
    {% endif %}
    <form method="post" action="{{ url('/content/slider/edit/') }}{{ id }}" class="form-layout form-layout-1 bg-white" data-parsley-validate>
        <div class="row vdivide">
            <div class="col-lg-8">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label">Title: <span class="tx-danger">*</span></label>
                            <input class="form-control" type="text" name="title" value="{% if request.isPost %}{{ request.getPost('title') }}{% else %}{{ slider.title }}{% endif %}" placeholder="Slider title" required autocomplete="off">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label">Image Url: <span class="tx-danger">*</span></label>
                            <input class="form-control" type="text" name="image" value="{% if request.isPost %}{{ request.getPost('image') }}{% else %}{{ slider.image }}{% endif %}" placeholder="Image Url" required autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="form-control-label">Description: <span class="tx-danger">*</span></label>
                            <textarea rows="3" class="form-control" name="description" placeholder="Slider Description" required autocomplete="off">{% if request.isPost %}{{ request.getPost('description') }}{% else %}{{ slider.description }}{% endif %}</textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="form-group">
                            <label class="form-control-label">URL Target: <span class="tx-danger">*</span></label>
                            <input class="form-control" type="text" name="url" value="{% if request.isPost %}{{ request.getPost('url') }}{% else %}{{ slider.url }}{% endif %}" placeholder="Target Url" required autocomplete="off">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-control-label">Placement: <span class="tx-danger">*</span></label>
                            <select class="form-control" name="placement" data-placeholder="Choose Placement" tabindex="-1" aria-hidden="true">
                                <option label="Home"></option>
                                {% for category in categories %}
                                    <option value="{{ category.id }}" label="{{ category.name }}" {% if request.isPost() AND request.getPost('placement')==category.id %}selected{% elseif slider.placement==category.id %}selected{% endif %}>{{ category.name }}</option>
                                {% endfor %}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-control-label">Status: <span class="tx-danger">*</span></label>
                            <select class="form-control" name="status" data-placeholder="Choose Status" tabindex="-1" aria-hidden="true">
                                {% for status in statuses %}
                                    {% if status.id==1 OR status.id==2 %}
                                    <option value="{{ status.id }}" label="{{ status.name }}" {% if request.isPost() AND request.getPost('status')==status.id %}selected{% elseif slider.status==status.id %}selected{% endif %}>{{ status.name }}</option>
                                    {% endif %}
                                {% endfor %}
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-control-label">Preview Image: </label>
                    <div id="preview">
                        <img class="img img-fluid" src="{{ slider.image }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-layout-footer">
            <button type="submit" class="btn btn-teal">Simpan</button>
        </div>
    </form>
</div>

<script>
    $(document).ready(function(){
        $('[name="image"]').on('change', function() {
            $('#preview').html('<img class="img img-fluid" src="'+$('[name="image"]').val()+'">')
        });
        $("form").submit(function(e) {
            e.preventDefault();
            $form = $(this);
            $('#overlay').toggle();
            $.post(
                $form.attr('action'),
                $(this).serialize(),
                function(result){
                    $("#main").html(result);
                    $('#overlay').toggle();
                })
                .fail(function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                });

        });
    });
</script>