<link href="{{ url('/lib/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">

<script src="{{ url('/lib/parsleyjs/parsley.js') }}"></script>
<script src="{{ url('/lib/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>


<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{ url('/dashboard') }}">Main Dashboard</a>
        <span class="breadcrumb-item">Contents</span>
        <a class="breadcrumb-item" href="{{ url('/content/media') }}">Media</a>
        <a class="breadcrumb-item active" href="{{ url('/content/media/edit/') }}{{ id }}">Edit</a>
    </nav>
</div><!-- br-pageheader -->
<div class="row no-gutters">
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <h4 class="tx-gray-800 mg-b-5">
            Edit Media
            <a href="{{ url('/content/media') }}" class="btn btn-teal btn-with-icon">
                <div class="ht-25">
                    <span class="icon wd-25"><i class="fa fa-list-ul"></i></span>
                    <span class="pd-x-15">Daftar Media</span>
                </div>
            </a>
        </h4>
        <p class="mg-b-0">Edit Media (file gambar, dokumen, dll)</p>
    </div>

</div>

<hr>
<div class="br-pagebody pd-sm-x-30">
    <div class="row bg-white form-layout form-layout-1 vdivide">
        <div class="col-lg-4">
            {% if media.content_type=="image/png" OR media.content_type=="image/jpg" OR media.content_type=="image/jpeg" OR media.content_type=="image/gif" %}
                <img class="img img-fluid" src="{{ url('/files/upload/') }}{{ media.created_at|year }}/{{ media.created_at|month }}/{{ media.original_file }}">
            {% elseif media.content_type=="application/pdf"  %}
                <img class="img img-fluid" src="{{ url('/img/pdf.png') }}">
            {% elseif data.content_type=="text/plain"  %}
                <img class="img img-fluid" src="{{ url('/img/text.png') }}">
            {% else  %}
                <img class="img img-fluid" src="{{ url('/img/file.png') }}">
            {% endif %}
        </div>
        <div class="col-lg-8">
            {% if status is defined AND status=="success" %}
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong class="d-block d-sm-inline-block-force">Success!</strong> Media telah berhasil diperbarui.
                </div><!-- alert -->
            {% elseif error.email is defined OR error.name is defined OR error.phone is defined OR error.gender is defined OR error.date_of_birth is defined OR error.biography is defined %}
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong class="d-block d-sm-inline-block-force">Oops!</strong> Silahkan periksa kembali kesalahan berikut :
                    <ul class="mg-b-0">
                        {% if error.email is defined %}<li>E-Mail yang digunakan telah terdaftar</li>{% endif %}
                        {% if error.name is defined %}<li>Nama masih kosong</li>{% endif %}
                        {% if error.phone is defined %}<li>No telepon masih kosong</li>{% endif %}
                        {% if error.gender is defined %}<li>Jenis kelamin belum dipilih</li>{% endif %}
                        {% if error.date_of_birth is defined %}<li>Tanggal lahir masih kosong</li>{% endif %}
                        {% if error.biography is defined %}<li>Biography masih kosong</li>{% endif %}
                    </ul>
                </div>
            {% endif %}
            <form method="post" action="{{ url('/content/media/edit/') }}{{ id }}"  data-parsley-validate>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="form-control-label">Media Name/Title: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="name" value="{% if request.isPost() %}{{ request.getPost('name') }}{% else %}{{ media.name }}{% endif %}" placeholder="Media name/title" required autocomplete="off">

                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group mg-b-10-force">
                            <label class="form-control-label">Description: <span class="tx-danger">*</span></label>
                            <textarea rows="3" class="form-control" name="description" placeholder="Description" required autocomplete="off">{% if request.isPost() %}{{ request.getPost('description') }}{% else %}{{ media.description }}{% endif %}</textarea>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group mg-b-10-force">
                            <label class="form-control-label">keywords: <span class="tx-danger">*</span></label>
                            <input class="form-control" type="text" name="keywords" value="{% if request.isPost() %}{{ request.getPost('keywords') }}{% else %}{{ media.keywords }}{% endif %}" placeholder="Keywords or Tags" data-role="tagsinput" required>
                        </div>
                    </div>
                </div>

                <div class="form-layout-footer">
                    <button type="submit" class="btn btn-teal">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

        $("form").submit(function(e) {
            e.preventDefault();
            $form = $(this);
            $('#overlay').toggle();
            $.post(
                $form.attr('action'),
                $(this).serialize(),
                function(result){
                    $("#main").html(result);
                    $('#overlay').toggle();
                })
                .fail(function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                });

        });
    });
</script>