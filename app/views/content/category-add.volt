<link href="{{ url('/lib/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
<script src="{{ url('/lib/parsleyjs/parsley.js') }}"></script>
<script src="{{ url('/lib/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{ url('/dashboard') }}">Main Dashboard</a>
        <span class="breadcrumb-item">Contents</span>
        <a class="breadcrumb-item" href="{{ url('/content/category') }}">Category</a>
        <a class="breadcrumb-item active" href="{{ url('/content/category') }}">Add</a>
    </nav>
</div><!-- br-pageheader -->
<div class="row">
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
            <h4 class="tx-gray-800 mg-b-5">
                Tambah Kategori
                <a href="{{ url('/content/category/') }}" class="btn btn-teal btn-with-icon">
                    <div class="ht-25">
                        <span class="icon wd-25"><i class="fa fa-list"></i></span>
                        <span class="pd-x-15">Daftar Kategori</span>
                    </div>
                </a>
            </h4>
            <p class="mg-b-0">Tambah Kategori Postingan</p>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30 text-right">

        </div>
    </div>
</div>

<hr>
<div class="br-pagebody pd-sm-x-30">
    {% if error.name is defined OR error.description is defined  OR error.keywords is defined %}
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong class="d-block d-sm-inline-block-force">Oops!</strong> Silahkan periksa kembali kesalahan berikut :
            <ul class="mg-b-0">
                {% if error.name is defined %}<li>Judul halaman masih kosong</li>{% endif %}
                {% if error.description is defined %}<li>Deskripsi halaman masih kosong</li>{% endif %}
                {% if error.keywords is defined %}<li>Tambahkan beberapa tags/keywords untuk memudahkan pencarian</li>{% endif %}
                {% if error.save is defined %}
                    {% for err in error.save %}
                        <li>Something error in database</li>
                    {% endfor %}
                {% endif %}
            </ul>
        </div>
    {% endif %}
    <form method="post" action="{{ url('/content/category/add') }}" class="form-layout form-layout-1 bg-white" data-parsley-validate>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-control-label">Category Name: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="name" value="{% if request.isPost() %}{{ request.getPost('name') }}{% else %}{% endif %}" placeholder="Category Name" required autocomplete="off">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-control-label">Parent: <span class="tx-danger">*</span></label>
                    <select class="form-control select2 select2-hidden-accessible" name="parent" data-placeholder="Choose parent" tabindex="-1" aria-hidden="true">
                        <option label=".: no parent :."></option>
                        {% for category in parents %}
                            <option value="{{ category.id }}" label="{{ category.name }}" {% if request.isPost() AND request.getPost('parent')==category.id %}selected{% endif %}>{{ category.name }}</option>
                        {% endfor %}
                    </select>
                </div>
            </div><!-- col-4 -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group mg-b-10-force">
                    <label class="form-control-label">Description: <span class="tx-danger">*</span></label>
                    <textarea rows="3" class="form-control" name="description" placeholder="Category Description" required autocomplete="off">{% if request.isPost() %}{{ request.getPost('description') }}{% else %}{% endif %}</textarea>
                </div>
            </div>
        </div><!-- row -->
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-control-label">Options: <span class="tx-danger"></span></label>
                    <input class="form-control" type="text" name="opstions" value="{% if request.isPost() %}{{ request.getPost('options') }}{% else %}{% endif %}" placeholder="Options" autocomplete="off">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-control-label">Keywords: <span class="tx-danger">*</span></label>
                    <input class="form-control" data-role="tagsinput" type="text" name="keywords" value="{% if request.isPost() %}{{ request.getPost('keywords') }}{% else %}{% endif %}" placeholder="Keywords or tags" required autocomplete="off">
                </div>
            </div>

        </div>
        <div class="form-layout-footer">
            <button type="submit" class="btn btn-teal">Create</button>
        </div>
    </form>
</div>

<script>
    $(document).ready(function(){
        $("form").submit(function(e) {
            e.preventDefault();
            $form = $(this);
            $('#overlay').toggle();
            $.post(
                $form.attr('action'),
                $(this).serialize(),
                function(result){
                    $("#main").html(result);
                    $('#overlay').toggle();
                })
                .fail(function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                });

        });
    });
</script>