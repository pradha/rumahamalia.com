<script src="{{ url('/lib/parsleyjs/parsley.js') }}"></script>
<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{ url('/dashboard') }}">Main Dashboard</a>
        <span class="breadcrumb-item">Contents</span>
        <a class="breadcrumb-item" href="{{ url('/content/slider') }}">Slider</a>
        <a class="breadcrumb-item active" href="{{ url('/content/slider/add') }}">Add</a>
    </nav>
</div><!-- br-pageheader -->
<div class="row">
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
            <h4 class="tx-gray-800 mg-b-5">
                Tambah Gambar Slider
                <a href="{{ url('/content/slider/') }}" class="btn btn-teal btn-with-icon">
                    <div class="ht-25">
                        <span class="icon wd-25"><i class="fa fa-list"></i></span>
                        <span class="pd-x-15">Daftar Slider</span>
                    </div>
                </a>
            </h4>
            <p class="mg-b-0">Tambah Gambar Slider</p>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30 text-right">

        </div>
    </div>
</div>

<hr>
<div class="br-pagebody pd-sm-x-30">
    <form method="post" action="{{ url('/content/slider/add') }}" class="form-layout form-layout-1 bg-white" data-parsley-validate>
        <div class="row vdivide">
            <div class="col-lg-8">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label">Title: <span class="tx-danger">*</span></label>
                            <input class="form-control" type="text" name="title" value="{% if request.isPost %}{{ request.getPost('title') }}{% else %}{% endif %}" placeholder="Slider title" required autocomplete="off">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label">Image Url: <span class="tx-danger">*</span></label>
                            <input class="form-control" type="text" name="image" value="{% if request.isPost %}{{ request.getPost('image') }}{% else %}{% endif %}" placeholder="Image Url" required autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="form-control-label">Description: <span class="tx-danger">*</span></label>
                            <textarea rows="3" class="form-control" name="description" placeholder="Slider Description" required autocomplete="off" maxlength="150">{% if request.isPost %}{{ request.getPost('description') }}{% else %}{% endif %}</textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="form-group">
                            <label class="form-control-label">URL Target: <span class="tx-danger">*</span></label>
                            <input class="form-control" type="text" name="url" value="{% if request.isPost %}{{ request.getPost('url') }}{% else %}{% endif %}" placeholder="Target Url" required autocomplete="off">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label class="form-control-label">Placement: <span class="tx-danger">*</span></label>
                            <select class="form-control" name="placement" data-placeholder="Choose Placement" tabindex="-1" aria-hidden="true">
                                <option label="Home"></option>
                                {% for category in categories %}
                                    <option value="{{ category.id }}" label="{{ category.name }}" {% if request.isPost() AND request.getPost('placement')==category.id %}selected{% else %}{% endif %}>{{ category.name }}</option>
                                {% endfor %}
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-control-label">Preview Image: </label>
                    <div id="preview"></div>
                </div>
            </div>
        </div>
        <div class="form-layout-footer">
            <button type="submit" class="btn btn-teal">Create</button>
        </div>
    </form>
</div>

<script>
    $(document).ready(function(){
        $('[name="image"]').on('change', function() {
            $('#preview').html('<img class="img img-fluid" src="'+$('[name="image"]').val()+'">')
        });
        $("form").submit(function(e) {
            e.preventDefault();
            $form = $(this);
            $('#overlay').toggle();
            $.post(
                $form.attr('action'),
                $(this).serialize(),
                function(result){
                    $("#main").html(result);
                    $('#overlay').toggle();
                })
                .fail(function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                });

        });
    });
</script>