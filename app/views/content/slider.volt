<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{ url('/dashboard') }}">Main Dashboard</a>
        <span class="breadcrumb-item">Contents</span>
        <a class="breadcrumb-item active" href="{{ url('/content/slider') }}">Slider</a>
    </nav>
</div><!-- br-pageheader -->
<div class="row no-gutters">
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
            <h4 class="tx-gray-800 mg-b-5">
                Sliders
                {% if haveRole(session.get('group'),38) %}
                <a href="{{ url('/content/slider/add') }}" class="btn btn-teal btn-with-icon">
                    <div class="ht-25">
                        <span class="icon wd-25"><i class="fa fa-plus"></i></span>
                        <span class="pd-x-15">Tambah Slider</span>
                    </div>
                </a>
                {% endif %}
            </h4>
            <p class="mg-b-0">Daftar Slider</p>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
            <div class="input-group">
                <input name="search" type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                <button class="btn bd bg-white tx-gray-600" type="button"><i class="fa fa-search"></i></button>
            </span>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                $('[name="search"]').on('keypress', function (e) {
                    if(e.which === 13){
                        $('#overlay').toggle();
                        $.post(
                            '{{ url('/content/slider/search') }}',
                            { search : $('[name="search"]').val() },
                            function(result){
                                $(".br-pagebody").html(result);
                                $('#overlay').toggle();
                            })
                            .fail(function( jqXhr, textStatus, errorThrown ){
                                console.log( errorThrown );
                                $('#overlay').toggle();
                            });
                    }
                });
            });
        </script>
    </div>
</div>

<hr>
<div class="br-pagebody pd-sm-x-30">
    Total : <strong>{{ page.total_items }}</strong> Sliders
    <table class="table table-hover table-bordered">
        <thead class="thead-colored thead-teal">
        <tr>
            {% if haveRole(session.get('group'),39) %}
            <th class="text-light">#</th>
            {% endif %}
            <th class="text-light wd-20p-force">Slider</th>
            <th class="text-light">Title</th>
            <th class="text-light">Description</th>
            <th class="text-light">Target</th>
            <th class="text-light">Placement</th>
            <th class="text-light">Status</th>
            <th class="text-light">Created by</th>
            <th class="text-light">Created at</th>
        </tr>
        </thead>
        <tbody class="bg-lightsky">
        {% for data in page.items %}
        <tr>
            {% if haveRole(session.get('group'),39) %}
            <td>
                <a title="edit" class="btn btn-teal pd-y-2 pg-x-3" href="{{ url('/content/slider/edit/') }}{{ data.id }}"><i class="icon ion-edit"></i> </a>
                <a title="delete" class="btn btn-teal pd-y-2 pg-x-3" href="{{ url('/content/slider/delete/') }}{{ data.id }}"><i class="icon ion-trash-a"></i> </a>
            </td>
            {% endif %}
            <td><img src="{{ data.image }}" class="img img-fluid"></td>
            <td>{{ data.title }}</td>
            <td>{{ data.description }}</td>
            <td><a target="_blank" href="{{ data.url }}"><i class="icon fa fa-external-link"></i> </a> </td>
            <td>{{ data.placement_name }} </td>
            <td>{{ data.status_name }} </td>
            <td>{{ data.created_at|dateTimeFormat }} / {{ data.created_by }}</td>
            <td>{{ data.updated_at|dateTimeFormat }} / {{ data.updated_by }}</td>
        </tr>
        {% endfor %}
        </tbody>
    </table>
</div>