<div class="mg-b-10">
    Ditemukan sebanyak : <strong>{{ media|length }}</strong> data, Dari hasil pencarian : "<strong>{{ search }}</strong>"  <a title="hapus pencarian" class="btn btn-warning rounded-10 pd-x-10 pd-y-1" href="{{ url('/content/media') }}"><i class="icon fa fa-close"></i> </a>
</div>
<table class="table table-hover table-bordered table-responsive">
    <thead class="thead-colored thead-teal">
    <tr>
        <th class="text-light">#</th>
        <th class="text-light wd-5p-force">Media</th>
        <th class="text-light">Name</th>
        <th class="text-light">Description</th>
        <th class="text-light">Keywords</th>
        <th class="text-light">File Type</th>
        <th class="text-light">Size</th>
        <th class="text-light">Created</th>
        <th class="text-light">Updated</th>
    </tr>
    </thead>
    <tbody class="bg-lightsky">
    {% for data in media %}
        <tr>
            <td>
                <a title="Edit" class="btn btn-teal pd-y-2 pg-x-3" href="{{ url('/content/media/edit/') }}{{ data.id }}"><i class="icon ion-edit"></i> </a>
            </td>
            <td>
                {% if data.content_type=="image/png" OR data.content_type=="image/jpg" OR data.content_type=="image/jpeg" OR data.content_type=="image/gif" %}
                    <img class="img img-fluid" src="{{ url('/files/upload/') }}{{ data.created_at|year }}/{{ data.created_at|month }}/{{ data.original_file }}">
                {% elseif data.content_type=="application/pdf"  %}
                    <img class="img img-fluid" src="{{ url('/img/pdf.png') }}">
                {% elseif data.content_type=="text/plain"  %}
                    <img class="img img-fluid" src="{{ url('/img/text.png') }}">
                {% else  %}
                    <img class="img img-fluid" src="{{ url('/img/file.png') }}">
                {% endif %}
            </td>
            <td>{{ data.name }}</td>
            <td>{{ data.description }}</td>
            <td>{{ data.keywords|tags }}</td>
            <td>{{ data.content_type }}</td>
            <td class="text-right">{{ data.file_size|fileSizeUnit }}</td>
            <td>{{ data.created_at|dateTimeFormat }} / {{ data.created_by }}</td>
            <td>{{ data.updated_at|dateTimeFormat }} / {{ data.updated_by }}</td>
        </tr>
    {% endfor %}
    </tbody>