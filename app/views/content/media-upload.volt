<link href="{{ url('/css/dropzone.css') }}" rel="stylesheet">
<script src="{{ url('/js/dropzone.js') }}"></script>
<form action="{{ url('/content/media/insertUpload') }}" class="dropzone" id="dropzonewidget">
</form>
<div class="form-layout-footer">
    <button id="backToGallery" class="btn btn-teal">Back to gallery</button>
</div>
<script>
    Dropzone.autoDiscover = false;
    $("form.dropzone").dropzone({
        url: "{{ url('/content/media/insertUpload') }}",
        maxFilesize: 2,
        maxThumbnailFilesize: 5,
        init: function() {
            this.on("success", function(file, response) {
                response=JSON.parse(response);
                console.log(response);
                if(response.status == 'success')
                {
                    this.defaultOptions.success(file,'success');
                    $('<div class="btn btn-teal pd-3 mg-t-5 tx-10 insert" data-content="'+response.id+'">Detail</div>').insertAfter($(file.previewTemplate).find('.dz-filename').off());

                }
                else
                {
                    this.defaultOptions.error(file, response.message);
                }
            })
        }
    });
</script>

<script>
    $("#data").on("click",".insert", function(){
        $.get("{{ url('/content/media/insert/') }}"+$(this).attr('data-content'), function(data, status){
            $('#data').html(data);
        });
    });
    $('#backToGallery').on('click',function(){
        loadMedia();
    });

</script>