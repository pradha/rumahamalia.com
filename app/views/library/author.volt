<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{ url('/dashboard') }}">Main Dashboard</a>
        <span class="breadcrumb-item">Library</span>
        <a class="breadcrumb-item active" href="{{ url('/library/author') }}">Book Authors</a>
    </nav>
</div><!-- br-pageheader -->
<div class="row no-gutters">
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
            <h4 class="tx-gray-800 mg-b-5">
                Penulis Buku
                {% if haveRole(session.get('group'),30) %}
                    <a href="{{ url('/library/author/add') }}" class="btn btn-teal btn-with-icon">
                        <div class="ht-25">
                            <span class="icon wd-25"><i class="fa fa-plus"></i></span>
                            <span class="pd-x-15">Tambah Penulis</span>
                        </div>
                    </a>
                {% endif %}
            </h4>
            <p class="mg-b-0">Daftar Penulis Buku</p>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
            <div class="input-group">
                <input name="search" type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                <button class="btn bd bg-white tx-gray-600" type="button"><i class="fa fa-search"></i></button>
            </span>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                $('[name="search"]').on('keypress', function (e) {
                    if(e.which === 13){
                        $('#overlay').toggle();
                        $.post(
                            '{{ url('/library/author/search') }}',
                            { search : $('[name="search"]').val() },
                            function(result){
                                $(".br-pagebody").html(result);
                                $('#overlay').toggle();
                            })
                            .fail(function( jqXhr, textStatus, errorThrown ){
                                console.log( errorThrown );
                                $('#overlay').toggle();
                            });
                    }
                });
            });
        </script>
    </div>
</div>

<hr>
<div class="br-pagebody pd-sm-x-30">
    Total : <strong>{{ page.total_items }}</strong> Book Authors
    <table class="table table-hover table-bordered table-responsive">
        <thead class="thead-colored thead-teal">
        <tr>
            {% if haveRole(session.get('group'),31) %}
                <th class="text-light">#</th>
            {% endif %}
            <th class="text-light">Name</th>
            <th class="text-light">Information</th>
            <th class="text-light">Created</th>
            <th class="text-light">Updated</th>
        </tr>
        </thead>
        <tbody class="bg-lightsky">
        {% for data in page.items %}
            <tr>
                {% if haveRole(session.get('group'),31) %}
                    <td>
                        <a title="edit" class="btn btn-teal pd-y-2 pg-x-3" href="{{ url('/library/author/edit/') }}{{ data.id }}"><i class="icon ion-edit"></i> </a>
                    </td>
                {% endif %}
                <td>{{ data.name }}</td>
                <td>{{ data.information }}</td>
                <td>{{ data.created_at|dateTimeFormat }} / {{ data.created_by }}</td>
                <td>{{ data.updated_at|dateTimeFormat }} / {{ data.updated_by }}</td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
</div>