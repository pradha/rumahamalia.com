<script src="{{ url('/lib/parsleyjs/parsley.js') }}"></script>
<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{ url('/dashboard') }}">Main Dashboard</a>
        <span class="breadcrumb-item">Library</span>
        <a class="breadcrumb-item" href="{{ url('/library/genre') }}">Genre</a>
        <a class="breadcrumb-item active" href="{{ url('/library/genre/add') }}">Add</a>
    </nav>
</div><!-- br-pageheader -->
<div class="row no-gutters">
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
            <h4 class="tx-gray-800 mg-b-5">
                Tambah Genre Buku
                {% if haveRole(session.get('group'),30) %}
                    <a href="{{ url('/library/genre') }}" class="btn btn-teal btn-with-icon">
                        <div class="ht-25">
                            <span class="icon wd-25"><i class="fa fa-list-alt"></i></span>
                            <span class="pd-x-15">Daftar Genre Buku</span>
                        </div>
                    </a>
                {% endif %}
            </h4>
            <p class="mg-b-0">Tambah Genre Buku</p>
        </div>
    </div>
</div>

<hr>
<div class="br-pagebody pd-sm-x-30">
    {% if error.name is defined OR error.description is defined OR error.save is defined %}
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong class="d-block d-sm-inline-block-force">Oops!</strong> Silahkan periksa kembali kesalahan berikut :
            <ul class="mg-b-0">
                {% if error.name is defined %}<li>Nama masih kosong</li>{% endif %}
                {% if error.description is defined %}<li>Deskripsi masih kosong</li>{% endif %}
                {% if error.save is defined %}
                    {% for err in error.save %}
                        <li>{{ err }}</li>
                    {% endfor %}
                {% endif %}
            </ul>
        </div>
    {% endif %}
    <?php
    print_r($error);
    ?>
    <form method="post" action="{{ url('/library/genre/add') }}" class="form-layout form-layout-1 bg-white" data-parsley-validate>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-control-label">Genre Name: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="name" value="{% if request.isPost() %}{{ request.getPost('name') }}{% else %}{% endif %}" placeholder="Genre Name" required autocomplete="off">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group mg-b-10-force">
                    <label class="form-control-label">Description: <span class="tx-danger">*</span></label>
                    <textarea rows="3" class="form-control" name="description" placeholder="Genre Description" required autocomplete="off">{% if request.isPost() %}{{ request.getPost('description') }}{% else %}{% endif %}</textarea>
                </div>
            </div>
        </div><!-- row -->
        <div class="form-layout-footer">
            <button type="submit" class="btn btn-teal">Create</button>
        </div>
    </form>
</div>
<script>
    $(document).ready(function(){
        $("form").submit(function(e) {
            e.preventDefault();
            $form = $(this);
            $('#overlay').toggle();
            $.post(
                $form.attr('action'),
                $(this).serialize(),
                function(result){
                    $("#main").html(result);
                    $('#overlay').toggle();
                })
                .fail(function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                });

        });
    });
</script>