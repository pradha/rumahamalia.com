<script src="{{ url('/lib/parsleyjs/parsley.js') }}"></script>
<script src="{{ url('/js/simpleUpload.min.js') }}"></script>
<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{ url('/dashboard') }}">Main Dashboard</a>
        <span class="breadcrumb-item">Library</span>
        <a class="breadcrumb-item" href="{{ url('/library/book/') }}">Book</a>
        <a class="breadcrumb-item active" href="{{ url('/library/book/edit/') }}{{ id }}">Add Book</a>
    </nav>
</div><!-- br-pageheader -->
<div class="row no-gutters">
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
            <h4 class="tx-gray-800 mg-b-5">
                Edit Buku
                {% if haveRole(session.get('group'),30) %}
                    <a href="{{ url('/library/book') }}" class="btn btn-teal btn-with-icon">
                        <div class="ht-25">
                            <span class="icon wd-25"><i class="fa fa-list-alt"></i></span>
                            <span class="pd-x-15">Daftar Buku</span>
                        </div>
                    </a>
                {% endif %}
            </h4>
            <p class="mg-b-0">Daftar Buku</p>
        </div>
    </div>
</div>

<hr>
<div class="br-pagebody pd-sm-x-30">
    {% if status is defined AND status=="success" %}
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong class="d-block d-sm-inline-block-force">Success!</strong> Buku telah berhasil disimpan.
        </div><!-- alert -->
    {% elseif error.title is defined OR error.description is defined OR error.author is defined OR error.publication_date is defined OR error.quantity is defined OR error.genre is defined OR error.save is defined %}
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong class="d-block d-sm-inline-block-force">Oops!</strong> Silahkan periksa kembali kesalahan berikut :
            <ul class="mg-b-0">
                {% if error.title is defined %}<li>Nama masih kosong</li>{% endif %}
                {% if error.description is defined %}<li>Deskriprsi masih kosong</li>{% endif %}
                {% if error.author is defined %}<li>Author belum dipilih</li>{% endif %}
                {% if error.publication_date is defined %}<li>Tanggal publikasi masih kosong</li>{% endif %}
                {% if error.quantity is defined %}<li>Kuantitas/stok buku masih kosong</li>{% endif %}
                {% if error.genre is defined %}<li>Genre buku belum dipiih</li>{% endif %}
                {% if error.save is defined %}
                    {% for err in error.save %}
                        <li>{{ err }}</li>
                    {% endfor %}
                {% endif %}
            </ul>
        </div>
    {% endif %}
    <form method="post" action="{{ url('/library/book/edit/') }}{{ id }}" class="form-layout form-layout-1 bg-white" data-parsley-validate>
        <div class="row vdivide">
            <div class="col-lg-9">
                <div class="row vdivide">
                    <div class="col-lg-3 pict">
                        <label class="form-control-label">Book Cover:</label>
                        <hr>
                        {% if book.cover!="" %}
                            <img src="{{ url('/files/upload/book/cover/') }}{{ book.cover }}" class="img img-fluid cover">
                        {% else %}
                            <img src="{{ url('/img/book.png') }}" class="img img-fluid cover">
                        {% endif %}
                        <div class="control">
                            <span class="text" data-toggle="modal" data-target="#cover"> <i class="icon fa fa-image"></i> Ganti Foto</span>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <label class="form-control-label">Book Title: <span class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" name="title" value="{% if request.isPost() %}{{ request.getPost('title') }}{% else %}{{ book.title }}{% endif %}" placeholder="Book Title" required autocomplete="off" maxlength="100">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="form-control-label">ISBN:</label>
                                    <input class="form-control" type="text" name="isbn" value="{% if request.isPost() %}{{ request.getPost('isbn') }}{% else %}{{ book.isbn }}{% endif %}" placeholder="ISBN" autocomplete="off" maxlength="13">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group mg-b-10-force">
                                    <label class="form-control-label">Description: <span class="tx-danger">*</span></label>
                                    <textarea rows="10" class="form-control" name="description" placeholder="Book Description" required autocomplete="off">{% if request.isPost() %}{{ request.getPost('description') }}{% else %}{{ book.description }}{% endif %}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-7">
                                <div class="form-group">
                                    <label class="form-control-label">Author: <span class="tx-danger">*</span></label>
                                    <select class="form-control select2 select2-hidden-accessible" required name="author" data-placeholder="Choose Author" tabindex="-1" aria-hidden="true">
                                        <option label=".: no author :."></option>
                                        {% for author in authors %}
                                            <option value="{{ author.id }}" label="{{ author.name }}" {% if request.isPost() AND request.getPost('author')==author.id %}selected{% elseif book.author==author.id %}selected{% endif %}>{{ author.name }}</option>
                                        {% endfor %}
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="form-control-label">Tanggal Publikasi:</label>
                                    <input class="form-control" type="text" name="publication_date" value="{% if request.isPost() %}{{ request.getPost('publication_date') }}{% else %}{{ book.publication_date }}{% endif %}" placeholder="Publication Date" required autocomplete="off">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label class="form-control-label">Quantity:</label>
                                    <input class="form-control" type="text" name="quantity" value="{% if request.isPost() %}{{ request.getPost('quantity') }}{% else %}{{ book.quantity }}{% endif %}" placeholder="Quantity" required autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="form-layout-footer">
                            <button type="submit" class="btn btn-teal">Save</button>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label class="form-control-label">Genre:</label>
                    <hr>
                    <div id="cbWrapper" class="row">
                        {% for genre in genres %}
                            <div class="col-lg-6">
                                <ul class="catList">
                                    <li>
                                        <label class="ckbox ckbox-info">
                                            <input name="genre[]" type="checkbox" value="{{ genre.id }}" data-parsley-mincheck="1" data-parsley-class-handler="#cbWrapper" data-parsley-errors-container="#cbErrorContainer" required <?php if ( ($this->request->isPost() && is_array($this->request->getPost('genre')) && in_array($genre->id,$this->request->getPost('genre'))) || (!$this->request->isPost() && in_array_multi($genre->id,$genreBook)) ) echo 'checked'; ?>>
                                            <span>{{ genre.name }}</span>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                        {% endfor %}
                    </div>
                    <div id="cbErrorContainer"></div>
                </div>
            </div>
        </div>
    </form>
</div>

<div id="cover" class="modal fade">
    <div class="modal-dialog modal-dialog-vertical-center" role="document">
        <div class="modal-content bd-0 tx-14">
            <div class="modal-header pd-y-20 pd-x-25">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Upload Image</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body pd-25">
                <form id="upload" method="post" action="{{ url('profile/uploadPict') }}" enctype="multipart/form-data">
                    <input type="file" name="file">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div><!-- modal-dialog -->
</div>

<script>
    $(document).ready(function(){
        $("form").submit(function(e) {
            e.preventDefault();
            $form = $(this);
            $('#overlay').toggle();
            $.post(
                $form.attr('action'),
                $(this).serialize(),
                function(result){
                    $("#main").html(result);
                    $('#overlay').toggle();
                })
                .fail(function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                });
        });

        $('input[type=file]').change(function(){

            $(this).simpleUpload("{{ url('/library/book/upload/') }}{{ id }}", {

                start: function(file){
                    console.log("upload started");
                },

                progress: function(progress){
                    console.log("upload progress: " + Math.round(progress) + "%");
                },

                success: function(data){
                    if (data!="failed"){
                        $("#upload").html('<img src="'+data+'" class="img ht-200">');
                        $("img.cover").attr('src',data);
                    }
                    console.log("upload successful!");
                    console.log(data);
                },

                error: function(error){
                    console.log("upload error: " + error.name + ": " + error.message);
                }

            });

        });

        $('[name="publication_date"]').datepicker({
            changeMonth: true,
            changeYear: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            maxDate:0,
            dateFormat: 'yy-mm-dd',
        });
    });
</script>