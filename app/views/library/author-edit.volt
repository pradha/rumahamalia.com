<script src="{{ url('/lib/parsleyjs/parsley.js') }}"></script>
<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{ url('/dashboard') }}">Main Dashboard</a>
        <span class="breadcrumb-item">Library</span>
        <a class="breadcrumb-item" href="{{ url('/library/author') }}">Book Authors</a>
        <a class="breadcrumb-item active" href="{{ url('/library/author/edit/') }}{{ id }}">Edit</a>
    </nav>
</div><!-- br-pageheader -->
<div class="row no-gutters">
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
            <h4 class="tx-gray-800 mg-b-5">
                Edit Penulis Buku
                {% if haveRole(session.get('group'),30) %}
                    <a href="{{ url('/library/author') }}" class="btn btn-teal btn-with-icon">
                        <div class="ht-25">
                            <span class="icon wd-25"><i class="fa fa-plus"></i></span>
                            <span class="pd-x-15">Daftar Penulis Buku</span>
                        </div>
                    </a>
                {% endif %}
            </h4>
            <p class="mg-b-0">Edit Penulis Buku</p>
        </div>
    </div>
</div>

<hr>
<div class="br-pagebody pd-sm-x-30">
    {% if status is defined AND status=="success" %}
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong class="d-block d-sm-inline-block-force">Success!</strong> Penulis buku telah berhasil disimpan.
        </div><!-- alert -->
    {% elseif error.name is defined OR error.information is defined %}
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong class="d-block d-sm-inline-block-force">Oops!</strong> Silahkan periksa kembali kesalahan berikut :
            <ul class="mg-b-0">
                {% if error.name is defined %}<li>Nama masih kosong</li>{% endif %}
                {% if error.information is defined %}<li>Informasi masih kosong</li>{% endif %}
            </ul>
        </div>
    {% endif %}
    <form method="post" action="{{ url('/library/author/edit/') }}{{ id }}" class="form-layout form-layout-1 bg-white" data-parsley-validate>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-control-label">Author Name: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="name" value="{% if request.isPost() %}{{ request.getPost('name') }}{% else %}{{ author.name }}{% endif %}" placeholder="Author Name" required autocomplete="off">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group mg-b-10-force">
                    <label class="form-control-label">Information: <span class="tx-danger">*</span></label>
                    <textarea rows="3" class="form-control" name="information" placeholder="Author Information" required autocomplete="off">{% if request.isPost() %}{{ request.getPost('information') }}{% else %}{{ author.information }}{% endif %}</textarea>
                </div>
            </div>
        </div><!-- row -->
        <div class="form-layout-footer">
            <button type="submit" class="btn btn-teal">Create</button>
        </div>
    </form>
</div>
<script>
    $(document).ready(function(){
        $("form").submit(function(e) {
            e.preventDefault();
            $form = $(this);
            $('#overlay').toggle();
            $.post(
                $form.attr('action'),
                $(this).serialize(),
                function(result){
                    $("#main").html(result);
                    $('#overlay').toggle();
                })
                .fail(function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                });

        });
    });
</script>