<div class="ra-nav hidden-md-down bg-gray-100">
    <div class="container">
        <div class="ht-65  d-flex align-items-center justify-content-end">
            <h4 class="mg-b-0 tx-uppercase tx-bold tx-spacing--2 tx-inverse tx-poppins mg-r-auto">
                <a href="{{ url() }}">
                    <img src="{{ url('/img/logo-bg.png') }}" class="ht-50 mg-r-10">
                    Rumah Amalia
                </a>
            </h4>
            <ul class="nav active-info tx-uppercase tx-12 tx-medium tx-spacing-2 flex-column flex-sm-row" role="tablist">
                <li class="nav-item"><a class="nav-link"  href="{{ url() }}" role="tab">Home</a></li>
                {% for page in pages %}
                    <li class="nav-item"><a class="nav-link"  href="{{ url('/page/') }}{{ page.slug }}" role="tab">{{ page.title }}</a></li>
                {% endfor %}
            </ul>
            <div class="social mg-l-20">
                <a href="https://www.facebook.com/Dahsyatnya.Doa" target="_blank" class="hover-dark tx-16 mg-l-10"><i class="fa fa-facebook"></i></a>
                <a href="http://instagram.com/dahsyatnya.doa" target="_blank" class="hover-dark tx-16 mg-l-10"><i class="fa fa-instagram"></i></a>
            </div>
        </div>
    </div>
</div>
<div class="ra-nav-index hidden-lg-up ">
    <div class="container">
        <div class="ht-65 d-flex align-items-center justify-content-between">
            <a href="#"  class="tx-gray-600 hover-dark openSidebar tx-18"><i class="fa fa-navicon"></i></a>
            <h4 class="mg-b-0 tx-uppercase tx-bold tx-spacing--2 tx-inverse tx-poppins mg-r-20">
                <a href="{{ url() }}">
                    Rumah Amalia
                </a>
            </h4>
            <div>

            </div>
        </div>
    </div>

</div>
{{ content() }}

<footer class="footer-distributed">
    <div class="container">
        <div class="footer-left">

            <h3><img src="{{ url('/img/logo-bg.png') }}" class="ht-100 mg-r-20"> Rumah Amalia</h3>

            <p class="footer-links">
                <a href="">Beranda</a> .
                {% for page in pages %}
                    <a href="{{ url('/page/') }}{{ page.slug }}">{{ page.title }}</a>
                    {% if loop.last==false %}
                        .
                    {% endif %}
                {% endfor %}
            </p>

            <p class="footer-company-name">Rumah Amalia &copy; 2018. All rights reserved</p>
        </div>

        <div class="footer-center">

            <div>
                <i class="fa fa-map-marker"></i>
                <p><span>Jl. Subagyo IV Blok ii, No. 24 Komplek Peruri</span> Ciledug, Tangerang. 15151</p>
            </div>

            <div>
                <i class="fa fa-phone"></i>
                <p>+62 878 777 124 31</p>
            </div>

            <div>
                <i class="fa fa-envelope"></i>
                <p><a href="mailto:agussyafii@rumahamalia.com">agussyafii@rumahamalia.com</a></p>
            </div>

        </div>

        <div class="footer-right">

            <p class="footer-company-about">
                <span>Sekilas Rumah Amalia</span>
                Rumah Amalia adalah Komunitas rumah belajar untuk anak Yatim & anak Dhuafa, berjumlah 90 anak amalia.
            </p>

            <div class="footer-icons">
                <a target="_blank" href="https://facebook.com/Dahsyatnya.Doa"><i class="fa fa-facebook"></i></a>
                <a target="_blank" href="http://instagram.com/dahsyatnya.doa"><i class="fa fa-instagram"></i></a>
            </div>

        </div>
    </div>


</footer>

<script>
    $(document).ready(function() {
        $('.openSidebar').on('click', function () {
            $(".sidebar").toggle("slide", {direction: "left"}, 500);
        });
    });
</script>
<div class="sidebar  bg-gray-900 ">
    <a href="#"  class="tx-gray-600 hover-dark openSidebar tx-18"><i class="fa fa-close"></i></a>
    <div class="pd-10">
        <ul class="nav nav-pills nav-pills-for-dark flex-column" role="tablist">
            <li class="nav-item"><a class="nav-link" href="{{ url() }}">Home</a></li>
            {% for page in pages %}
                <li class="nav-item"><a class="nav-link" href="{{ url('/page/') }}{{ page.slug }}">{{ page.title }}</a></li>
            {% endfor %}
        </ul>
    </div>
</div>