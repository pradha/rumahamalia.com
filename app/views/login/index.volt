<link href="{{ url('/lib/SpinKit/spinkit.css') }}" rel="stylesheet">

<script src="{{ url('/lib/parsleyjs/parsley.js') }}"></script>

<div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v">
    <div class="row no-gutters">
        <div class="col-lg-6 bg-warning text-center">
            <div class="pd-40">
                <h3 class="tx-white mg-b-20">{{ options[0].value }}</h3>
                <p class="pd-15">
                    <img class="img-fluid" src="{{ options[7].value }} ">
                </p>
            </div>
        </div>
        <div class="col-lg-6 bg-white">
            <div id="overlay">
                <div class="d-flex  ht-100p pos-relative align-items-center">
                    <div class="sk-three-bounce">
                        <div class="sk-child sk-bounce1 bg-gray-800"></div>
                        <div class="sk-child sk-bounce2 bg-gray-800"></div>
                        <div class="sk-child sk-bounce3 bg-gray-800"></div>
                        <div class="text">Access login...</div>
                    </div>
                </div>
            </div>
            <div class="pd-30">
                <div class="pd-x-30 pd-y-10">
                    <h3 class="tx-inverse  mg-b-5">Selamat Datang Kembali!</h3>
                    <p>Silahkan masuk ke akun anda untuk melanjutkan</p>
                    <br>

                    <form id="login" method="post" action="{{ url('/login/') }}{{ param }}" data-parsley-validate>
                        <div class="form-group">
                            <input type="text" name="usermail" class="form-control pd-y-12" placeholder="Masukan username atau email" autocomplete="off" required>
                        </div>
                        <div class="form-group mg-b-20">
                            <input type="password" name="password" class="form-control pd-y-12" placeholder="Masukan password" required>
                            <a href="{{ url('/recover') }}" class="tx-12 d-block mg-t-10">Lupa password?</a>
                        </div>
                        <button type="submit" class="btn btn-warning pd-y-12 btn-block">Sign In</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('[name="usermail"]').focus();
        $("form").submit(function(e){
            e.preventDefault();
            $form = $(this);
            $('#overlay').toggle();

            $.post(
                $form.attr('action'),
                $(this).serialize(),
                function(result){
                    if (result!="success"){
                        $('#overlay').toggle();
                        $(".alert").remove();
                        var alert='<div class="alert alert-warning" role="alert">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                            '<span aria-hidden="true">&times;</span>' +
                            '</button>' +
                            '<strong class="d-block d-sm-inline-block-force">Oops!</strong> ' +result+
                            '</div>';
                        $('#login').prepend(alert);
                        $('[name="usermail"]').val('');
                        $('[name="password"]').val('');
                        $('[name="usermail"]').focus();
                    } else {
                        $('#overlay .text').html("<br><br>Login success...!Redirecting...");
                        window.location.replace('{{ url('/dashboard') }}');
                    }
                })
                .fail(function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                });

        });
    });
</script>