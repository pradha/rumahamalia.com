<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{ url('/dashboard') }}">Main Dashboard</a>
        <a class="breadcrumb-item active" href="{{ url('/profile') }}">Profile</a>
    </nav>
</div><!-- br-pageheader -->
<div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
    <h4 class="tx-gray-800 mg-b-5">
        Profile Saya
        {% if haveRole(session.get('group'),4) %}
        <a href="{{ url('/profile/edit/') }}" class="btn btn-teal btn-with-icon">
            <div class="ht-25">
                <span class="icon wd-25"><i class="fa fa-edit"></i></span>
                <span class="pd-x-15">Edit Profile</span>
            </div>
        </a>
        {% endif %}
    </h4>
    <p class="mg-b-0">Informasi Profile Saya</p>
</div>

<div class="br-pagebody">
    <div class="row row-sm mg-t-20">
        <div class="col-lg-12 mg-t-20 mg-lg-t-0">
            <div class="widget-2">
                <div class="card shadow-base overflow-hidden">
                    <div class="card-body pd-0 bd-color-gray-lighter">
                        <div class="row ">
                            <div class="col-lg-3 col-sm-4 pd-y-20">
                                {% if user.photo!="" %}
                                    <img src="{{ url('/files/upload/profile/') }}{{ user.photo }}" class="img profile img-fluid">
                                {% else %}
                                    <img src="{{ url('/img/user.png') }}" class="img profile img-fluid">
                                {% endif %}
                            </div><!-- col-2 -->
                            <div class="col-lg-9 col-sm-8 pd-y-20 pd-x-20 bd-l bd-color-gray-lighter">
                                <div class="row">
                                    <div class="col-lg-4 col-sm-6">
                                        <label class="tx-12 tx-gray-600 mg-b-0">Username</label>
                                        <div class="tx-18 tx-black">{{ user.username }}</div><br>
                                    </div>
                                    <div class="col-lg-4 col-sm-6">
                                        <label class="tx-12 tx-gray-600 mg-b-0">E-Mail</label>
                                        <div class=" tx-18 tx-black">{{ user.email }}</div><br>
                                    </div>
                                    <div class="col-lg-4 col-sm-6">
                                        <label class="tx-12 tx-gray-600 mg-b-0">Full Name</label>
                                        <div class=" tx-18 tx-black">{{ user.name }}</div><br>
                                    </div>
                                    <div class="col-lg-4 col-sm-6">
                                        <label class="tx-12 tx-gray-600 mg-b-0">Phone</label>
                                        <div class=" tx-18 tx-black">{{ user.phone }}</div><br>
                                    </div>
                                    <div class="col-lg-4 col-sm-6">
                                        <label class="tx-12 tx-gray-600 mg-b-0">Gender</label>
                                        <div class=" tx-18 tx-black">{{ user.gender_name }}</div><br>
                                    </div>
                                    <div class="col-lg-4 col-sm-6">
                                        <label class="tx-12 tx-gray-600 mg-b-0">Date of Birth</label>
                                        <div class=" tx-18 tx-black">{{ user.date_of_birth|dateFormat }}</div><br>
                                    </div>
                                    <div class="col-lg-4 col-sm-6">
                                        <label class="tx-12 tx-gray-600 mg-b-0">Biography</label>
                                        <div class=" tx-18 tx-black">{{ user.biography }}</div><br>
                                    </div>
                                    <div class="col-lg-4 col-sm-6">
                                        <label class="tx-12 tx-gray-600 mg-b-0">Access Level</label>
                                        <div class=" tx-18 tx-black">{{ user.group_name }}</div><br>
                                    </div>
                                </div>

                            </div><!-- col-2 -->

                        </div><!-- row -->
                    </div><!-- card-body -->
                </div><!-- card -->
            </div><!-- widget-2 -->
        </div><!-- col-6 -->
    </div>


</div>