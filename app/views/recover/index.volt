<link href="{{ url('/lib/SpinKit/spinkit.css') }}" rel="stylesheet">

<script src="{{ url('/lib/parsleyjs/parsley.js') }}"></script>

<div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v">
    <div class="row no-gutters">

        <div class="col-lg-12 bg-white">
            <div id="overlay">
                <div class="d-flex  ht-100p pos-relative align-items-center">
                    <div class="sk-three-bounce">
                        <div class="sk-child sk-bounce1 bg-gray-800"></div>
                        <div class="sk-child sk-bounce2 bg-gray-800"></div>
                        <div class="sk-child sk-bounce3 bg-gray-800"></div>
                        <div class="text">Memeriksa akun...</div>
                    </div>
                </div>
            </div>
            <div class="pd-30">
                <div class="pd-x-30 pd-y-10">
                    <h3 class="tx-inverse  mg-b-5">Lupa Password</h3>
                    <p>Silahkan masukan email atau username anda, selanjutnya system akan memberikan informasi mengenai password anda</p>
                    <br>

                    <form id="recover" method="post" action="{{ url('/recover/') }}" data-parsley-validate>
                        <div class="form-group">
                            <input type="text" name="usermail" class="form-control pd-y-12" placeholder="Masukan username atau email" autocomplete="off" required>
                        </div>
                        <button type="submit" class="btn btn-warning pd-y-12 btn-block">Recover</button>
                        <a href="{{ url('/index/') }}{{ key }}" class="tx-12 d-block mg-t-10">Login</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('[name="usermail"]').focus();
        $("form").submit(function(e){
            e.preventDefault();
            $form = $(this);
            $('#overlay').toggle();

            $.post(
                $form.attr('action'),
                $(this).serialize(),
                function(result){
                    if (result!="success"){
                        $('#overlay').toggle();
                        $(".alert").remove();
                        var alert='<div class="alert alert-warning" role="alert">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                            '<span aria-hidden="true">&times;</span>' +
                            '</button>' +
                            '<strong class="d-block d-sm-inline-block-force">Oops!</strong> ' +result+
                            '</div>';
                        $('#recover').prepend(alert);
                        $('[name="usermail"]').val('');
                        $('[name="usermail"]').focus();
                    } else {
                        var success='<div class="alert alert-success" role="alert">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                            '<span aria-hidden="true">&times;</span>' +
                            '</button>' +
                            '<strong class="d-block d-sm-inline-block-force">Silahkan periksa email anda, kami telah mengirimkan tautan untuk mengembalikan password</strong>'+
                            '</div>';
                        $('#overlay').toggle();
                        $('#recover').html(success);
                    }
                })
                .fail(function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                });

        });
    });
</script>