<div class="page mg-t-90">
    <div class="container">
        <div class="row vdivide">
            <div class="col-lg-9">
                <h1 class="page-header">{{ data.title }}</h1>
                <hr>
                <div class="content">
                {{ data.content }}
                </div>
            </div>
            <div class="col-lg-3"></div>
        </div>

    </div>
</div>