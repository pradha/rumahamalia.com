<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{ url('/dashboard') }}">Main Dashboard</a>
        <span class="breadcrumb-item">System</span>
        <a class="breadcrumb-item active" href="{{ url('/system/user') }}">User</a>
    </nav>
</div><!-- br-pageheader -->
<div class="row">
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
            <h4 class="tx-gray-800 mg-b-5">
                Pengguna
                {% if haveRole(session.get('group'),9) %}
                <a href="{{ url('/system/user/add') }}" class="btn btn-teal btn-with-icon">
                    <div class="ht-25">
                        <span class="icon wd-25"><i class="fa fa-plus"></i></span>
                        <span class="pd-x-15">Tambah Pengguna</span>
                    </div>
                </a>
                {% endif %}
            </h4>
            <p class="mg-b-0">Daftar Pengguna</p>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
            <div class="input-group">
                <input name="search" type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                <button class="btn bd bg-white tx-gray-600" type="button"><i class="fa fa-search"></i></button>
            </span>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                $('[name="search"]').on('keypress', function (e) {
                    if(e.which === 13){
                        $('#overlay').toggle();
                        $.post(
                            '{{ url('/system/user/search') }}',
                            { search : $('[name="search"]').val() },
                            function(result){
                                $(".br-pagebody").html(result);
                                $('#overlay').toggle();
                            })
                            .fail(function( jqXhr, textStatus, errorThrown ){
                                console.log( errorThrown );
                                $('#overlay').toggle();
                            });
                    }
                });
            });
        </script>
    </div>
</div>

<hr>
<div class="br-pagebody pd-sm-x-30">
    Total : <strong>{{ page.total_items }}</strong> Users
    <table class="table table-hover table-bordered">
        <thead class="thead-colored thead-teal">
        <tr>
            {% if haveRole(session.get('group'),10) %}<th class="text-light">#</th>{% endif %}
            <th class="text-light">Username</th>
            <th class="text-light">Name</th>
            <th class="text-light">Email</th>
            <th class="text-light">Gender</th>
            <th class="text-light">Biography</th>
            <th class="text-light">Active</th>
            <th class="text-light">Last Logged From</th>
            <th class="text-light">Status</th>
            <th class="text-light">Created by</th>
            <th class="text-light">Created at</th>
        </tr>
        </thead>
        <tbody class="bg-lightsky">
        {% for data in page.items %}
            <tr>
                {% if haveRole(session.get('group'),10) %}
                <td>
                    <a title="edit" class="btn btn-teal pd-y-2 pg-x-3" href="{{ url('/system/user/edit/') }}{{ data.username|encode }}"><i class="icon ion-edit"></i> </a>
                </td>
                {% endif %}
                <td>{{ data.username }}</td>
                <td>{{ data.name }}</td>
                <td>{{ data.email }}</td>
                <td>{{ data.gender_name }}</td>
                <td>{{ data.biography }}</td>
                <td>{% if data.is_logged_in %}<i class="icon ion-checkmark"></i>{% else %}<i class="icon ion-close"></i>{% endif %} </td>
                <td>{{ data.last_logged_in_from }} </td>
                <td>{% if data.status %}<i class="icon ion-checkmark"></i>{% else %}<i class="icon ion-close"></i>{% endif %} </td>
                <td>{{ data.created_at|dateTimeFormat }} / {{ data.created_by }}</td>
                <td>{{ data.updated_at|dateTimeFormat }} / {{ data.updated_by }}</td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    <div class="d-flex align-items-center justify-content-center">
        <nav aria-label="Page navigation">
            <ul class="pagination pagination-basic mg-b-0">
                {% if page.current!=1 %}
                    {% if page.current>5 %}
                    <li class="page-item">
                        <a class="page-link" href="{{ url('/system/user/') }}{{ page.next }}" aria-label="Next">
                            <i class="fa fa-angle-double-left"></i>
                        </a>
                    </li>
                        {% endif %}
                    <li class="page-item">
                        <a class="page-link" href="{{ url('/system/user//') }}{{ page.next }}" aria-label="Next">
                            <i class="fa fa-angle-left"></i>
                        </a>
                    </li>
                {% endif %}
                {% for i in 1..page.last %}
                    {% if (i>page.current-5 AND i<page.current+5)  %}
                        <li class="page-item {%  if i==page.current %}active{% endif %}"><a class="page-link" href="{{ url('/system/user/') }}{{ i }}">{{ i }}</a></li>
                    {% endif %}
                {% endfor %}

                {% if page.current!=page.last %}
                    <li class="page-item">
                        <a class="page-link" href="{{ url('/system/user/') }}{{ page.next }}" aria-label="Next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </li>
                {% endif %}
                {% if page.last>5 AND page.current!=page.last %}

                    <li class="page-item">
                        <a class="page-link" href="{{ url('/system/user/') }}{{ page.last }}" aria-label="Last">
                            <i class="fa fa-angle-double-right"></i>
                        </a>
                    </li>
                {% endif %}
            </ul>
        </nav>
    </div>
</div>