<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{ url('/dashboard') }}">Main Dashboard</a>
        <span class="breadcrumb-item">System</span>
        <a class="breadcrumb-item active" href="{{ url('/system/group') }}">Group</a>
    </nav>
</div><!-- br-pageheader -->
<div class="row no-gutters">
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
            <h4 class="tx-gray-800 mg-b-5">
                Group Pengguna
                {% if haveRole(session.get('group'),13) %}
                <a href="{{ url('/system/group/add') }}" class="btn btn-teal btn-with-icon">
                    <div class="ht-25">
                        <span class="icon wd-25"><i class="fa fa-plus"></i></span>
                        <span class="pd-x-15">Tambah Group</span>
                    </div>
                </a>
                {% endif %}
            </h4>
            <p class="mg-b-0">Daftar Group Pengguna</p>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
            <div class="input-group">
                <input name="search" type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                <button class="btn bd bg-white tx-gray-600" type="button"><i class="fa fa-search"></i></button>
            </span>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                $('[name="search"]').on('keypress', function (e) {
                    if(e.which === 13){
                        $('#overlay').toggle();
                        $.post(
                            '{{ url('/system/group/search') }}',
                            { search : $('[name="search"]').val() },
                            function(result){
                                $(".br-pagebody").html(result);
                                $('#overlay').toggle();
                            })
                            .fail(function( jqXhr, textStatus, errorThrown ){
                                console.log( errorThrown );
                                $('#overlay').toggle();
                            });
                    }
                });
            });
        </script>
    </div>
</div>

<hr>
<div class="br-pagebody pd-sm-x-30">
    Total : <strong>{{ page.total_items }}</strong> Groups
    <table class="table table-hover table-bordered">
        <thead class="thead-colored thead-teal">
        <tr>
            {% if haveRole(session.get('group'),14) OR haveRole(session.get('group'),15) %}
            <th class="text-light">#</th>
            {% endif %}
            <th class="text-light">Name</th>
            <th class="text-light">Description</th>
            <th class="text-light">Created by</th>
            <th class="text-light">Created at</th>
        </tr>
        </thead>
        <tbody class="bg-lightsky">
        {% for data in page.items %}
            <tr>
                {% if haveRole(session.get('group'),14) OR haveRole(session.get('group'),15) %}
                <td>
                    {% if haveRole(session.get('group'),14) %}<a title="edit" class="btn btn-teal pd-y-2 pg-x-3" href="{{ url('/system/group/edit/') }}{{ data.id|encode }}"><i class="icon ion-edit"></i> </a>{% endif %}
                    {% if haveRole(session.get('group'),15) %}<a title="roles" class="btn btn-warning pd-y-2 pg-x-3" href="{{ url('/system/group/role/') }}{{ data.id|encode }}"><i class="icon ion-lock-combination"></i> </a>{% endif %}
                </td>
                {% endif %}
                <td>{{ data.name }}</td>
                <td>{{ data.description }}</td>
                <td>{{ data.created_at|dateTimeFormat }} / {{ data.created_by }}</td>
                <td>{{ data.updated_at|dateTimeFormat }} / {{ data.updated_by }}</td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    <div class="d-flex align-items-center justify-content-center">
        <nav aria-label="Page navigation">
            <ul class="pagination pagination-basic mg-b-0">
                {% if page.current!=1 %}
                    <li class="page-item">
                        <a class="page-link" href="{{ url('/system/group/') }}{{ page.next }}" aria-label="Next">
                            <i class="fa fa-angle-double-left"></i>
                        </a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="{{ url('/system/group/') }}{{ page.next }}" aria-label="Next">
                            <i class="fa fa-angle-left"></i>
                        </a>
                    </li>
                {% endif %}
                {% for i in 1..page.last %}
                    {% if (i>page.current-5 AND i<page.current+5)  %}
                        <li class="page-item {%  if i==page.current %}active{% endif %}"><a class="page-link" href="{{ url('/content/category/') }}">{{ i }}</a></li>
                    {% endif %}
                {% endfor %}

                {% if page.current!=page.last %}
                    <li class="page-item">
                        <a class="page-link" href="{{ url('/system/group/') }}{{ page.next }}" aria-label="Next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </li>
                {% endif %}
                {% if page.last>5 AND page.current!=page.last %}

                    <li class="page-item">
                        <a class="page-link" href="{{ url('/system/group/') }}{{ page.last }}" aria-label="Last">
                            <i class="fa fa-angle-double-right"></i>
                        </a>
                    </li>
                {% endif %}
            </ul>
        </nav>
    </div>
</div>