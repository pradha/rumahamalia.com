<div class="mg-b-10">
    Ditemukan sebanyak : <strong>{{ users|length }}</strong> data, Dari hasil pencarian : "<strong>{{ search }}</strong>"  <a title="hapus pencarian" class="btn btn-warning rounded-10 pd-x-10 pd-y-1" href="{{ url('/system/user') }}"><i class="icon fa fa-close"></i> </a>
</div>

<table class="table table-hover table-bordered">
    <thead class="thead-colored thead-teal">
    <tr>
        <th class="text-light">#</th>
        <th class="text-light">Username</th>
        <th class="text-light">Name</th>
        <th class="text-light">Email</th>
        <th class="text-light">Gender</th>
        <th class="text-light">Biography</th>
        <th class="text-light">Active</th>
        <th class="text-light">Last Logged From</th>
        <th class="text-light">Status</th>
        <th class="text-light">Created by</th>
        <th class="text-light">Created at</th>
    </tr>
    </thead>
    <tbody class="bg-lightsky">
    {% for data in users %}
        <tr>
            <td>
                <a title="edit" class="btn btn-teal pd-y-2 pg-x-3" href="{{ url('/system/user/edit/') }}{{ data.username|encode }}"><i class="icon ion-edit"></i> </a>
            </td>
            <td>{{ data.username }}</td>
            <td>{{ data.name }}</td>
            <td>{{ data.email }}</td>
            <td>{{ data.gender_name }}</td>
            <td>{{ data.biography }}</td>
            <td>{% if data.is_logged_in %}<i class="icon ion-checkmark"></i>{% else %}<i class="icon ion-close"></i>{% endif %} </td>
            <td>{{ data.last_logged_in_from }} </td>
            <td>{% if data.status %}<i class="icon ion-checkmark"></i>{% else %}<i class="icon ion-close"></i>{% endif %} </td>
            <td>{{ data.created_at|dateTimeFormat }} / {{ data.created_by }}</td>
            <td>{{ data.updated_at|dateTimeFormat }} / {{ data.updated_by }}</td>
        </tr>
    {% endfor %}
    </tbody>
</table>