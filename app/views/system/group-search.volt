<div class="mg-b-10">
    Ditemukan sebanyak : <strong>{{ users|length }}</strong> data, Dari hasil pencarian : "<strong>{{ search }}</strong>"  <a title="hapus pencarian" class="btn btn-warning rounded-10 pd-x-10 pd-y-1" href="{{ url('/system/group') }}"><i class="icon fa fa-close"></i> </a>
</div>
<table class="table table-hover table-bordered">
    <thead class="thead-colored thead-teal">
    <tr>
        <th class="text-light wd-10p-force">#</th>
        <th class="text-light">Name</th>
        <th class="text-light">Description</th>
        <th class="text-light">Created by</th>
        <th class="text-light">Created at</th>
    </tr>
    </thead>
    <tbody class="bg-lightsky">
    {% for data in groups %}
        <tr>
            <td>
                <a title="edit" class="btn btn-teal pd-y-2 pg-x-3" href="{{ url('/system/group/edit/') }}{{ data.id|encode }}"><i class="icon ion-edit"></i> </a>
                <a title="roles" class="btn btn-warning pd-y-2 pg-x-3" href="{{ url('/system/role/') }}{{ data.id }}"><i class="icon ion-lock-combination"></i> </a>
            </td>
            <td>{{ data.name }}</td>
            <td>{{ data.description }}</td>
            <td>{{ data.created_at|dateTimeFormat }} / {{ data.created_by }}</td>
            <td>{{ data.updated_at|dateTimeFormat }} / {{ data.updated_by }}</td>
        </tr>
    {% endfor %}
    </tbody>
</table>