<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>{% if title is defined %}{{ title }}{% else %}Rumah Amalia | Organisasi Nonprofit{% endif %}</title>
    <meta name="description" content="{% if description is defined %}{{ description }}{% else %}Rumah Amalia adalah Komunitas rumah belajar untuk anak Yatim & anak Dhuafa{% endif %}">
    <meta name="keywords" content="{% if keywords is defined %}{{ keywords }}{% else %}Ruah Amalia, Komunitas rumah belajar, anak yatim, sosial, dhuafa{% endif %}">
    <meta name="author" content="{% if author is defined AND author_email is defined %}{{ author }}, {{ author_email }}{% else %}Rumah Amalia, adit@pradha.id{% endif %}">
    <meta name='designer' content='{% if designer is defined %}{{ designer }}{% else %}Aditya Y Pradhana{% endif %}'>
    <meta name='copyright' content='{% if copyright is defined %}{{ copyright }}{% else %}Pradha Corp{% endif %}'>
    <meta name='language' content='{% if language is defined %}{{ language }}{% else %}ID{% endif %}'>
    <meta name='robots' content='{% if robots is defined %}{{ robots }}{% else %}index,follow{% endif %}'>
    <meta name='coverage' content='{% if coverage is defined %}{{ coverage }}{% else %}Worldwide{% endif %}'>
    <meta name='distribution' content='{% if distribution is defined %}{{ distribution }}{% else %}Global{% endif %}'>
    <meta name='target' content='{% if target is defined %}{{ target }}{% else %}all{% endif %}'>
    <meta name='owner' content='{% if owner is defined %}{{ owner }}{% else %}Aditya Y Pradhana{% endif %}'>
    <meta name='url' content='{{ url() }}{{ router.getRewriteUri() }}'>
    <meta name='identifier-URL' content='{{ url() }}{{ router.getRewriteUri() }}'>
    <link rel='canonical' href='{{ url() }}{{ router.getRewriteUri() }}'>
    <link rel='publisher' href='{% if publisher is defined %}{{ publisher }}{% else %}Aditya Y Pradhana{% endif %}'>

    <!-- Facebook Open Graph -->
    <meta property="og:url" content="{{ url() }}{{ router.getRewriteUri() }}" />
    <meta property="og:type" content="{% if og_type is defined %}{{ og_type }}{% else %}summary{% endif %}" />
    <meta property="og:title" content="{% if title is defined %}{{ title }}{% else %}Rumah Amalia | Organisasi Nonprofit{% endif %}" />
    <meta property="og:description" content="{% if description is defined %}{{ description }}{% else %}Rumah Amalia adalah Komunitas rumah belajar untuk anak Yatim & anak Dhuafa{% endif %}" />

    {% if images is defined %}
        {% for image in images %}
        <meta property="og:image" content="{{ image }}" />
        {% endfor %}
    {% elseif image is defined %}
        <meta property="og:image" content="{{ image }}" />
    {% endif %}

    <meta property="og:locale" content="{% if locale is defined %}{{ locale }}{% else %}id_ID{% endif %}" />
    <!-- End Facebook Open Graph -->

    <!-- Twitter Card -->
    <meta name="twitter:card" content="{% if twitter_card is defined %}{{ twitter_card }}{% else %}summary{% endif %}">
    <meta name="twitter:url" content="{{ url() }}{{ router.getRewriteUri() }}">
    <meta name="twitter:title" content="{% if title is defined %}{{ title }}{% else %}Rumah Amalia | Organisasi Nonprofit{% endif %}">
    <meta name="twitter:description" content="{% if description is defined %}{{ description }}{% else %}Rumah Amalia adalah Komunitas rumah belajar untuk anak Yatim & anak Dhuafa{% endif %}">

    {% if images is defined %}
        {% for image in images %}
            <meta name="twitter:image" content="{{ image }}">
        {% endfor %}
    {% elseif image is defined %}
        <meta name="twitter:image" content="{{ image }}">
    {% endif %}

    <meta name="twitter:site" content="{% if twitter_site is defined %}{{ twitter_site }}{% else %}@adityudhna{% endif %}">
    <meta name="twitter:creator" content="{% if twitter_creator is defined %}{{ twitter_creator }}{% else %}@adityudhna{% endif %}">
    <!-- End Twitter Card -->

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ url('/img/favicons/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ url('/img/favicons/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ url('/img/favicons/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('/img/favicons/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ url('/img/favicons/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ url('/img/favicons/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ url('/img/favicons/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ url('/img/favicons/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('/img/favicons/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ url('/img/favicons/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('/img/favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ url('/img/favicons/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('/img/favicons/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ url('/img/favicons/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ url('/img/favicons/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">
    <!-- End Favicon -->

    <!-- Stylesheet -->
    <link href="{{ url('/lib/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ url('/lib/Ionicons/css/ionicons.css') }}" rel="stylesheet">
    <link href="{{ url('/lib/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet">
    <link href="{{ url('/lib/jquery-switchbutton/jquery.switchButton.css') }}" rel="stylesheet">
    <link href="{{ url('/lib/SpinKit/spinkit.css') }}" rel="stylesheet">
    <link href="{{ url('/css/bracket.css') }}" rel="stylesheet">
    <link href="{{ url('/css/slick.css') }}" rel="stylesheet">
    <link href="{{ url('/css/slick-theme.css') }}" rel="stylesheet">
    <link href="{{ url('/css/style.css') }}" rel="stylesheet">

    <!-- End Stylesheet -->


    <!-- Scripts -->
    <script src="{{ url('/lib/jquery/jquery.js') }}"></script>
    <script src="{{ url('/js/slick.min.js') }}"></script>
    <script src="{{ url('/js/jquery-sidebar.min.js') }}"></script>
    <!-- End Scripts -->

</head>
<body>
    {{ content() }}
    <script src="{{ url('/lib/popper.js/popper.js') }}"></script>
    <script src="{{ url('/lib/bootstrap/bootstrap.js') }}"></script>
    <script src="{{ url('/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js') }}"></script>
    <script src="{{ url('/lib/moment/moment.js') }}"></script>
    <script src="{{ url('/lib/jquery-ui/jquery-ui.js') }}"></script>
    <script src="{{ url('/lib/jquery-switchbutton/jquery.switchButton.js') }}"></script>
    <script src="{{ url('/lib/peity/jquery.peity.js') }}"></script>
    <script src="{{ url('/js/bracket.js') }}"></script>

</body>
</html>
