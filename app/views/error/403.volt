<div class="pd-y-50-force mg-x-20-force text-center">
    <h1 class="tx-80">ERROR 403</h1>
    <h2>FORBIDDEN</h2>
    <img src="{{ url('/img/forbidden.png') }}" class="img mg-10">
    <h1>You have no privilege to access this page</h1>
</div>
