<?php

class VCategories extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     * @Column(type="string", length=36, nullable=true)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=36, nullable=true)
     */
    public $parent;

    /**
     *
     * @var string
     * @Column(type="string", length=25, nullable=true)
     */
    public $parent_name;

    /**
     *
     * @var string
     * @Column(type="string", length=150, nullable=true)
     */
    public $parent_description;

    /**
     *
     * @var string
     * @Column(type="string", length=25, nullable=true)
     */
    public $name;

    /**
     *
     * @var string
     * @Column(type="string", length=150, nullable=true)
     */
    public $description;

    /**
     *
     * @var string
     * @Column(type="string", length=250, nullable=true)
     */
    public $keywords;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $options;

    /**
     *
     * @var string
     * @Column(type="string", length=150, nullable=true)
     */
    public $slug;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=true)
     */
    public $created_by;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=true)
     */
    public $updated_by;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("content");
        $this->setSource("v_categories");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'v_categories';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return VCategories[]|VCategories|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return VCategories|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
