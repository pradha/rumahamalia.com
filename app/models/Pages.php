<?php

class Pages extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     * @Primary
     * @Column(type="string", length=36, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=36, nullable=true)
     */
    public $parent;

    /**
     *
     * @var string
     * @Column(type="string", length=150, nullable=false)
     */
    public $title;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $content;

    /**
     *
     * @var string
     * @Column(type="string", length=250, nullable=false)
     */
    public $description;

    /**
     *
     * @var string
     * @Column(type="string", length=150, nullable=false)
     */
    public $slug;

    /**
     *
     * @var string
     * @Column(type="string", length=250, nullable=false)
     */
    public $keywords;

    /**
     *
     * @var string
     * @Column(type="string", length=150, nullable=true)
     */
    public $image;

    /**
     *
     * @var integer
     * @Column(type="integer", length=16, nullable=true)
     */
    public $index;

    /**
     *
     * @var integer
     * @Column(type="integer", length=16, nullable=true)
     */
    public $status;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=false)
     */
    public $created_by;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=false)
     */
    public $updated_by;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("content");
        $this->setSource("pages");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'pages';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Pages[]|Pages|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Pages|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
