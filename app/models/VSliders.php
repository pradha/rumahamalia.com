<?php

class VSliders extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     * @Column(type="string", length=36, nullable=true)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=150, nullable=true)
     */
    public $title;

    /**
     *
     * @var string
     * @Column(type="string", length=250, nullable=true)
     */
    public $description;

    /**
     *
     * @var string
     * @Column(type="string", length=150, nullable=true)
     */
    public $url;

    /**
     *
     * @var string
     * @Column(type="string", length=150, nullable=true)
     */
    public $image;

    /**
     *
     * @var integer
     * @Column(type="integer", length=16, nullable=true)
     */
    public $status;

    /**
     *
     * @var string
     * @Column(type="string", length=25, nullable=true)
     */
    public $status_name;

    /**
     *
     * @var string
     * @Column(type="string", length=36, nullable=true)
     */
    public $placement;

    /**
     *
     * @var string
     * @Column(type="string", length=25, nullable=true)
     */
    public $placement_name;

    /**
     *
     * @var string
     * @Column(type="string", length=150, nullable=true)
     */
    public $placement_description;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=true)
     */
    public $created_by;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=true)
     */
    public $updated_by;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("content");
        $this->setSource("v_sliders");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'v_sliders';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return VSliders[]|VSliders|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return VSliders|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
