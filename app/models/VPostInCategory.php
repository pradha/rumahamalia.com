<?php

class VPostInCategory extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     * @Column(type="string", length=36, nullable=true)
     */
    public $post;

    /**
     *
     * @var string
     * @Column(type="string", length=36, nullable=true)
     */
    public $post_id;

    /**
     *
     * @var string
     * @Column(type="string", length=150, nullable=true)
     */
    public $post_title;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $post_content;

    /**
     *
     * @var string
     * @Column(type="string", length=250, nullable=true)
     */
    public $post_description;

    /**
     *
     * @var string
     * @Column(type="string", length=150, nullable=true)
     */
    public $post_slug;

    /**
     *
     * @var string
     * @Column(type="string", length=250, nullable=true)
     */
    public $post_keywords;

    /**
     *
     * @var string
     * @Column(type="string", length=150, nullable=true)
     */
    public $post_image;

    /**
     *
     * @var integer
     * @Column(type="integer", length=16, nullable=true)
     */
    public $post_status;

    /**
     *
     * @var string
     * @Column(type="string", length=25, nullable=true)
     */
    public $status_name;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=true)
     */
    public $post_created_by;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=true)
     */
    public $post_updated_by;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=true)
     */
    public $post_published_by;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $post_created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $post_updated_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $post_published_at;

    /**
     *
     * @var string
     * @Column(type="string", length=36, nullable=true)
     */
    public $category;

    /**
     *
     * @var string
     * @Column(type="string", length=36, nullable=true)
     */
    public $category_id;

    /**
     *
     * @var string
     * @Column(type="string", length=36, nullable=true)
     */
    public $category_parent;

    /**
     *
     * @var string
     * @Column(type="string", length=25, nullable=true)
     */
    public $category_name;

    /**
     *
     * @var string
     * @Column(type="string", length=150, nullable=true)
     */
    public $category_descrption;

    /**
     *
     * @var string
     * @Column(type="string", length=250, nullable=true)
     */
    public $category_keywords;

    /**
     *
     * @var string
     * @Column(type="string", length=150, nullable=true)
     */
    public $category_slug;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=true)
     */
    public $category_created_by;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=true)
     */
    public $category_updated_by;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $category_created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $category_updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("content");
        $this->setSource("v_post_in_category");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'v_post_in_category';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return VPostInCategory[]|VPostInCategory|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return VPostInCategory|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
