<?php

class UserSettings extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     * @Primary
     * @Column(type="string", length=15, nullable=false)
     */
    public $username;

    /**
     *
     * @var integer
     * @Column(type="integer", length=16, nullable=false)
     */
    public $profit_type;

    /**
     *
     * @var double
     * @Column(type="double", length=20, nullable=false)
     */
    public $profit;

    /**
     *
     * @var string
     * @Column(type="string", length=6, nullable=true)
     */
    public $pin;

    /**
     *
     * @var double
     * @Column(type="double", length=20, nullable=false)
     */
    public $balance;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=true)
     */
    public $master;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $telegram;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=false)
     */
    public $created_by;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=false)
     */
    public $updated_by;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("system");
        $this->setSource("user_settings");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'user_settings';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserSettings[]|UserSettings|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserSettings|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
