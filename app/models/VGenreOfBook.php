<?php

class VGenreOfBook extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     * @Column(type="string", length=36, nullable=true)
     */
    public $book;

    /**
     *
     * @var string
     * @Column(type="string", length=36, nullable=true)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", nullable=true)
     */
    public $number;

    /**
     *
     * @var string
     * @Column(type="string", length=13, nullable=true)
     */
    public $isbn;

    /**
     *
     * @var string
     * @Column(type="string", length=50, nullable=true)
     */
    public $title;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $description;

    /**
     *
     * @var string
     * @Column(type="string", length=150, nullable=true)
     */
    public $cover;

    /**
     *
     * @var integer
     * @Column(type="integer", length=32, nullable=true)
     */
    public $quantity;

    /**
     *
     * @var string
     * @Column(type="string", length=36, nullable=true)
     */
    public $author;

    /**
     *
     * @var string
     * @Column(type="string", length=150, nullable=true)
     */
    public $author_name;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $auhtor_information;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $publication_date;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=true)
     */
    public $created_by;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=true)
     */
    public $updated_by;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $updated_at;

    /**
     *
     * @var string
     * @Column(type="string", length=36, nullable=true)
     */
    public $genre;

    /**
     *
     * @var string
     * @Column(type="string", length=150, nullable=true)
     */
    public $genre_name;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $genre_description;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("library");
        $this->setSource("v_genre_of_book");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'v_genre_of_book';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return VGenreOfBook[]|VGenreOfBook|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return VGenreOfBook|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
