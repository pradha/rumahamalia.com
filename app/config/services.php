<?php

use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Php as PhpEngine;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Flash\Direct as Flash;

/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    return include APP_PATH . "/config/config.php";
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $config = $this->getConfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});

/**
 * Setting up the view component
 */
$di->setShared('view', function () {
    $config = $this->getConfig();

    $view = new View();
    $view->setDI($this);
    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines([
        '.volt' => function ($view) {
            $config = $this->getConfig();

            $volt = new VoltEngine($view, $this);

            $volt->setOptions([
                'compiledPath' => $config->application->cacheDir,
                'compiledSeparator' => '_',
                'stat' => true,
                'compileAlways' => true
            ]);

            $compiler = $volt->getCompiler();
            $compiler->addFunction(
                'haveRole',
                function ($resolvedArgs, $exprArgs) use ($compiler) {
                    $first=$compiler->expression($exprArgs[0]['expr']);
                    $second=$compiler->expression($exprArgs[1]['expr']);
                    return 'haveRole(' . $first . ', ' . $second . ')';
                }
            );
            $compiler->addFunction(
                'add30m',
                function ($resolvedArgs, $exprArgs) use ($compiler) {
                    return "date('H:i', strtotime($resolvedArgs. '+30 minutes'))";
                }
            );
            $compiler->addFunction(
                'subCat',
                function ($resolvedArgs, $exprArgs) use ($compiler) {
                    return "subCat($resolvedArgs)";
                }
            );
            $compiler->addFunction(
                'sub30m',
                function ($resolvedArgs, $exprArgs) use ($compiler) {
                    return "date('H:i', strtotime($resolvedArgs. '-30 minutes'))";
                }
            );
            $compiler->addFunction(
                'postInCategory',
                function ($resolvedArgs, $exprArgs) use ($compiler) {
                    return "postInCategory($resolvedArgs)";
                }
            );
            $compiler->addFunction(
                'firstImage',
                function ($resolvedArgs, $exprArgs) use ($compiler) {
                    return "firstImage(".$resolvedArgs.")";
                }
            );

            $compiler->addFilter(
                'numberFormat',
                function ($resolvedArgs, $exprArgs) use ($compiler) {
                    return 'number_format('.$resolvedArgs.',2)';
                }
            );
            $compiler->addFilter(
                'dateFormat',
                function ($resolvedArgs, $exprArgs) use ($compiler) {
                    return "date('d M Y', strtotime($resolvedArgs))";
                }
            );
            $compiler->addFilter(
                'month',
                function ($resolvedArgs, $exprArgs) use ($compiler) {
                    return "date('m', strtotime($resolvedArgs))";
                }
            );
            $compiler->addFilter(
                'year',
                function ($resolvedArgs, $exprArgs) use ($compiler) {
                    return "date('Y', strtotime($resolvedArgs))";
                }
            );
            $compiler->addFilter(
                'dateTimeFormat',
                function ($resolvedArgs, $exprArgs) use ($compiler) {
                    return "date('d M Y H:i:s', strtotime($resolvedArgs))";
                }
            );
            $compiler->addFilter(
                'encode',
                function ($resolvedArgs, $exprArgs) use ($compiler) {
                    return "encode($resolvedArgs)";
                }
            );
            $compiler->addFilter(
                'decode',
                function ($resolvedArgs, $exprArgs) use ($compiler) {
                    return "decode($resolvedArgs)";
                }
            );
            $compiler->addFilter(
                'tags',
                function ($resolvedArgs, $exprArgs) use ($compiler) {
                    return "tags($resolvedArgs)";
                }
            );
            $compiler->addFilter(
                'fileSizeUnit',
                function ($resolvedArgs, $exprArgs) use ($compiler) {
                    return "fileSizeUnit($resolvedArgs)";
                }
            );
            $compiler->addFilter(
                'slug',
                function ($resolvedArgs, $exprArgs) use ($compiler) {
                    return "slugify($resolvedArgs)";
                }
            );
            return $volt;
        },
        '.phtml' => PhpEngine::class

    ]);

    return $view;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () {
    $config = $this->getConfig();

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $config->database->adapter;
    $params = [
        'host'     => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname'   => $config->database->dbname
    ];

    if ($config->database->adapter == 'Postgresql') {
        unset($params['charset']);
    }

    $connection = new $class($params);

    return $connection;
});


/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Register the session flash service with the Twitter Bootstrap classes
 */
$di->set('flash', function () {
    return new Flash([
        'error'   => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice'  => 'alert alert-info',
        'warning' => 'alert alert-warning'
    ]);
});

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

$di->set('router', function() {

    $router = new \Phalcon\Mvc\Router();

    $router->add("/index/:params", array(
        'controller' => "index",
        'params' => 1,
    ));
    $router->add("/page/:params", array(
        'controller' => "page",
        'params' => 1,
    ));
    $router->add("/read/:params", array(
        'controller' => "read",
        'params' => 1,
    ));
    $router->add("/login/:params", array(
        'controller' => "login",
        'params' => 1,
    ));
    $router->add("/forgot/:params", array(
        'controller' => "forgot",
        'params' => 1,
    ));
    $router->add("/bot/:params", array(
        'controller' => "bot",
        'params' => 1,
    ));
    $router->add("/out/:params", array(
        'controller' => "out",
        'params' => 1,
    ));

    return $router;
});

/* All Functions */
function haveRole($group, $role){
    if (isset($group) && $group!=null){
        $check=GroupHaveRole::Find("group_id='".$group."' AND role_id='".$role."'");
        if (count($check)>=1) return true;
        else return false;
    } else return false;
}
function encode($string){
    return str_replace(array('+', '/'), array('-', '_'), base64_encode(gzdeflate($string)));
}
function decode($string){
    $replace=str_replace(array('-', '_'), array('+', '/'),$string);
    return gzinflate(base64_decode($replace));
}
function tags($string){
    $tags=explode(",",$string);
    $data="";
    $function=new Functions();
    $url = new UrlResolver();
    foreach ($tags as $tag){
        $data.='<a target="_blank" href="'.$url->get('tag/').$function->slugify(null,$tag).'" class="badge badge-pill badge-info mg-x-1 pd-5">'.$tag.'</a>';
    }
    echo $data;
}
function postInCategory($post){
    return VPostInCategory::find(["post='".$post."'"]);
}
function firstImage($html){
    preg_match('/< *img[^>]*src *= *["\']?([^"\']*)/i', $html, $img);
    return $img[1];
}
function slugify($text)
{
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, '-');

    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);

    // lowercase
    $text = strtolower($text);

    if (empty($text)) {
        return 'n-a';
    }


    return $text;
}
function subCat($parent){
    $categories=Categories::find(["parent='".$parent."'"]);
    if (count($categories)>=1){
        echo '<ul>';
        foreach ($categories as $category){
            echo '<li>';
            echo '<label class="ckbox">';
            echo '<input name="category[]" type="checkbox" value="'.$category->id.'">';
            echo '<span>'.$category->name.'</span></label>';
            echo '</li>';
        }
        echo '</ul>';
    }
}
function in_array_multi($needle, $haystack) {
    if(in_array($needle, $haystack)) {
        return true;
    }
    foreach($haystack as $element) {
        if(is_array($element) && in_array_multi($needle, $element))
            return true;
    }
    return false;
}
function fileSizeUnit($bytes)
{
    if ($bytes >= 1073741824)
    {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    }
    elseif ($bytes >= 1048576)
    {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    }
    elseif ($bytes >= 1024)
    {
        $bytes = number_format($bytes / 1024, 2) . ' KB';
    }
    elseif ($bytes > 1)
    {
        $bytes = $bytes . ' bytes';
    }
    elseif ($bytes == 1)
    {
        $bytes = $bytes . ' byte';
    }
    else
    {
        $bytes = '0 bytes';
    }

    return $bytes;
}